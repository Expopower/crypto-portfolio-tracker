﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CryptoPortfolioTracker.Migrations
{
    public partial class ChangedTransactionValuesIntoList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_TransactionValues_CryptoValueID",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_TransactionValues_FiatValueID",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_TransactionValues_CurrencyID",
                table: "TransactionValues");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_CryptoValueID",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_FiatValueID",
                table: "Transactions");

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("71e2e84c-04ad-4e9f-a1a9-9871f97a1a34"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("a05489fc-3975-457c-96a5-8926b92ee12c"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("c191812c-23b6-401c-8980-d659585802a1"));

            migrationBuilder.DropColumn(
                name: "CryptoValueID",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "FiatValueID",
                table: "Transactions");

            migrationBuilder.RenameColumn(
                name: "OverrideAmountInCad",
                table: "Transactions",
                newName: "OverrideFiatValue");

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("1c7673b8-3ecf-42f6-96b2-92adb4a7cb47"), "USDC", 0, "USD Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("63068fed-4284-4b4b-bde6-e0c424e3430d"), "CRO", 0, "Crypto.com Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("7b4d8cd1-14be-4464-8a45-dde47ffce4a1"), "ETH", 0, "Ethereum" });

            migrationBuilder.CreateIndex(
                name: "IX_TransactionValues_CurrencyID_TransactionID",
                table: "TransactionValues",
                columns: new[] { "CurrencyID", "TransactionID" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TransactionValues_TransactionID",
                table: "TransactionValues",
                column: "TransactionID");

            migrationBuilder.AddForeignKey(
                name: "FK_TransactionValues_Transactions_TransactionID",
                table: "TransactionValues",
                column: "TransactionID",
                principalTable: "Transactions",
                principalColumn: "TransactionID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TransactionValues_Transactions_TransactionID",
                table: "TransactionValues");

            migrationBuilder.DropIndex(
                name: "IX_TransactionValues_CurrencyID_TransactionID",
                table: "TransactionValues");

            migrationBuilder.DropIndex(
                name: "IX_TransactionValues_TransactionID",
                table: "TransactionValues");

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("1c7673b8-3ecf-42f6-96b2-92adb4a7cb47"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("63068fed-4284-4b4b-bde6-e0c424e3430d"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("7b4d8cd1-14be-4464-8a45-dde47ffce4a1"));

            migrationBuilder.RenameColumn(
                name: "OverrideFiatValue",
                table: "Transactions",
                newName: "OverrideAmountInCad");

            migrationBuilder.AddColumn<Guid>(
                name: "CryptoValueID",
                table: "Transactions",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "FiatValueID",
                table: "Transactions",
                type: "TEXT",
                nullable: true);

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("71e2e84c-04ad-4e9f-a1a9-9871f97a1a34"), "USDC", 0, "USD Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("a05489fc-3975-457c-96a5-8926b92ee12c"), "CRO", 0, "Crypto.com Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("c191812c-23b6-401c-8980-d659585802a1"), "ETH", 0, "Ethereum" });

            migrationBuilder.CreateIndex(
                name: "IX_TransactionValues_CurrencyID",
                table: "TransactionValues",
                column: "CurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_CryptoValueID",
                table: "Transactions",
                column: "CryptoValueID");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_FiatValueID",
                table: "Transactions",
                column: "FiatValueID");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_TransactionValues_CryptoValueID",
                table: "Transactions",
                column: "CryptoValueID",
                principalTable: "TransactionValues",
                principalColumn: "TransactionValueID");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_TransactionValues_FiatValueID",
                table: "Transactions",
                column: "FiatValueID",
                principalTable: "TransactionValues",
                principalColumn: "TransactionValueID");
        }
    }
}
