﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CryptoPortfolioTracker.Migrations
{
    public partial class AddedInvertedExchanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("1f5078be-a381-4705-b07e-b31d1d247b57"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("499c0586-f096-4b0d-8b09-36ac56d27eb3"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("a66a840e-6b48-4689-95f5-79a0122fd6ef"));

            migrationBuilder.AddColumn<bool>(
                name: "IsInverted",
                table: "CurrencyReferenceExchanges",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("2b16871e-b124-410a-8eef-93b8a4932291"), "CRO", 0, "Crypto.com Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("52b22707-6283-4d05-9e64-2b405a4431f2"), "USDC", 0, "USD Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("acfbd265-92c7-440f-a44b-753853f47f67"), "ETH", 0, "Ethereum" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("2b16871e-b124-410a-8eef-93b8a4932291"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("52b22707-6283-4d05-9e64-2b405a4431f2"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("acfbd265-92c7-440f-a44b-753853f47f67"));

            migrationBuilder.DropColumn(
                name: "IsInverted",
                table: "CurrencyReferenceExchanges");

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("1f5078be-a381-4705-b07e-b31d1d247b57"), "USDC", 0, "USD Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("499c0586-f096-4b0d-8b09-36ac56d27eb3"), "ETH", 0, "Ethereum" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("a66a840e-6b48-4689-95f5-79a0122fd6ef"), "CRO", 0, "Crypto.com Coin" });
        }
    }
}
