﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CryptoPortfolioTracker.Migrations
{
    public partial class MovedRetrievalDatesNewTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("43839011-fbf5-42ce-b775-c5621dcac704"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("6c4baafa-c11b-4250-9d65-01454a14e70c"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("83dccace-c0e7-4e41-b8be-55ba876e0ce1"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("a23497e0-c85b-4b9f-bbe1-05ebe1ac3b3f"));

            migrationBuilder.CreateTable(
                name: "CurrencyRateRetrievalDateRanges",
                columns: table => new
                {
                    CurrencyRateRetrievalDateRangeID = table.Column<Guid>(type: "TEXT", nullable: false),
                    StartDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    EndDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    FromCurrencyID = table.Column<Guid>(type: "TEXT", nullable: false),
                    ToCurrencyID = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrencyRateRetrievalDateRanges", x => x.CurrencyRateRetrievalDateRangeID);
                    table.ForeignKey(
                        name: "FK_CurrencyRateRetrievalDateRanges_Currencies_FromCurrencyID",
                        column: x => x.FromCurrencyID,
                        principalTable: "Currencies",
                        principalColumn: "CurrencyID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CurrencyRateRetrievalDateRanges_Currencies_ToCurrencyID",
                        column: x => x.ToCurrencyID,
                        principalTable: "Currencies",
                        principalColumn: "CurrencyID",
                        onDelete: ReferentialAction.Cascade);
                });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("6a4fc4c6-e805-4bab-b75c-c173a12c09eb"), "BTC", 0, "Bitcoin", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("801eb8ad-ad32-4a19-b2ba-947f7c48a303"), "CRO", 0, "Crypto.com Coin", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("8e24b0be-035a-4088-9f00-18c0fa856df3"), "USDC", 0, "USD Coin", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("a359c3bc-63b6-460e-99a9-0dbc2fb1e81f"), "ETH", 0, "Ethereum", null });

            migrationBuilder.CreateIndex(
                name: "IX_CurrencyRateRetrievalDateRanges_FromCurrencyID_ToCurrencyID",
                table: "CurrencyRateRetrievalDateRanges",
                columns: new[] { "FromCurrencyID", "ToCurrencyID" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CurrencyRateRetrievalDateRanges_ToCurrencyID",
                table: "CurrencyRateRetrievalDateRanges",
                column: "ToCurrencyID");

            migrationBuilder.Sql(@"
                INSERT INTO CurrencyRateRetrievalDateRanges (CurrencyRateRetrievalDateRangeID, StartDate, EndDate, FromCurrencyID, ToCurrencyID)
                SELECT
                    (
                        -- Pseudo-GUID generation
                        hex(randomblob(4)) ||  '-'
	                    || hex(randomblob(2)) ||  '-'
	                    || hex(randomblob(2)) ||  '-'
	                    || hex(randomblob(2)) ||  '-'
	                    || hex(randomblob(6))
                    ),
                    c.RetrievedRatesStartDateTime,
                    c.RetrievedRatesEndDateTime,
                    c.CurrencyID,
                    (
                        CASE WHEN c.CurrencyType = 0 -- Crypto
                            THEN 'F69C219E-EE7C-48F2-BC1D-77B3AFE70F5D' -- USD
                            ELSE '758C888A-0CE2-45B3-A863-434796C451CE' -- CAD
                        END
                    )
                FROM
                    Currencies c
                WHERE
                    c.RetrievedRatesStartDateTime IS NOT NULL
                    AND c.RetrievedRatesEndDateTime IS NOT NULL");

            migrationBuilder.DropColumn(
                name: "RetrievedRatesEndDateTime",
                table: "Currencies");

            migrationBuilder.DropColumn(
                name: "RetrievedRatesStartDateTime",
                table: "Currencies");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CurrencyRateRetrievalDateRanges");

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("6a4fc4c6-e805-4bab-b75c-c173a12c09eb"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("801eb8ad-ad32-4a19-b2ba-947f7c48a303"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("8e24b0be-035a-4088-9f00-18c0fa856df3"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("a359c3bc-63b6-460e-99a9-0dbc2fb1e81f"));

            migrationBuilder.AddColumn<DateTime>(
                name: "RetrievedRatesEndDateTime",
                table: "Currencies",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RetrievedRatesStartDateTime",
                table: "Currencies",
                type: "TEXT",
                nullable: true);

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange", "RetrievedRatesEndDateTime", "RetrievedRatesStartDateTime" },
            //    values: new object[] { new Guid("43839011-fbf5-42ce-b775-c5621dcac704"), "ETH", 0, "Ethereum", null, null, null });

            //migrationbuilder.insertdata(
            //    table: "currencies",
            //    columns: new[] { "currencyid", "currencycode", "currencytype", "name", "referenceexchange", "retrievedratesenddatetime", "retrievedratesstartdatetime" },
            //    values: new object[] { new guid("6c4baafa-c11b-4250-9d65-01454a14e70c"), "btc", 0, "bitcoin", null, null, null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange", "RetrievedRatesEndDateTime", "RetrievedRatesStartDateTime" },
            //    values: new object[] { new Guid("83dccace-c0e7-4e41-b8be-55ba876e0ce1"), "CRO", 0, "Crypto.com Coin", null, null, null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange", "RetrievedRatesEndDateTime", "RetrievedRatesStartDateTime" },
            //    values: new object[] { new Guid("a23497e0-c85b-4b9f-bbe1-05ebe1ac3b3f"), "USDC", 0, "USD Coin", null, null, null });
        }
    }
}
