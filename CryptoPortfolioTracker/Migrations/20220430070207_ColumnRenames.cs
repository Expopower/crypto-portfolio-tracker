﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CryptoPortfolioTracker.Migrations
{
    public partial class ColumnRenames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NetValueInCad",
                table: "TaxableEvents",
                newName: "NetValueInFiat");

            migrationBuilder.RenameColumn(
                name: "IsMissingTransactionAmountInCad",
                table: "TaxableEvents",
                newName: "IsMissingTransactionValueInFiat");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NetValueInFiat",
                table: "TaxableEvents",
                newName: "NetValueInCad");

            migrationBuilder.RenameColumn(
                name: "IsMissingTransactionValueInFiat",
                table: "TaxableEvents",
                newName: "IsMissingTransactionAmountInCad");
        }
    }
}
