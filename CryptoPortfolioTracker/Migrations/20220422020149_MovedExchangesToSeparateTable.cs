﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CryptoPortfolioTracker.Migrations
{
    public partial class MovedExchangesToSeparateTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("41125d37-1b31-4bf3-9639-e7856fb89e05"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("b8ad71fb-f038-4b1b-adbd-27194bd51f07"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("d7def278-909d-49b1-b5db-a5ce0d8caf4a"));

            migrationBuilder.CreateTable(
                name: "CurrencyReferenceExchanges",
                columns: table => new
                {
                    CurrencyReferenceExchangeID = table.Column<Guid>(type: "TEXT", nullable: false),
                    ExchangeName = table.Column<string>(type: "TEXT", nullable: true),
                    ExchangeCurrencyType = table.Column<int>(type: "INTEGER", nullable: false),
                    FromCurrencyID = table.Column<Guid>(type: "TEXT", nullable: false),
                    ToCurrencyID = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrencyReferenceExchanges", x => x.CurrencyReferenceExchangeID);
                    table.ForeignKey(
                        name: "FK_CurrencyReferenceExchanges_Currencies_FromCurrencyID",
                        column: x => x.FromCurrencyID,
                        principalTable: "Currencies",
                        principalColumn: "CurrencyID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CurrencyReferenceExchanges_Currencies_ToCurrencyID",
                        column: x => x.ToCurrencyID,
                        principalTable: "Currencies",
                        principalColumn: "CurrencyID",
                        onDelete: ReferentialAction.Cascade);
                });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("1f5078be-a381-4705-b07e-b31d1d247b57"), "USDC", 0, "USD Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("499c0586-f096-4b0d-8b09-36ac56d27eb3"), "ETH", 0, "Ethereum" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("a66a840e-6b48-4689-95f5-79a0122fd6ef"), "CRO", 0, "Crypto.com Coin" });

            migrationBuilder.CreateIndex(
                name: "IX_CurrencyReferenceExchanges_FromCurrencyID_ToCurrencyID",
                table: "CurrencyReferenceExchanges",
                columns: new[] { "FromCurrencyID", "ToCurrencyID" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CurrencyReferenceExchanges_ToCurrencyID",
                table: "CurrencyReferenceExchanges",
                column: "ToCurrencyID");

            migrationBuilder.Sql(@"
                INSERT INTO CurrencyReferenceExchanges (CurrencyReferenceExchangeID, ExchangeCurrencyType, ExchangeName, FromCurrencyID, ToCurrencyID)
                SELECT
	                (
		                -- Pseudo-GUID generation
		                hex(randomblob(4)) ||  '-'
		                || hex(randomblob(2)) ||  '-'
		                || hex(randomblob(2)) ||  '-'
		                || hex(randomblob(2)) ||  '-'
		                || hex(randomblob(6))
	                ),
	                c.CurrencyType,
	                c.ReferenceExchange,
	                c.CurrencyID,
	                (
		                CASE WHEN c.CurrencyType = 0 -- Crypto
			                THEN 'F69C219E-EE7C-48F2-BC1D-77B3AFE70F5D' -- USD
			                ELSE '758C888A-0CE2-45B3-A863-434796C451CE' -- CAD
		                END
	                )
                FROM
	                Currencies c
                WHERE
	                c.ReferenceExchange IS NOT NULL");

            migrationBuilder.DropColumn(
                name: "ReferenceExchange",
                table: "Currencies");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CurrencyReferenceExchanges");

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("1f5078be-a381-4705-b07e-b31d1d247b57"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("499c0586-f096-4b0d-8b09-36ac56d27eb3"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("a66a840e-6b48-4689-95f5-79a0122fd6ef"));

            migrationBuilder.AddColumn<string>(
                name: "ReferenceExchange",
                table: "Currencies",
                type: "TEXT",
                nullable: true);

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("41125d37-1b31-4bf3-9639-e7856fb89e05"), "CRO", 0, "Crypto.com Coin", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("b8ad71fb-f038-4b1b-adbd-27194bd51f07"), "ETH", 0, "Ethereum", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("d7def278-909d-49b1-b5db-a5ce0d8caf4a"), "USDC", 0, "USD Coin", null });
        }
    }
}
