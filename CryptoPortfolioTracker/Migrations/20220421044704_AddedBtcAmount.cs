﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CryptoPortfolioTracker.Migrations
{
    public partial class AddedBtcAmount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("6a4fc4c6-e805-4bab-b75c-c173a12c09eb"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("801eb8ad-ad32-4a19-b2ba-947f7c48a303"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("8e24b0be-035a-4088-9f00-18c0fa856df3"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("a359c3bc-63b6-460e-99a9-0dbc2fb1e81f"));

            migrationBuilder.AddColumn<decimal>(
                name: "AmountInBtc",
                table: "Transactions",
                type: "TEXT",
                nullable: true);

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("41125d37-1b31-4bf3-9639-e7856fb89e05"), "CRO", 0, "Crypto.com Coin", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("5b7573a6-95c3-4869-bc32-0642f7c130c2"), "BTC", 0, "Bitcoin", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("b8ad71fb-f038-4b1b-adbd-27194bd51f07"), "ETH", 0, "Ethereum", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("d7def278-909d-49b1-b5db-a5ce0d8caf4a"), "USDC", 0, "USD Coin", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("41125d37-1b31-4bf3-9639-e7856fb89e05"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("5b7573a6-95c3-4869-bc32-0642f7c130c2"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("b8ad71fb-f038-4b1b-adbd-27194bd51f07"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("d7def278-909d-49b1-b5db-a5ce0d8caf4a"));

            migrationBuilder.DropColumn(
                name: "AmountInBtc",
                table: "Transactions");

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("6a4fc4c6-e805-4bab-b75c-c173a12c09eb"), "BTC", 0, "Bitcoin", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("801eb8ad-ad32-4a19-b2ba-947f7c48a303"), "CRO", 0, "Crypto.com Coin", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("8e24b0be-035a-4088-9f00-18c0fa856df3"), "USDC", 0, "USD Coin", null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange" },
            //    values: new object[] { new Guid("a359c3bc-63b6-460e-99a9-0dbc2fb1e81f"), "ETH", 0, "Ethereum", null });
        }
    }
}
