﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CryptoPortfolioTracker.Migrations
{
    public partial class MovedTransactionValuesToNewTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("2b16871e-b124-410a-8eef-93b8a4932291"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("52b22707-6283-4d05-9e64-2b405a4431f2"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("acfbd265-92c7-440f-a44b-753853f47f67"));

            //migrationBuilder.RenameColumn(
            //    name: "AmountInCad",
            //    table: "Transactions",
            //    newName: "FiatValueID");

            //migrationBuilder.RenameColumn(
            //    name: "AmountInBtc",
            //    table: "Transactions",
            //    newName: "CryptoValueID");

            migrationBuilder.AddColumn<string>(
                name: "FiatValueID",
                table: "Transactions",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CryptoValueID",
                table: "Transactions",
                type: "TEXT",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TransactionValues",
                columns: table => new
                {
                    TransactionValueID = table.Column<Guid>(type: "TEXT", nullable: false),
                    Amount = table.Column<decimal>(type: "TEXT", nullable: false),
                    TransactionID = table.Column<Guid>(type: "TEXT", nullable: false),
                    CurrencyID = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionValues", x => x.TransactionValueID);
                    table.ForeignKey(
                        name: "FK_TransactionValues_Currencies_CurrencyID",
                        column: x => x.CurrencyID,
                        principalTable: "Currencies",
                        principalColumn: "CurrencyID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.Sql(@"
                INSERT INTO TransactionValues (TransactionValueID, Amount, TransactionID, CurrencyID)
				SELECT
	                (
		                -- Pseudo-GUID generation
		                hex(randomblob(4)) ||  '-'
		                || hex(randomblob(2)) ||  '-'
		                || hex(randomblob(2)) ||  '-'
		                || hex(randomblob(2)) ||  '-'
		                || hex(randomblob(6))
	                ),
	                t.AmountInCad,
	                t.TransactionID,
	                '758C888A-0CE2-45B3-A863-434796C451CE' -- CAD
                FROM
	                Transactions t
                WHERE
	                t.AmountInCad IS NOT NULL");

            migrationBuilder.Sql(@"
                INSERT INTO TransactionValues (TransactionValueID, Amount, TransactionID, CurrencyID)
				SELECT
	                (
		                -- Pseudo-GUID generation
		                hex(randomblob(4)) ||  '-'
		                || hex(randomblob(2)) ||  '-'
		                || hex(randomblob(2)) ||  '-'
		                || hex(randomblob(2)) ||  '-'
		                || hex(randomblob(6))
	                ),
	                t.AmountInBtc,
	                t.TransactionID,
	                '5B7573A6-95C3-4869-BC32-0642F7C130C2' -- BTC
                FROM
	                Transactions t
                WHERE
	                t.AmountInBtc IS NOT NULL");

            migrationBuilder.Sql(@"
                UPDATE Transactions
                SET
	                CryptoValueID = (
		                SELECT
			                tv.TransactionValueID
		                FROM
			                TransactionValues tv
		                WHERE
			                tv.TransactionID = Transactions.TransactionID
			                AND tv.CurrencyID = '5B7573A6-95C3-4869-BC32-0642F7C130C2' -- BTC
		                LIMIT 1
	                )");

            migrationBuilder.Sql(@"
                UPDATE Transactions
                SET
	                FiatValueID = (
		                SELECT
			                tv.TransactionValueID
		                FROM
			                TransactionValues tv
		                WHERE
			                tv.TransactionID = Transactions.TransactionID
			                AND tv.CurrencyID = '758C888A-0CE2-45B3-A863-434796C451CE' -- CAD
		                LIMIT 1
	                )");

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("71e2e84c-04ad-4e9f-a1a9-9871f97a1a34"), "USDC", 0, "USD Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("a05489fc-3975-457c-96a5-8926b92ee12c"), "CRO", 0, "Crypto.com Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("c191812c-23b6-401c-8980-d659585802a1"), "ETH", 0, "Ethereum" });

            migrationBuilder.DropColumn(
                name: "AmountInCad",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "AmountInBtc",
                table: "Transactions");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_CryptoValueID",
                table: "Transactions",
                column: "CryptoValueID");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_FiatValueID",
                table: "Transactions",
                column: "FiatValueID");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionValues_CurrencyID",
                table: "TransactionValues",
                column: "CurrencyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_TransactionValues_CryptoValueID",
                table: "Transactions",
                column: "CryptoValueID",
                principalTable: "TransactionValues",
                principalColumn: "TransactionValueID");

            migrationBuilder.AddForeignKey(
                name: "FK_Transactions_TransactionValues_FiatValueID",
                table: "Transactions",
                column: "FiatValueID",
                principalTable: "TransactionValues",
                principalColumn: "TransactionValueID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_TransactionValues_CryptoValueID",
                table: "Transactions");

            migrationBuilder.DropForeignKey(
                name: "FK_Transactions_TransactionValues_FiatValueID",
                table: "Transactions");

            migrationBuilder.DropTable(
                name: "TransactionValues");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_CryptoValueID",
                table: "Transactions");

            migrationBuilder.DropIndex(
                name: "IX_Transactions_FiatValueID",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "FiatValueID",
                table: "Transactions");

            migrationBuilder.DropColumn(
                name: "CryptoValueID",
                table: "Transactions");

            migrationBuilder.AddColumn<string>(
                name: "AmountInCad",
                table: "Transactions",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AmountInBtc",
                table: "Transactions",
                type: "TEXT",
                nullable: true);

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("71e2e84c-04ad-4e9f-a1a9-9871f97a1a34"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("a05489fc-3975-457c-96a5-8926b92ee12c"));

            //migrationBuilder.DeleteData(
            //    table: "Currencies",
            //    keyColumn: "CurrencyID",
            //    keyValue: new Guid("c191812c-23b6-401c-8980-d659585802a1"));

            //migrationBuilder.RenameColumn(
            //    name: "FiatValueID",
            //    table: "Transactions",
            //    newName: "AmountInCad");

            //migrationBuilder.RenameColumn(
            //    name: "CryptoValueID",
            //    table: "Transactions",
            //    newName: "AmountInBtc");

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("2b16871e-b124-410a-8eef-93b8a4932291"), "CRO", 0, "Crypto.com Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("52b22707-6283-4d05-9e64-2b405a4431f2"), "USDC", 0, "USD Coin" });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name" },
            //    values: new object[] { new Guid("acfbd265-92c7-440f-a44b-753853f47f67"), "ETH", 0, "Ethereum" });
        }
    }
}
