﻿// <auto-generated />
using System;
using CryptoPortfolioTracker;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace CryptoPortfolioTracker.Migrations
{
    [DbContext(typeof(CryptoPortfolioContext))]
    partial class CryptoPortfolioContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "6.0.1");

            modelBuilder.Entity("CryptoPortfolioTracker.Models.Currency", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("CurrencyCode")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("CurrencyType")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.HasIndex("CurrencyCode", "CurrencyType")
                        .IsUnique();

                    b.ToTable("Currencies");

                    b.HasData(
                        new
                        {
                            ID = new Guid("758c888a-0ce2-45b3-a863-434796c451ce"),
                            CurrencyCode = "CAD",
                            CurrencyType = 1,
                            Name = "Canadian Dollars"
                        },
                        new
                        {
                            ID = new Guid("f69c219e-ee7c-48f2-bc1d-77b3afe70f5d"),
                            CurrencyCode = "USD",
                            CurrencyType = 1,
                            Name = "United States Dollars"
                        },
                        new
                        {
                            ID = new Guid("f16fc08c-973a-422c-a600-b1c65381ec0f"),
                            CurrencyCode = "CRO",
                            CurrencyType = 0,
                            Name = "Crypto.com Coin"
                        },
                        new
                        {
                            ID = new Guid("5b7573a6-95c3-4869-bc32-0642f7c130c2"),
                            CurrencyCode = "BTC",
                            CurrencyType = 0,
                            Name = "Bitcoin"
                        },
                        new
                        {
                            ID = new Guid("aca57051-72ae-4eab-8191-fef754132d98"),
                            CurrencyCode = "ETH",
                            CurrencyType = 0,
                            Name = "Ethereum"
                        },
                        new
                        {
                            ID = new Guid("237a9a8b-737b-4b34-b03e-3165e1c52eb3"),
                            CurrencyCode = "USDC",
                            CurrencyType = 0,
                            Name = "USD Coin"
                        },
                        new
                        {
                            ID = new Guid("521d94ae-b1a1-4912-b9cb-0aef8158161b"),
                            CurrencyCode = "TCAD",
                            CurrencyType = 0,
                            Name = "TrueCAD"
                        });
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.CurrencyRate", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("EffectiveDateTime")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("FromCurrencyID")
                        .HasColumnType("TEXT");

                    b.Property<decimal>("Rate")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("ToCurrencyID")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.HasIndex("ToCurrencyID");

                    b.HasIndex("FromCurrencyID", "ToCurrencyID", "EffectiveDateTime")
                        .IsUnique();

                    b.ToTable("CurrencyRates");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.CurrencyRateRetrievalDateRange", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("EndDate")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("FromCurrencyID")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("StartDate")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("ToCurrencyID")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.HasIndex("ToCurrencyID");

                    b.HasIndex("FromCurrencyID", "ToCurrencyID")
                        .IsUnique();

                    b.ToTable("CurrencyRateRetrievalDateRanges");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.CurrencyReferenceExchange", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<int>("ExchangeCurrencyType")
                        .HasColumnType("INTEGER");

                    b.Property<string>("ExchangeName")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("FromCurrencyID")
                        .HasColumnType("TEXT");

                    b.Property<bool>("IsInverted")
                        .HasColumnType("INTEGER");

                    b.Property<Guid>("ToCurrencyID")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.HasIndex("ToCurrencyID");

                    b.HasIndex("FromCurrencyID", "ToCurrencyID")
                        .IsUnique();

                    b.ToTable("CurrencyReferenceExchanges");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.TaxableEvent", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("CreatedDateTime")
                        .HasColumnType("TEXT");

                    b.Property<string>("Description")
                        .HasColumnType("TEXT");

                    b.Property<int>("EventType")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("IsDisabled")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("IsMissingTransactionValueInFiat")
                        .HasColumnType("INTEGER");

                    b.Property<bool?>("IsModified")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasDefaultValue(true);

                    b.Property<decimal?>("NetValueInFiat")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.ToTable("TaxableEvents");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.Transaction", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<decimal>("Amount")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("CurrencyID")
                        .HasColumnType("TEXT");

                    b.Property<decimal?>("OverrideFiatValue")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("TaxableEventID")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.HasIndex("CurrencyID");

                    b.HasIndex("TaxableEventID");

                    b.ToTable("Transactions");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.TransactionValue", b =>
                {
                    b.Property<Guid>("ID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<decimal>("Amount")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("CurrencyID")
                        .HasColumnType("TEXT");

                    b.Property<Guid>("TransactionID")
                        .HasColumnType("TEXT");

                    b.HasKey("ID");

                    b.HasIndex("TransactionID");

                    b.HasIndex("CurrencyID", "TransactionID")
                        .IsUnique();

                    b.ToTable("TransactionValues");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.CurrencyRate", b =>
                {
                    b.HasOne("CryptoPortfolioTracker.Models.Currency", "FromCurrency")
                        .WithMany()
                        .HasForeignKey("FromCurrencyID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CryptoPortfolioTracker.Models.Currency", "ToCurrency")
                        .WithMany()
                        .HasForeignKey("ToCurrencyID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("FromCurrency");

                    b.Navigation("ToCurrency");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.CurrencyRateRetrievalDateRange", b =>
                {
                    b.HasOne("CryptoPortfolioTracker.Models.Currency", "FromCurrency")
                        .WithMany()
                        .HasForeignKey("FromCurrencyID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CryptoPortfolioTracker.Models.Currency", "ToCurrency")
                        .WithMany()
                        .HasForeignKey("ToCurrencyID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("FromCurrency");

                    b.Navigation("ToCurrency");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.CurrencyReferenceExchange", b =>
                {
                    b.HasOne("CryptoPortfolioTracker.Models.Currency", "FromCurrency")
                        .WithMany("ReferenceExchanges")
                        .HasForeignKey("FromCurrencyID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CryptoPortfolioTracker.Models.Currency", "ToCurrency")
                        .WithMany()
                        .HasForeignKey("ToCurrencyID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("FromCurrency");

                    b.Navigation("ToCurrency");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.Transaction", b =>
                {
                    b.HasOne("CryptoPortfolioTracker.Models.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyID")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("CryptoPortfolioTracker.Models.TaxableEvent", "TaxableEvent")
                        .WithMany("Transactions")
                        .HasForeignKey("TaxableEventID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Currency");

                    b.Navigation("TaxableEvent");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.TransactionValue", b =>
                {
                    b.HasOne("CryptoPortfolioTracker.Models.Currency", "Currency")
                        .WithMany()
                        .HasForeignKey("CurrencyID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("CryptoPortfolioTracker.Models.Transaction", "Transaction")
                        .WithMany("TransactionValues")
                        .HasForeignKey("TransactionID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Currency");

                    b.Navigation("Transaction");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.Currency", b =>
                {
                    b.Navigation("ReferenceExchanges");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.TaxableEvent", b =>
                {
                    b.Navigation("Transactions");
                });

            modelBuilder.Entity("CryptoPortfolioTracker.Models.Transaction", b =>
                {
                    b.Navigation("TransactionValues");
                });
#pragma warning restore 612, 618
        }
    }
}
