﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CryptoPortfolioTracker.Migrations
{
    public partial class NormalizedIDNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TransactionValueID",
                table: "TransactionValues",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "TransactionID",
                table: "Transactions",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "TaxableEventID",
                table: "TaxableEvents",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "CurrencyReferenceExchangeID",
                table: "CurrencyReferenceExchanges",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "CurrencyRateID",
                table: "CurrencyRates",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "CurrencyRateRetrievalDateRangeID",
                table: "CurrencyRateRetrievalDateRanges",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "CurrencyID",
                table: "Currencies",
                newName: "ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ID",
                table: "TransactionValues",
                newName: "TransactionValueID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Transactions",
                newName: "TransactionID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "TaxableEvents",
                newName: "TaxableEventID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "CurrencyReferenceExchanges",
                newName: "CurrencyReferenceExchangeID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "CurrencyRates",
                newName: "CurrencyRateID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "CurrencyRateRetrievalDateRanges",
                newName: "CurrencyRateRetrievalDateRangeID");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Currencies",
                newName: "CurrencyID");
        }
    }
}
