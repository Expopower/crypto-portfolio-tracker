﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CryptoPortfolioTracker.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Currencies",
                columns: table => new
                {
                    CurrencyID = table.Column<Guid>(type: "TEXT", nullable: false),
                    CurrencyType = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrencyCode = table.Column<string>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    ReferenceExchange = table.Column<string>(type: "TEXT", nullable: true),
                    RetrievedRatesStartDateTime = table.Column<DateTime>(type: "TEXT", nullable: true),
                    RetrievedRatesEndDateTime = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currencies", x => x.CurrencyID);
                });

            migrationBuilder.CreateTable(
                name: "TaxableEvents",
                columns: table => new
                {
                    TaxableEventID = table.Column<Guid>(type: "TEXT", nullable: false),
                    EventType = table.Column<int>(type: "INTEGER", nullable: false),
                    NetValueInCad = table.Column<decimal>(type: "TEXT", nullable: true),
                    IsMissingTransactionAmountInCad = table.Column<bool>(type: "INTEGER", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: true),
                    CreatedDateTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    IsModified = table.Column<bool>(type: "INTEGER", nullable: true, defaultValue: true),
                    IsDisabled = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxableEvents", x => x.TaxableEventID);
                });

            migrationBuilder.CreateTable(
                name: "CurrencyRates",
                columns: table => new
                {
                    CurrencyRateID = table.Column<Guid>(type: "TEXT", nullable: false),
                    Rate = table.Column<decimal>(type: "TEXT", nullable: false),
                    EffectiveDateTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    FromCurrencyID = table.Column<Guid>(type: "TEXT", nullable: false),
                    ToCurrencyID = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrencyRates", x => x.CurrencyRateID);
                    table.ForeignKey(
                        name: "FK_CurrencyRates_Currencies_FromCurrencyID",
                        column: x => x.FromCurrencyID,
                        principalTable: "Currencies",
                        principalColumn: "CurrencyID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CurrencyRates_Currencies_ToCurrencyID",
                        column: x => x.ToCurrencyID,
                        principalTable: "Currencies",
                        principalColumn: "CurrencyID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    TransactionID = table.Column<Guid>(type: "TEXT", nullable: false),
                    Amount = table.Column<decimal>(type: "TEXT", nullable: false),
                    OverrideAmountInCad = table.Column<decimal>(type: "TEXT", nullable: true),
                    AmountInCad = table.Column<decimal>(type: "TEXT", nullable: true),
                    TaxableEventID = table.Column<Guid>(type: "TEXT", nullable: false),
                    CurrencyID = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.TransactionID);
                    table.ForeignKey(
                        name: "FK_Transactions_Currencies_CurrencyID",
                        column: x => x.CurrencyID,
                        principalTable: "Currencies",
                        principalColumn: "CurrencyID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Transactions_TaxableEvents_TaxableEventID",
                        column: x => x.TaxableEventID,
                        principalTable: "TaxableEvents",
                        principalColumn: "TaxableEventID",
                        onDelete: ReferentialAction.Cascade);
                });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange", "RetrievedRatesEndDateTime", "RetrievedRatesStartDateTime" },
            //    values: new object[] { new Guid("43839011-fbf5-42ce-b775-c5621dcac704"), "ETH", 0, "Ethereum", null, null, null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange", "RetrievedRatesEndDateTime", "RetrievedRatesStartDateTime" },
            //    values: new object[] { new Guid("521d94ae-b1a1-4912-b9cb-0aef8158161b"), "TCAD", 0, "TrueCAD", null, null, null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange", "RetrievedRatesEndDateTime", "RetrievedRatesStartDateTime" },
            //    values: new object[] { new Guid("5b7573a6-95c3-4869-bc32-0642f7c130c2"), "BTC", 0, "Bitcoin", null, null, null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange", "RetrievedRatesEndDateTime", "RetrievedRatesStartDateTime" },
            //    values: new object[] { new Guid("758c888a-0ce2-45b3-a863-434796c451ce"), "CAD", 1, "Canadian Dollars", null, null, null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange", "RetrievedRatesEndDateTime", "RetrievedRatesStartDateTime" },
            //    values: new object[] { new Guid("83dccace-c0e7-4e41-b8be-55ba876e0ce1"), "CRO", 0, "Crypto.com Coin", null, null, null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange", "RetrievedRatesEndDateTime", "RetrievedRatesStartDateTime" },
            //    values: new object[] { new Guid("a23497e0-c85b-4b9f-bbe1-05ebe1ac3b3f"), "USDC", 0, "USD Coin", null, null, null });

            //migrationBuilder.InsertData(
            //    table: "Currencies",
            //    columns: new[] { "CurrencyID", "CurrencyCode", "CurrencyType", "Name", "ReferenceExchange", "RetrievedRatesEndDateTime", "RetrievedRatesStartDateTime" },
            //    values: new object[] { new Guid("f69c219e-ee7c-48f2-bc1d-77b3afe70f5d"), "USD", 1, "United States Dollars", null, null, null });

            migrationBuilder.CreateIndex(
                name: "IX_Currencies_CurrencyCode_CurrencyType",
                table: "Currencies",
                columns: new[] { "CurrencyCode", "CurrencyType" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CurrencyRates_FromCurrencyID_ToCurrencyID_EffectiveDateTime",
                table: "CurrencyRates",
                columns: new[] { "FromCurrencyID", "ToCurrencyID", "EffectiveDateTime" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CurrencyRates_ToCurrencyID",
                table: "CurrencyRates",
                column: "ToCurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_CurrencyID",
                table: "Transactions",
                column: "CurrencyID");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_TaxableEventID",
                table: "Transactions",
                column: "TaxableEventID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CurrencyRates");

            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "Currencies");

            migrationBuilder.DropTable(
                name: "TaxableEvents");
        }
    }
}
