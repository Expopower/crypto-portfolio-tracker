﻿using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryptoPortfolioTracker.UserControls
{
    /// <summary>
    /// Interaction logic for DialogControls.xaml
    /// </summary>
    public partial class DialogControls : UserControl
    {
        public delegate Task AsyncEventHandler(object sender, EventArgs e);
        public event AsyncEventHandler? SuccessAsync;
        public event EventHandler? Success;
        public event EventHandler? Failure;

        public static readonly DependencyProperty SuccessActionLabelProperty =
            DependencyProperty.Register(
                name: "SuccessActionLabel",
                propertyType: typeof(string),
                ownerType: typeof(DialogControls));

        public static readonly DependencyProperty FailureActionLabelProperty =
            DependencyProperty.Register(
                name: "FailureActionLabel",
                propertyType: typeof(string),
                ownerType: typeof(DialogControls));

        public string SuccessActionLabel
        {
            get
            {
                return (string)GetValue(SuccessActionLabelProperty);
            }
            set
            {
                SetValue(SuccessActionLabelProperty, value);
            }
        }

        public string FailureActionLabel
        {
            get
            {
                return (string)GetValue(FailureActionLabelProperty);
            }
            set
            {
                SetValue(FailureActionLabelProperty, value);
            }
        }

        public DialogControls()
        {
            InitializeComponent();
        }

        public void Reset()
        {
            SuccessButton.IsEnabled = true;
            FailureButton.IsEnabled = true;
            SuccessText.Visibility = Visibility.Visible;
            LoadingIcon.Visibility = Visibility.Hidden;
        }

        private void SuccessButton_Click(object sender, RoutedEventArgs e)
        {
            SuccessButton.IsEnabled = false;
            FailureButton.IsEnabled = false;

            if (SuccessAsync != null)
            {
                SuccessText.Visibility = Visibility.Hidden;
                LoadingIcon.Visibility = Visibility.Visible;

                Dispatcher.Invoke(async () =>
                {
                    try
                    {
                        await SuccessAsync(this, EventArgs.Empty);
                    }
                    catch (Exception ex)
                    {
                        // Necessary to handle exception explicitly, or it will get swallowed up on the other thread
                        // TODO: Perhaps implement a more graceful way to handle exceptions before it gets to this point?
                        MessageBox.Show(TextUtility.BuildExceptionMessage(ex), "Unhandled Exception",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                });
            }
            else if (Success != null)
            {
                Success(this, EventArgs.Empty);
            }
        }

        private void FailureButton_Click(object sender, RoutedEventArgs e)
        {
            EventHandler handler = Failure;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}
