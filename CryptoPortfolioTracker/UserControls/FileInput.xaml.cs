﻿using CryptoPortfolioTracker.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryptoPortfolioTracker.UserControls
{
    /// <summary>
    /// Interaction logic for FileInput.xaml
    /// </summary>
    public partial class FileInput : UserControl
    {
        private FileInputViewModel ViewModel
        {
            get
            {
                return (FileInputViewModel)DataContext;
            }
        }

        public FileInput()
        {
            InitializeComponent();
        }

        private void FileDialogButton_Click(object sender, RoutedEventArgs e)
        {
            // Ideally the control wouldn't know about the ViewModel being bound
            // to it, but this seemed like the most straightforward way to pass
            // along the value.
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == true)
            {
                ViewModel.FilePath = dialog.FileName;
            }
        }
    }
}
