﻿using CryptoPortfolioTracker.DataImporters;
using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.Windows;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace CryptoPortfolioTracker.UserControls
{
    /// <summary>
    /// Interaction logic for TopMenu.xaml
    /// </summary>
    public partial class TopMenu : UserControl
    {
        public TopMenu()
        {
            InitializeComponent();
        }

        private void OpenDatabaseFolderMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer", DatabaseFileService.DbDirectory);
        }

        private void ChangeDatabaseFolderMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog()
            {
                Title = "Change Database Location",
                InitialDirectory = DatabaseFileService.DbDirectory,
                IsFolderPicker = true
            };

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                DatabaseFileService.CreateCheckpoint();
                DatabaseFileService.UpdateDatabaseDirectory(dialog.FileName);
            }
        }

        private void SaveDatabaseBackupMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new SaveFileDialog()
            {
                Title = "Save Database Backup",
                InitialDirectory = DatabaseFileService.DbDirectory,
                FileName = DatabaseFileService.GetDefaultBackupFileName(),
                Filter = DatabaseFileService.DbDialogFilter,
                DefaultExt = DatabaseFileService.DbDialogExt
            };

            if (dialog.ShowDialog() == true)
            {
                DatabaseFileService.CreateCheckpoint();
                DatabaseFileService.CopyDatabaseFile(dialog.FileName);
            }
        }

        private void LoadDatabaseFromFileMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new OpenFileDialog()
            {
                Title = "Load Database File",
                InitialDirectory = DatabaseFileService.DbDirectory,
                Filter = DatabaseFileService.DbDialogFilter,
                DefaultExt = DatabaseFileService.DbDialogExt
            };

            if (dialog.ShowDialog() == true)
            {
                DatabaseFileService.ReplaceDatabaseFile(dialog.FileName);
            }
        }

        private void ManageCryptoCurrenciesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            new CurrencyListWindow(CurrencyType.Crypto)
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ManageFiatCurrenciesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            new CurrencyListWindow(CurrencyType.Fiat)
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void PreferredCurrenciesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            new PreferredCurrenciesWindow()
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ImportCryptoComTransactions_Click(object sender, RoutedEventArgs e)
        {
            new ImportTaxableEventsWindow(new CryptoComTransactionsDataImporter())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ImportCryptoComCardHistory_Click(object sender, RoutedEventArgs e)
        {
            new ImportTaxableEventsWindow(new CryptoComCardHistoryDataImporter())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ImportCryptoComExchangeStakingHistory_Click(object sender, RoutedEventArgs e)
        {
            new ImportTaxableEventsWindow(new CryptoComExchangeStakingHistoryDataImporter())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ImportCryptoComExchangeSpotTrades_Click(object sender, RoutedEventArgs e)
        {
            new ImportTaxableEventsWindow(new CryptoComExchangeSpotTradesDataImporter())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ImportCryptoComExchangeTradingFeeRebates_Click(object sender, RoutedEventArgs e)
        {
            new ImportTaxableEventsWindow(new CryptoComExchangeTradingFeeRebatesImporter())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ImportCryptoComExchangeDepositsWithdrawals_Click(object sender, RoutedEventArgs e)
        {
            new ImportTaxableEventsWindow(new CryptoComExchangeDepositsWithdrawalsImporter())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ImportCryptoComExchangeDeposits_Click(object sender, RoutedEventArgs e)
        {
            new ImportTaxableEventsWindow(new CryptoComExchangeDepositsImporter())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ImportCryptoComExchangeWithdrawals_Click(object sender, RoutedEventArgs e)
        {
            new ImportTaxableEventsWindow(new CryptoComExchangeWithdrawalsImporter())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ImportCryptoComExchangeWalletTransactions_Click(object sender, RoutedEventArgs e)
        {
            new ImportTaxableEventsWindow(new CryptoComExchangeWalletTransactionsImporter())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ImportNexoTransations_Click(object sender, RoutedEventArgs e)
        {
            new ImportTaxableEventsWindow(new NexoTransactionsDataImporter())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }
    }
}
