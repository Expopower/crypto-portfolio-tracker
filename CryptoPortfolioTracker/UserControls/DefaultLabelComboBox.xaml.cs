﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryptoPortfolioTracker.UserControls
{
    /// <summary>
    /// Interaction logic for DefaultLabelComboBox.xaml
    /// </summary>
    public partial class DefaultLabelComboBox : UserControl
    {
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(DefaultLabelComboBox));

        public static readonly DependencyProperty SelectedValueProperty =
            DependencyProperty.Register("SelectedValue", typeof(object), typeof(DefaultLabelComboBox));

        public static readonly DependencyProperty SelectedIndexProperty =
            DependencyProperty.Register("SelectedIndex", typeof(int), typeof(DefaultLabelComboBox));

        public static readonly DependencyProperty ChildVerticalAlignmentProperty =
            DependencyProperty.Register("ChildVerticalAlignment", typeof(VerticalAlignment), typeof(DefaultLabelComboBox));

        public static readonly DependencyProperty DefaultLabelTextProperty =
            DependencyProperty.Register("DefaultLabelText", typeof(string), typeof(DefaultLabelComboBox));

        public IEnumerable ItemsSource {
            get
            {
                return (IEnumerable)GetValue(ItemsSourceProperty);
            }
            set
            {
                SetValue(ItemsSourceProperty, value);
            }
        }

        public object SelectedValue
        {
            get
            {
                return GetValue(SelectedValueProperty);
            }
            set
            {
                SetValue(SelectedValueProperty, value);
            }
        }

        public int SelectedIndex
        {
            get
            {
                return (int)GetValue(SelectedIndexProperty);
            }
            set
            {
                SetValue(SelectedIndexProperty, value);
            }
        }

        public VerticalAlignment ChildVerticalAlignment
        {
            get
            {
                return (VerticalAlignment)GetValue(ChildVerticalAlignmentProperty);
            }
            set
            {
                SetValue(ChildVerticalAlignmentProperty, value);
            }
        }

        public string DefaultLabelText
        {
            get
            {
                return (string)GetValue(DefaultLabelTextProperty);
            }
            set
            {
                SetValue(DefaultLabelTextProperty, value);
            }
        }

        public DefaultLabelComboBox()
        {
            InitializeComponent();
        }
    }
}
