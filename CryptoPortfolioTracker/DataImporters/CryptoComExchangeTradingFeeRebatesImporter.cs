﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    internal class CryptoComExchangeTradingFeeRebatesImporter : TaxableEventDataImporter
    {
        public override string DialogTitle
        {
            get
            {
                return "Import Crypto.com Exchange Trading Fee Rebates";
            }
        }
        public override string DialogNote
        {
            get
            {
                return "Duplicates are assessed based on matching descriptions and dates.";
            }
        }
        public override string SaveActionLabel
        {
            get
            {
                return "Import Trading Fee Rebates";
            }
        }

        public override async Task<(List<TaxableEvent>, List<TaxableEvent>, string?)> ImportDataAsync(
            string filePath, DateTime? afterDate, DateTime? beforeDate)
        {
            try
            {
                var taxableEvents = new List<TaxableEvent>();
                var possibleDuplicates = new List<TaxableEvent>();

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var importedRebates = csv.GetRecordsAsync<CryptoComExchangeTradingFeeRebate>();

                    await foreach (CryptoComExchangeTradingFeeRebate importedRebate in importedRebates)
                    {
                        if (!ShouldImportRebate(importedRebate, afterDate, beforeDate))
                        {
                            continue;
                        }

                        var taxableEvent = new TaxableEvent()
                        {
                            CreatedDateTime = importedRebate.Timestamp.ToLocalTime(),
                            EventType = TaxableEventType.Cashback,
                            Description = GetDescription(importedRebate)
                        };

                        Transaction transaction = await CreateTransactionAsync(
                            importedRebate.RebateAmount, importedRebate.RebateCurrencyCode, CurrencyType.Crypto, taxableEvent);

                        taxableEvent.Transactions.Add(transaction);
                        taxableEvents.Add(taxableEvent);
                    }

                    possibleDuplicates = await GetDuplicatesOfExistingEventsAsync(taxableEvents);
                }

                return (taxableEvents, possibleDuplicates, null);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        private string GetDescription(CryptoComExchangeTradingFeeRebate importedRebate)
        {
            return $"Trading fee rebate from Crypto.com exchange ({importedRebate.RebatePercentage} " +
                $"of {importedRebate.FeeAmount} {importedRebate.FeeCurrencyCode})";
        }

        private bool ShouldImportRebate(CryptoComExchangeTradingFeeRebate rebate, DateTime? afterDate, DateTime? beforeDate)
        {
            return IsWithinDateRange(rebate.Timestamp, afterDate, beforeDate);
        }

        private class CryptoComExchangeTradingFeeRebate
        {
            [Name("Time (UTC)")]
            public DateTime Timestamp { get; set; }

            [Name("Trading Fee Currency")]
            public string FeeCurrencyCode { get; set; }

            [Name("Trading Fee Paid")]
            public decimal FeeAmount { get; set; }

            [Name("Rebate Currency")]
            public string RebateCurrencyCode { get; set; }

            [Name("Rebate Amount")]
            public decimal RebateAmount { get; set; }

            [Name("Rebate Percentage")]
            public string RebatePercentage { get; set; }
        }
    }
}
