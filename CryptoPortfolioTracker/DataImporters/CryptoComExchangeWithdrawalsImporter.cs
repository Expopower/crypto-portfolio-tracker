﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    internal class CryptoComExchangeWithdrawalsImporter : TaxableEventDataImporter
    {
        public override string DialogTitle
        {
            get
            {
                return "Import Crypto.com Exchange Withdrawals";
            }
        }
        public override string DialogNote
        {
            get
            {
                return "Duplicates are assessed based on matching descriptions and dates.";
            }
        }
        public override string SaveActionLabel
        {
            get
            {
                return "Import Withdrawals";
            }
        }

        public override async Task<(List<TaxableEvent>, List<TaxableEvent>, string?)> ImportDataAsync(
            string filePath, DateTime? afterDate, DateTime? beforeDate)
        {
            try
            {
                var taxableEvents = new List<TaxableEvent>();
                var possibleDuplicates = new List<TaxableEvent>();

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var importedWithdrawals = csv.GetRecordsAsync<CryptoComExchangeWithdrawal>();

                    await foreach (CryptoComExchangeWithdrawal importedWithdrawal in importedWithdrawals)
                    {
                        if (!ShouldImportWithdrawal(importedWithdrawal, afterDate, beforeDate))
                        {
                            continue;
                        }

                        var taxableEvent = new TaxableEvent()
                        {
                            CreatedDateTime = importedWithdrawal.Timestamp.ToLocalTime(),
                            EventType = TaxableEventType.Variable,
                            Description = GetDescription(importedWithdrawal),
                            
                            // Generally withdrawals shouldn't factor into calculations, but should be retained for historical reasons
                            IsDisabled = true
                        };

                        Transaction transaction = await CreateTransactionAsync(
                            importedWithdrawal.Amount, importedWithdrawal.CurrencyCode, CurrencyType.Crypto, taxableEvent);

                        taxableEvent.Transactions.Add(transaction);
                        taxableEvents.Add(taxableEvent);

                        // Create separate taxable event for the fee
                        if (importedWithdrawal.Fee > 0)
                        {
                            var feeTaxableEvent = new TaxableEvent()
                            {
                                CreatedDateTime = importedWithdrawal.Timestamp.ToLocalTime(),
                                EventType = TaxableEventType.Fee,
                                Description = GetFeeDescription(importedWithdrawal)
                            };

                            Transaction feeTransaction = await CreateTransactionAsync(
                                -importedWithdrawal.Fee, importedWithdrawal.CurrencyCode, CurrencyType.Crypto, feeTaxableEvent);

                            feeTaxableEvent.Transactions.Add(feeTransaction);
                            taxableEvents.Add(feeTaxableEvent);
                        }
                    }

                    possibleDuplicates = await GetDuplicatesOfExistingEventsAsync(taxableEvents);
                }

                return (taxableEvents, possibleDuplicates, null);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        private string GetDescription(CryptoComExchangeWithdrawal importedWithdrawal)
        {
            return $"{importedWithdrawal.CurrencyCode} withdrawal from Crypto.com exchange to " +
                $"{importedWithdrawal.Address} (TX: {importedWithdrawal.TransactionID})";
        }

        private string GetFeeDescription(CryptoComExchangeWithdrawal importedWithdrawal)
        {
            return $"Withdrawal fee for {importedWithdrawal.CurrencyCode} withdrawal from Crypto.com exchange";
        }

        private bool ShouldImportWithdrawal(CryptoComExchangeWithdrawal withdrawal, DateTime? afterDate, DateTime? beforeDate)
        {
            return IsWithinDateRange(withdrawal.Timestamp, afterDate, beforeDate);
        }

        private class CryptoComExchangeWithdrawal
        {
            [Name("Time (UTC)")]
            public DateTime Timestamp { get; set; }

            [Name("Coin")]
            public string CurrencyCode { get; set; }

            [Name("Withdrawal Amount")]
            public decimal Amount { get; set; }

            [Name("Fee")]
            public decimal Fee { get; set; }

            [Name("Withdrawal Address")]
            public string Address { get; set; }

            [Name("Txid")]
            public string TransactionID { get; set; }
        }
    }
}
