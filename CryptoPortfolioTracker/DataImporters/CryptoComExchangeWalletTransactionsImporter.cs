﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    internal class CryptoComExchangeWalletTransactionsImporter : TaxableEventDataImporter
    {
        public override string DialogTitle
        {
            get
            {
                return "Import Crypto.com Exchange Wallet Transactions";
            }
        }
        public override string DialogNote
        {
            get
            {
                return "Duplicates are assessed based on matching descriptions and dates.";
            }
        }
        public override string SaveActionLabel
        {
            get
            {
                return "Import Transactions";
            }
        }

        public override async Task<(List<TaxableEvent>, List<TaxableEvent>, string?)> ImportDataAsync(
            string filePath, DateTime? afterDate, DateTime? beforeDate)
        {
            try
            {
                var taxableEvents = new List<TaxableEvent>();
                var possibleDuplicates = new List<TaxableEvent>();
                var skippedTransactionTypes = new HashSet<string>();

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var importedTransactions = csv.GetRecordsAsync<CryptoComExchangeWalletTransaction>();

                    await foreach (CryptoComExchangeWalletTransaction importedTransaction in importedTransactions)
                    {
                        if (GetTaxableEventType(importedTransaction) == TaxableEventType.Unknown)
                        {
                            skippedTransactionTypes.Add(importedTransaction.Type);
                        }

                        if (!ShouldImportTransaction(importedTransaction, afterDate, beforeDate))
                        {
                            continue;
                        }

                        var taxableEvent = new TaxableEvent()
                        {
                            CreatedDateTime = importedTransaction.Timestamp.ToLocalTime(),
                            EventType = GetTaxableEventType(importedTransaction),
                            Description = GetDescription(importedTransaction)
                        };

                        // For now, there is no expectation that there will be Fiat transactions imported
                        Transaction fromTransaction = await CreateTransactionAsync(
                            importedTransaction.Amount, importedTransaction.CurrencyCode, CurrencyType.Crypto, taxableEvent);

                        taxableEvent.Transactions.Add(fromTransaction);
                        taxableEvents.Add(taxableEvent);
                    }

                    possibleDuplicates = await GetDuplicatesOfExistingEventsAsync(taxableEvents);
                }

                string? importMessage = null;
                if (skippedTransactionTypes.Any())
                {
                    importMessage = "Found unsupported transaction types:\n" + string.Join("\n", skippedTransactionTypes);
                }

                return (taxableEvents, possibleDuplicates, importMessage);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        private string GetDescription(CryptoComExchangeWalletTransaction importedTransaction)
        {
            return $"{importedTransaction.Type} - {importedTransaction.Amount} {importedTransaction.CurrencyCode}";
        }

        private bool ShouldImportTransaction(CryptoComExchangeWalletTransaction transaction, DateTime? afterDate, DateTime? beforeDate)
        {
            if (!IsWithinDateRange(transaction.Timestamp, afterDate, beforeDate))
            {
                return false;
            }

            if (GetTaxableEventType(transaction) == TaxableEventType.Unknown)
            {
                Trace.WriteLine($"Unsupported transaction kind {transaction.Type} from transaction on {transaction.Timestamp}.");
                return false;
            }

            return true;
        }

        private TaxableEventType GetTaxableEventType(CryptoComExchangeWalletTransaction importedTransaction)
        {
            switch (importedTransaction.Type)
            {
                case "SOFT_STAKE_REWARD":
                    return TaxableEventType.Interest;
                default:
                    return TaxableEventType.Unknown;
            }
        }

        private class CryptoComExchangeWalletTransaction
        {
            [Name("Time")]
            public string TimeRaw { get; set; }

            [Ignore]
            public DateTime Timestamp
            {
                get
                {
                    return DateTime.Parse(TimeRaw);
                }
            }

            [Name("Instrument")]
            public string CurrencyCode { get; set; }

            [Name("Type")]
            public string Type { get; set; }

            [Name("Side")]
            public string Side { get; set; }

            [Name("Quantity")]
            public decimal Amount { get; set; }

            [Name("Transaction Cost")]
            public decimal Cost { get; set; }

            [Name("Realized PnL (USD)")]
            public string ProfitLoss { get; set; }

            [Name("Order ID")]
            public string OrderID { get; set; }

            [Name("Trade ID")]
            public string TradeID { get; set; }
        }
    }
}
