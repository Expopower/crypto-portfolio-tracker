﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    internal class CryptoComExchangeSpotTradesDataImporter : TaxableEventDataImporter
    {
        public override string DialogTitle
        {
            get
            {
                return "Import Crypto.com Exchange Spot Trades";
            }
        }
        public override string DialogNote
        {
            get
            {
                return "Duplicates are assessed based on matching descriptions and dates. " +
                    "Note: Trades from CRO to BTC require manual correction of fees.";
            }
        }
        public override string SaveActionLabel
        {
            get
            {
                return "Import Spot Trades";
            }
        }

        public override async Task<(List<TaxableEvent>, List<TaxableEvent>, string?)> ImportDataAsync(
            string filePath, DateTime? afterDate, DateTime? beforeDate)
        {
            try
            {
                var taxableEvents = new List<TaxableEvent>();
                var possibleDuplicates = new List<TaxableEvent>();

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var importedTrades = csv.GetRecordsAsync<CryptoComExchangeSpotTrade>();

                    await foreach (CryptoComExchangeSpotTrade importedTrade in importedTrades)
                    {
                        if (!ShouldImportTrade(importedTrade, afterDate, beforeDate))
                        {
                            continue;
                        }

                        var taxableEvent = new TaxableEvent()
                        {
                            CreatedDateTime = importedTrade.Timestamp.ToLocalTime(),
                            EventType = TaxableEventType.Exchange,
                            Description = GetDescription(importedTrade)
                        };

                        Transaction toTransaction;
                        Transaction fromTransaction;

                        Transaction transaction1 = await CreateTransactionAsync(
                            importedTrade.Amount1, importedTrade.CurrencyCode1, CurrencyType.Crypto, taxableEvent);
                        Transaction transaction2 = await CreateTransactionAsync(
                            importedTrade.Amount2, importedTrade.CurrencyCode2, CurrencyType.Crypto, taxableEvent);

                        // Depending on the trade type, the transaction roles may be switched
                        if (importedTrade.Side == "SELL")
                        {
                            fromTransaction = transaction1;
                            toTransaction = transaction2;
                        }
                        else
                        {
                            toTransaction = transaction1;
                            fromTransaction = transaction2;
                        }

                        // Note: For now, we assume that the fees are paid from the acquired asset
                        fromTransaction.Amount *= -1;
                        toTransaction.Amount -= importedTrade.FeeAmount;
                        await new TransactionService().SynchronizeToAndFromTransactionsAsync(
                            toTransaction, fromTransaction, taxableEvent.EventType, taxableEvent.CreatedDateTime);

                        taxableEvent.Transactions.Add(toTransaction);
                        taxableEvent.Transactions.Add(fromTransaction);
                        taxableEvents.Add(taxableEvent);
                    }

                    possibleDuplicates = await GetDuplicatesOfExistingEventsAsync(taxableEvents);
                }

                return (taxableEvents, possibleDuplicates, null);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        private string GetDescription(CryptoComExchangeSpotTrade importedTrade)
        {
            return $"{importedTrade.Side} {importedTrade.Symbol} (trade {importedTrade.TradeID} for order {importedTrade.OrderID})";
        }

        private bool ShouldImportTrade(CryptoComExchangeSpotTrade trade, DateTime? afterDate, DateTime? beforeDate)
        {
            return IsWithinDateRange(trade.Timestamp, afterDate, beforeDate);
        }

        private class CryptoComExchangeSpotTrade
        {
            [Name("Time (UTC)")]
            public DateTime Timestamp { get; set; }

            [Name("Order ID")]
            public string OrderID { get; set; }

            [Name("Trade ID")]
            public string TradeID { get; set; }

            [Name("Symbol")]
            public string Symbol { get; set; }

            [Ignore]
            public string CurrencyCode1
            {
                get
                {
                    return Symbol.Split("_")[0];
                }
            }

            [Ignore]
            public string CurrencyCode2
            {
                get
                {
                    return Symbol.Split("_")[1];
                }
            }

            [Name("Side")]
            public string Side { get; set; }

            [Name("Trade Amount")]
            public decimal Amount1 { get; set; }

            [Name("Volume of Business")]
            public decimal Amount2 { get; set; }

            [Name("Fee")]
            public decimal FeeAmount { get; set; }

            [Name("Fee Currency")]
            public string FeeCurrencyCode { get; set; }
        }
    }
}
