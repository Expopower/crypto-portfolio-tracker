﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    public abstract class DataImporter<T> : IDataImporter<T>
    {
        public abstract string DialogTitle { get; }
        public abstract string DialogNote { get; }
        public abstract string SaveActionLabel { get; }
        public abstract string BaseConfirmationMessage { get; }

        public abstract Task<(List<T>, List<T>, string?)> ImportDataAsync(string filePath, DateTime? afterDate, DateTime? beforeDate);
        public abstract Task SaveImportedDataAsync(List<T> importedData);
    }
}
