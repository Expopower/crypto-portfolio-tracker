﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    internal class CryptoComTransactionsDataImporter : TaxableEventDataImporter
    {
        public override string DialogTitle
        {
            get
            {
                return "Import Crypto.com Transactions";
            }
        }
        public override string DialogNote
        {
            get
            {
                return "Duplicates are assessed based on matching descriptions and dates.";
            }
        }
        public override string SaveActionLabel
        {
            get
            {
                return "Import Transactions";
            }
        }

        public override async Task<(List<TaxableEvent>, List<TaxableEvent>, string?)> ImportDataAsync(
            string filePath, DateTime? afterDate, DateTime? beforeDate)
        {
            try
            {
                var taxableEvents = new List<TaxableEvent>();
                var possibleDuplicates = new List<TaxableEvent>();
                var skippedTransactionTypes = new HashSet<string>();

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var importedTransactions = csv.GetRecordsAsync<CryptoComTransaction>();

                    await foreach (CryptoComTransaction importedTransaction in importedTransactions)
                    {
                        if (GetTaxableEventType(importedTransaction) == TaxableEventType.Unknown)
                        {
                            skippedTransactionTypes.Add(importedTransaction.TransactionKind);
                        }

                        if (!ShouldImportTransaction(importedTransaction, afterDate, beforeDate))
                        {
                            continue;
                        }

                        var taxableEvent = new TaxableEvent()
                        {
                            CreatedDateTime = importedTransaction.Timestamp.ToLocalTime(),
                            EventType = GetTaxableEventType(importedTransaction),
                            Description = GetDescription(importedTransaction),
                            IsDisabled = ShouldDisable(importedTransaction)
                        };

                        CurrencyType effectiveFromType = taxableEvent.EventType == TaxableEventType.Purchase
                            ? CurrencyType.Fiat
                            : CurrencyType.Crypto;
                        Transaction fromTransaction = await CreateTransactionAsync(
                            importedTransaction.FromAmount, importedTransaction.FromCurrencyCode, effectiveFromType, taxableEvent);

                        taxableEvent.Transactions.Add(fromTransaction);

                        // To transaction is optional based on nature of the event
                        string? effectiveToCurrencyCode = GetToCurrencyCode(importedTransaction);
                        decimal? effectiveToAmount = GetToAmount(importedTransaction);
                        if (!string.IsNullOrWhiteSpace(effectiveToCurrencyCode)
                            && effectiveToAmount.HasValue)
                        {
                            CurrencyType effectiveToType = taxableEvent.EventType == TaxableEventType.Sale
                                ? CurrencyType.Fiat
                                : CurrencyType.Crypto;
                            Transaction toTransaction = await CreateTransactionAsync(
                                effectiveToAmount.Value, effectiveToCurrencyCode, effectiveToType, taxableEvent);
                            await new TransactionService().SynchronizeToAndFromTransactionsAsync(
                                toTransaction, fromTransaction, taxableEvent.EventType, taxableEvent.CreatedDateTime);

                            taxableEvent.Transactions.Add(toTransaction);
                        }

                        taxableEvents.Add(taxableEvent);
                    }

                    possibleDuplicates = await GetDuplicatesOfExistingEventsAsync(taxableEvents);
                }

                string? importMessage = null;
                if (skippedTransactionTypes.Any())
                {
                    importMessage = "Found unsupported transaction types:\n" + string.Join("\n", skippedTransactionTypes);
                }

                return (taxableEvents, possibleDuplicates, importMessage);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        private string GetDescription(CryptoComTransaction importedTransaction)
        {
            return $"{importedTransaction.Description} ({importedTransaction.TransactionKind})";
        }

        private string? GetToCurrencyCode(CryptoComTransaction importedTransaction)
        {
            switch (importedTransaction.TransactionKind)
            {
                // For some reason, the ToCurrencyCode is incorrect for a crypto payment
                case "crypto_payment":
                    return importedTransaction.NativeCurrency;
                default:
                    return importedTransaction.ToCurrencyCode;
            }
        }

        private decimal? GetToAmount(CryptoComTransaction importedTransaction)
        {
            switch (importedTransaction.TransactionKind)
            {
                // For some reason, the ToAmount is incorrect for a crypto payment
                case "crypto_payment":
                    return importedTransaction.NativeAmount;
                default:
                    return importedTransaction.ToAmount;
            }
        }

        private bool ShouldImportTransaction(CryptoComTransaction transaction, DateTime? afterDate, DateTime? beforeDate)
        {
            // TODO: Possibly expand to generic data validation implementation for data imports?
            if (string.IsNullOrWhiteSpace(transaction.FromCurrencyCode)
                || string.IsNullOrWhiteSpace(transaction.TransactionKind))
            {
                Trace.WriteLine($"Missing key data from transaction on {transaction.Timestamp}.");
                return false;
            }

            if (!IsWithinDateRange(transaction.Timestamp, afterDate, beforeDate))
            {
                return false;
            }

            if (GetTaxableEventType(transaction) == TaxableEventType.Unknown)
            {
                Trace.WriteLine($"Unsupported transaction kind {transaction.TransactionKind} from transaction on {transaction.Timestamp}.");
                return false;
            }

            return true;
        }

        private bool ShouldDisable(CryptoComTransaction importedTransaction)
        {
            switch (importedTransaction.TransactionKind)
            {
                case "exchange_to_crypto_transfer":
                case "crypto_to_exchange_transfer":
                    return true;
                default:
                    return false;
            }
        }

        private TaxableEventType GetTaxableEventType(CryptoComTransaction importedTransaction)
        {
            switch (importedTransaction.TransactionKind)
            {
                case "crypto_exchange":
                    return TaxableEventType.Exchange;
                case "card_top_up":
                case "crypto_payment":
                    return TaxableEventType.Sale;
                case "referral_card_cashback":
                case "gift_card_reward":
                    return TaxableEventType.Cashback;
                case "crypto_earn_interest_paid":
                case "mco_stake_reward":
                case "supercharger_reward_to_app_credited":
                    return TaxableEventType.Interest;
                case "rewards_platform_deposit_credited":
                case "admin_wallet_credited":
                    return TaxableEventType.Bonus;
                case "crypto_deposit":
                case "card_cashback_reverted":
                case "exchange_to_crypto_transfer":
                case "crypto_to_exchange_transfer":
                case "crypto_withdrawal":
                    return TaxableEventType.Variable;
                default:
                    return TaxableEventType.Unknown;
            }
        }

        private class CryptoComTransaction
        {
            [Name("Timestamp (UTC)")]
            public DateTime Timestamp { get; set; }

            [Name("Transaction Description")]
            public string Description { get; set; }

            [Name("Currency")]
            public string FromCurrencyCode { get; set; }

            [Name("Amount")]
            public decimal FromAmount { get; set; }

            [Name("To Currency")]
            public string? ToCurrencyCode { get; set; }

            [Name("Native Currency")]
            public string NativeCurrency { get; set; }

            [Name("Native Amount")]
            public decimal NativeAmount { get; set; }

            [Name("To Amount")]
            public decimal? ToAmount { get; set; }

            [Name("Transaction Kind")]
            public string TransactionKind { get; set; }
        }
    }
}
