﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    internal class CryptoComCardHistoryDataImporter : TaxableEventDataImporter
    {
        public override string DialogTitle
        {
            get
            {
                return "Import Crypto.com Card History";
            }
        }
        public override string DialogNote
        {
            get
            {
                return "Does not create new records; Crypto.com transactions should be imported first." +
                    " Duplicates are assessed based on whether Taxable Events already appear updated.";
            }
        }
        public override string SaveActionLabel
        {
            get
            {
                return "Import Card History";
            }
        }
        public override string BaseConfirmationMessage
        {
            get
            {
                return "Update {0} existing Taxable Event(s)?";
            }
        }

        public override async Task<(List<TaxableEvent>, List<TaxableEvent>, string?)> ImportDataAsync(
            string filePath, DateTime? afterDate, DateTime? beforeDate)
        {
            try
            {
                var taxableEvents = new List<TaxableEvent>();
                var possibleDuplicates = new List<TaxableEvent>();
                var unmatchedDates = new List<DateTime>();
                string importMessage = null;

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var importedRecords = csv.GetRecordsAsync<CryptoComCardRecord>();

                    await foreach (CryptoComCardRecord importedRecord in importedRecords)
                    {
                        // At this point in time, we only expect to import card top-ups (and treat them as Sale events)
                        if (!ShouldImportRecord(importedRecord, afterDate, beforeDate))
                        {
                            continue;
                        }

                        TaxableEvent? topUpEvent = await GetExistingTopUpTaxableEventAsync(importedRecord);
                        if (topUpEvent == null)
                        {
                            unmatchedDates.Add(importedRecord.Timestamp);
                            continue;
                        }

                        Transaction? cryptoTransaction = GetExistingCryptoTransaction(importedRecord, topUpEvent);
                        if (cryptoTransaction == null)
                        {
                            unmatchedDates.Add(importedRecord.Timestamp);
                            continue;
                        }
                        // Clear navigation property before save is attempted
                        cryptoTransaction.Currency = null;

                        if (cryptoTransaction.OverrideFiatValue.HasValue)
                        {
                            possibleDuplicates.Add(topUpEvent);
                        }

                        Transaction fiatTransaction = await CreateTransactionAsync(
                            importedRecord.FromAmount, importedRecord.FromCurrencyCode, CurrencyType.Fiat, topUpEvent);
                        await new TransactionService().SynchronizeToAndFromTransactionsAsync(
                            fiatTransaction, cryptoTransaction, topUpEvent.EventType, topUpEvent.CreatedDateTime);

                        topUpEvent.Transactions.Add(fiatTransaction);
                        topUpEvent.IsModified = true;
                        taxableEvents.Add(topUpEvent);
                    }
                }

                if (unmatchedDates.Any())
                {
                    importMessage = "Failed to match top-up transactions on the following dates:\n"
                        + string.Join("\n", unmatchedDates.Select(date => date.ToString("yyyy-MM-dd")));
                }

                return (taxableEvents, possibleDuplicates, importMessage);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        private bool ShouldImportRecord(CryptoComCardRecord record, DateTime? afterDate, DateTime? beforeDate)
        {
            if (string.IsNullOrWhiteSpace(record.FromCurrencyCode)
                || string.IsNullOrWhiteSpace(record.ToCurrencyCode)
                || string.IsNullOrWhiteSpace(record.Description)
                || record.Description.Contains("refund", StringComparison.OrdinalIgnoreCase)
                || !record.ToAmount.HasValue
                || record.ToAmount < 0)
            {
                return false;
            }

            if (!IsWithinDateRange(record.Timestamp, afterDate, beforeDate))
            {
                return false;
            }

            return true;
        }

        private async Task<TaxableEvent?> GetExistingTopUpTaxableEventAsync(CryptoComCardRecord cardRecord)
        {
            using (var db = new CryptoPortfolioContext())
            {
                // Select from date range since card records and transactions from Crypto.com do not always match exactly
                var upperBound = cardRecord.Timestamp.ToLocalTime().AddHours(23);
                var lowerBound = cardRecord.Timestamp.ToLocalTime().AddHours(-23);

                List<TaxableEvent> possibleMatches = await db.TaxableEvents
                    .Include(evt => evt.Transactions)
                    .ThenInclude(tx => tx.Currency)
                    .Where(evt => evt.CreatedDateTime <= upperBound
                        && evt.CreatedDateTime >= lowerBound
                        && evt.EventType == TaxableEventType.Sale
                        && evt.Description != null
                        && evt.Description.IndexOf("card_top_up") >= 0
                        && evt.Description.IndexOf(cardRecord.Description) >= 0)
                    .ToListAsync();

                // TODO: What if multiple matches are found? Is there no better way to determine uniqueness?
                return possibleMatches
                    .FirstOrDefault(evt => GetExistingCryptoTransaction(cardRecord, evt) != null);
            }
        }

        private Transaction? GetExistingCryptoTransaction(CryptoComCardRecord cardRecord, TaxableEvent taxableEvent)
        {
            // We don't have a way to directly associate transactions with card records, so they must be inferred
            // Strangely, the "To" fields contain Crypto information rather than the "From" fields in a Fiat -> Crypto top-up.
            // Additionally, neither the "To" nor "From" Amount is ever negative for a card top-up.
            return taxableEvent.Transactions
                .FirstOrDefault(tx =>
                    tx.Currency.CurrencyCode == cardRecord.ToCurrencyCode
                    && Math.Abs(tx.Amount) == cardRecord.ToAmount);
        }

        private class CryptoComCardRecord
        {
            [Name("Timestamp (UTC)")]
            public DateTime Timestamp { get; set; }

            [Name("Transaction Description")]
            public string Description { get; set; }

            [Name("Currency")]
            public string FromCurrencyCode { get; set; }

            [Name("Amount")]
            public decimal FromAmount { get; set; }

            [Name("To Currency")]
            public string? ToCurrencyCode { get; set; }

            [Name("To Amount")]
            public decimal? ToAmount { get; set; }
        }
    }
}
