﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    internal class NexoTransactionsDataImporter : TaxableEventDataImporter
    {
        public override string DialogTitle
        {
            get
            {
                return "Import Nexo Transactions";
            }
        }
        public override string DialogNote
        {
            get
            {
                return "Duplicates are assessed based on matching descriptions and dates.";
            }
        }
        public override string SaveActionLabel
        {
            get
            {
                return "Import Transactions";
            }
        }

        public override async Task<(List<TaxableEvent>, List<TaxableEvent>, string?)> ImportDataAsync(
            string filePath, DateTime? afterDate, DateTime? beforeDate)
        {
            try
            {
                var taxableEvents = new List<TaxableEvent>();
                var possibleDuplicates = new List<TaxableEvent>();
                var skippedTransactionTypes = new HashSet<string>();

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var importedTransactions = csv.GetRecordsAsync<NexoTransaction>();

                    await foreach (NexoTransaction importedTransaction in importedTransactions)
                    {
                        if (GetTaxableEventType(importedTransaction) == TaxableEventType.Unknown)
                        {
                            skippedTransactionTypes.Add(importedTransaction.Type);
                        }

                        if (!ShouldImportTransaction(importedTransaction, afterDate, beforeDate))
                        {
                            continue;
                        }

                        var taxableEvent = new TaxableEvent()
                        {
                            CreatedDateTime = importedTransaction.Timestamp.ToLocalTime(),
                            EventType = GetTaxableEventType(importedTransaction),
                            Description = GetDescription(importedTransaction)
                        };

                        // For now, there is no expectation that there will be Fiat transactions imported
                        Transaction fromTransaction = await CreateTransactionAsync(
                            importedTransaction.FromAmount, importedTransaction.FromCurrencyCode, CurrencyType.Crypto, taxableEvent);

                        taxableEvent.Transactions.Add(fromTransaction);

                        // To transaction should always be populated, but is only relevant for an Exchange
                        if (taxableEvent.EventType == TaxableEventType.Exchange
                            && !string.IsNullOrWhiteSpace(importedTransaction.ToCurrencyCode)
                            && importedTransaction.ToAmount.HasValue)
                        {
                            Transaction toTransaction = await CreateTransactionAsync(
                                importedTransaction.ToAmount.Value, importedTransaction.ToCurrencyCode, CurrencyType.Crypto, taxableEvent);
                            await new TransactionService().SynchronizeToAndFromTransactionsAsync(
                                toTransaction, fromTransaction, taxableEvent.EventType, taxableEvent.CreatedDateTime);

                            taxableEvent.Transactions.Add(toTransaction);
                        }

                        taxableEvents.Add(taxableEvent);
                    }

                    possibleDuplicates = await GetDuplicatesOfExistingEventsAsync(taxableEvents);
                }

                string? importMessage = null;
                if (skippedTransactionTypes.Any())
                {
                    importMessage = "Found unsupported transaction types:\n" + string.Join("\n", skippedTransactionTypes);
                }

                return (taxableEvents, possibleDuplicates, importMessage);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        private string GetDescription(NexoTransaction importedTransaction)
        {
            string currencyCode = importedTransaction.FromCurrencyCode == importedTransaction.ToCurrencyCode
                ? importedTransaction.FromCurrencyCode
                : $"{importedTransaction.FromCurrencyCode}/{importedTransaction.ToCurrencyCode}";

            return $"{currencyCode} {importedTransaction.Type} - " +
                $"{importedTransaction.Details.Split(" / ")[1]} ({importedTransaction.ID})";
        }

        private bool ShouldImportTransaction(NexoTransaction transaction, DateTime? afterDate, DateTime? beforeDate)
        {
            if (!IsWithinDateRange(transaction.Timestamp, afterDate, beforeDate))
            {
                return false;
            }

            if (GetTaxableEventType(transaction) == TaxableEventType.Unknown)
            {
                Console.WriteLine($"Unsupported transaction kind {transaction.Type} from transaction on {transaction.Timestamp}.");
                return false;
            }

            return true;
        }

        private TaxableEventType GetTaxableEventType(NexoTransaction importedTransaction)
        {
            switch (importedTransaction.Type)
            {
                case "Exchange":
                    return TaxableEventType.Exchange;
                case "Interest":
                case "FixedTermInterest":
                    return TaxableEventType.Interest;
                case "Exchange Cashback":
                case "ReferralBonus":
                    return TaxableEventType.Bonus;
                case "Deposit":
                case "Withdrawal":
                    return TaxableEventType.Variable;
                default:
                    return TaxableEventType.Unknown;
            }
        }

        private class NexoTransaction
        {
            [Name("Date / Time")]
            public DateTime Timestamp { get; set; }

            [Name("Transaction")]
            public string ID { get; set; }

            [Name("Details")]
            public string Details { get; set; }

            [Name("Input Currency")]
            public string FromCurrencyCode { get; set; }

            [Name("Input Amount")]
            public decimal FromAmount { get; set; }

            [Name("Output Currency")]
            public string? ToCurrencyCode { get; set; }

            [Name("Output Amount")]
            public decimal? ToAmount { get; set; }

            [Name("Type")]
            public string Type { get; set; }
        }
    }
}
