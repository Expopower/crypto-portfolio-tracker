﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    internal class CryptoComExchangeStakingHistoryDataImporter : TaxableEventDataImporter
    {
        public override string DialogTitle
        {
            get
            {
                return "Import Crypto.com Exchange Staking History";
            }
        }
        public override string DialogNote
        {
            get
            {
                return "Duplicates are assessed based on matching descriptions and dates.";
            }
        }
        public override string SaveActionLabel
        {
            get
            {
                return "Import Staking History";
            }
        }

        public override async Task<(List<TaxableEvent>, List<TaxableEvent>, string?)> ImportDataAsync(
            string filePath, DateTime? afterDate, DateTime? beforeDate)
        {
            try
            {
                var taxableEvents = new List<TaxableEvent>();
                var possibleDuplicates = new List<TaxableEvent>();

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var importedRecords = csv.GetRecordsAsync<CryptoComExchangeStakingRecord>();

                    await foreach (CryptoComExchangeStakingRecord importedRecord in importedRecords)
                    {
                        if (!ShouldImportRecord(importedRecord, afterDate, beforeDate))
                        {
                            continue;
                        }

                        var taxableEvent = new TaxableEvent()
                        {
                            CreatedDateTime = importedRecord.Timestamp.ToLocalTime(),
                            EventType = TaxableEventType.Interest,
                            Description = GetDescription(importedRecord)
                        };

                        // For now, there is no expectation that there will be Fiat transactions imported
                        Transaction fromTransaction = await CreateTransactionAsync(
                            importedRecord.InterestAmount, importedRecord.InterestCurrencyCode, CurrencyType.Crypto, taxableEvent);

                        taxableEvent.Transactions.Add(fromTransaction);
                        taxableEvents.Add(taxableEvent);
                    }

                    possibleDuplicates = await GetDuplicatesOfExistingEventsAsync(taxableEvents);
                }

                return (taxableEvents, possibleDuplicates, null);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        private string GetDescription(CryptoComExchangeStakingRecord importedRecord)
        {
            return $"Staking interest from {importedRecord.StakedAmount} {importedRecord.StakedCurrencyCode}" +
                $" at {importedRecord.AnnualPercentageRate} APR";
        }

        private bool ShouldImportRecord(CryptoComExchangeStakingRecord record, DateTime? afterDate, DateTime? beforeDate)
        {
            // TODO: Possibly expand to generic data validation implementation for data imports?
            if (string.IsNullOrWhiteSpace(record.StakedCurrencyCode)
                || string.IsNullOrWhiteSpace(record.InterestCurrencyCode))
            {
                Trace.WriteLine($"Missing key data from staking interest record on {record.Timestamp}.");
                return false;
            }

            if (!IsWithinDateRange(record.Timestamp, afterDate, beforeDate))
            {
                return false;
            }

            return true;
        }

        private class CryptoComExchangeStakingRecord
        {
            [Name("Time (UTC)")]
            public DateTime Timestamp { get; set; }

            [Name("Staked Currency")]
            [Optional]
            public string? StakedCurrency { get; set; }

            [Name("Stake Currency")]
            [Optional]
            public string? StakeCurrency { get; set; }

            [Ignore]
            public string StakedCurrencyCode
            {
                get
                {
                    return StakedCurrency ?? StakeCurrency ?? string.Empty;
                }
            }

            [Name("Staked Amount")]
            public decimal StakedAmount { get; set; }

            [Name("Interest Currency")]
            public string InterestCurrencyCode { get; set; }

            [Name("Interest Amount")]
            public decimal InterestAmount { get; set; }

            [Name("APR")]
            public string AnnualPercentageRate { get; set; }
        }
    }
}
