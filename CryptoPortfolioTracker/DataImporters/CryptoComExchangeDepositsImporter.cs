﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    internal class CryptoComExchangeDepositsImporter : TaxableEventDataImporter
    {
        private const string InternalAddress = "INTERNAL_DEPOSIT";
        public override string DialogTitle
        {
            get
            {
                return "Import Crypto.com Exchange Deposits";
            }
        }
        public override string DialogNote
        {
            get
            {
                return "Duplicates are assessed based on matching descriptions and dates.";
            }
        }
        public override string SaveActionLabel
        {
            get
            {
                return "Import Deposits";
            }
        }

        public override async Task<(List<TaxableEvent>, List<TaxableEvent>, string?)> ImportDataAsync(
            string filePath, DateTime? afterDate, DateTime? beforeDate)
        {
            try
            {
                var taxableEvents = new List<TaxableEvent>();
                var possibleDuplicates = new List<TaxableEvent>();

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var importedDeposits = csv.GetRecordsAsync<CryptoComExchangeDeposit>();

                    await foreach (CryptoComExchangeDeposit importedDeposit in importedDeposits)
                    {
                        if (!ShouldImportDeposit(importedDeposit, afterDate, beforeDate))
                        {
                            continue;
                        }

                        var taxableEvent = new TaxableEvent()
                        {
                            CreatedDateTime = importedDeposit.Timestamp.ToLocalTime(),
                            EventType = TaxableEventType.Variable,
                            Description = GetDescription(importedDeposit),
                            IsDisabled = ShouldDisable(importedDeposit)
                        };

                        Transaction transaction = await CreateTransactionAsync(
                            importedDeposit.Amount, importedDeposit.CurrencyCode, CurrencyType.Crypto, taxableEvent);

                        taxableEvent.Transactions.Add(transaction);
                        taxableEvents.Add(taxableEvent);
                    }

                    possibleDuplicates = await GetDuplicatesOfExistingEventsAsync(taxableEvents);
                }

                return (taxableEvents, possibleDuplicates, null);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        private string GetDescription(CryptoComExchangeDeposit importedDeposit)
        {
            return $"{importedDeposit.CurrencyCode} deposit to Crypto.com exchange at address {importedDeposit.Address} (TX: {importedDeposit.TransactionID})";
        }

        private bool ShouldDisable(CryptoComExchangeDeposit importedDeposit)
        {
            return importedDeposit.Address == InternalAddress;
        }

        private bool ShouldImportDeposit(CryptoComExchangeDeposit deposit, DateTime? afterDate, DateTime? beforeDate)
        {
            return IsWithinDateRange(deposit.Timestamp, afterDate, beforeDate);
        }

        private class CryptoComExchangeDeposit
        {
            [Name("Time (UTC)")]
            public DateTime Timestamp { get; set; }

            [Name("Coin")]
            public string CurrencyCode { get; set; }

            [Name("Deposit Amount")]
            public decimal Amount { get; set; }

            [Name("Deposit Address")]
            public string Address { get; set; }

            [Name("TxId")]
            public string TransactionID { get; set; }
        }
    }
}
