﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CsvHelper;
using CsvHelper.Configuration.Attributes;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    internal class CryptoComExchangeDepositsWithdrawalsImporter : TaxableEventDataImporter
    {
        public override string DialogTitle
        {
            get
            {
                return "Import Crypto.com Exchange Deposits and Withdrawals";
            }
        }
        public override string DialogNote
        {
            get
            {
                return "Duplicates are assessed based on matching descriptions and dates.";
            }
        }
        public override string SaveActionLabel
        {
            get
            {
                return "Import Deposits and Withdrawals";
            }
        }

        public override async Task<(List<TaxableEvent>, List<TaxableEvent>, string?)> ImportDataAsync(
            string filePath, DateTime? afterDate, DateTime? beforeDate)
        {
            try
            {
                var taxableEvents = new List<TaxableEvent>();
                var possibleDuplicates = new List<TaxableEvent>();

                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var importedActions = csv.GetRecordsAsync<CryptoComExchangeDepositWithdrawal>();

                    await foreach (CryptoComExchangeDepositWithdrawal importedAction in importedActions)
                    {
                        if (!ShouldImportWithdrawal(importedAction, afterDate, beforeDate))
                        {
                            continue;
                        }

                        var taxableEvent = new TaxableEvent()
                        {
                            CreatedDateTime = importedAction.Timestamp.ToLocalTime(),
                            EventType = TaxableEventType.Variable,
                            Description = GetDescription(importedAction),
                            
                            // Disable by default, since most such actions are retained for historical purposes
                            IsDisabled = true
                        };

                        Transaction transaction = await CreateTransactionAsync(
                            importedAction.Quantity, importedAction.CurrencyCode, CurrencyType.Crypto, taxableEvent);

                        taxableEvent.Transactions.Add(transaction);
                        taxableEvents.Add(taxableEvent);

                        // Create separate taxable event for the fee
                        if (importedAction.Fee > 0)
                        {
                            var feeTaxableEvent = new TaxableEvent()
                            {
                                CreatedDateTime = importedAction.Timestamp.ToLocalTime(),
                                EventType = TaxableEventType.Fee,
                                Description = GetFeeDescription(importedAction)
                            };

                            Transaction feeTransaction = await CreateTransactionAsync(
                                -importedAction.Fee, importedAction.CurrencyCode, CurrencyType.Crypto, feeTaxableEvent);

                            feeTaxableEvent.Transactions.Add(feeTransaction);
                            taxableEvents.Add(feeTaxableEvent);
                        }
                    }

                    possibleDuplicates = await GetDuplicatesOfExistingEventsAsync(taxableEvents);
                }

                return (taxableEvents, possibleDuplicates, null);
            }
            catch(Exception ex)
            {
                Trace.WriteLine(ex.Message);
                throw;
            }
        }

        private string GetDescription(CryptoComExchangeDepositWithdrawal importedWithdrawal)
        {
            return $"{importedWithdrawal.CurrencyCode} {importedWithdrawal.Type} on Crypto.com exchange. " +
                $"{importedWithdrawal.Information} (TX: {importedWithdrawal.TransactionID})";
        }

        private string GetFeeDescription(CryptoComExchangeDepositWithdrawal importedWithdrawal)
        {
            return $"Fee for {importedWithdrawal.CurrencyCode} {importedWithdrawal.Type} on Crypto.com exchange";
        }

        private bool ShouldImportWithdrawal(CryptoComExchangeDepositWithdrawal withdrawal, DateTime? afterDate, DateTime? beforeDate)
        {
            return IsWithinDateRange(withdrawal.Timestamp, afterDate, beforeDate);
        }

        private class CryptoComExchangeDepositWithdrawal
        {
            [Name("Time")]
            public string Time { get; set; }

            public DateTime Timestamp
            {
                get
                {
                    return DateTime.Parse(Regex.Match(Time, "\\((.*?) UTC\\)").Groups[1].Value);
                }
            }

            [Name("Coin")]
            public string Coin { get; set; }

            public string CurrencyCode
            {
                get
                {
                    return Regex.Match(Coin, "\\((.*?)\\)").Groups[1].Value;
                }
            }

            [Name("Type")]
            public string Type { get; set; }

            [Name("Quantity")]
            public decimal Quantity { get; set; }

            [Name("Fee")]
            public decimal Fee { get; set; }

            [Name("TXID")]
            public string TransactionID { get; set; }

            [Name("Information")]
            public string Information { get; set; }
        }
    }
}
