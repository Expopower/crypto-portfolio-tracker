﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    public abstract class TaxableEventDataImporter : DataImporter<TaxableEvent>
    {
        public override string BaseConfirmationMessage
        {
            get
            {
                return "Create {0} new Taxable Event(s)?";
            }
        }

        public override async Task SaveImportedDataAsync(List<TaxableEvent> importedData)
        {
            await new TaxableEventService().SaveTaxableEventsAsync(importedData);
        }

        protected async Task<Transaction> CreateTransactionAsync(decimal transactionAmount,
            string transactionCurrencyCode, CurrencyType currencyType, TaxableEvent taxableEvent)
        {
            Currency currency = await new CurrencyService().EnsureCurrencyForCodeAsync(
                transactionCurrencyCode, currencyType);

            return new Transaction()
            {
                TaxableEventID = taxableEvent.ID,
                Amount = transactionAmount,
                CurrencyID = currency.ID
            };
        }

        protected async Task<List<TaxableEvent>> GetDuplicatesOfExistingEventsAsync(List<TaxableEvent> importedEvents)
        {
            using (var db = new CryptoPortfolioContext())
            {
                HashSet<Tuple<DateTime, string?>> existingDescriptions =
                    (await db.TaxableEvents
                        .Select(evt => new Tuple<DateTime, string?>(evt.CreatedDateTime, evt.Description))
                        .ToListAsync())
                    .ToHashSet();

                return importedEvents
                    .Where(evt => existingDescriptions.Contains(
                        new Tuple<DateTime, string?>(evt.CreatedDateTime, evt.Description)))
                    .ToList();
            }
        }


        protected bool IsWithinDateRange(DateTime referenceDateTime, DateTime? afterDate, DateTime? beforeDate)
        {
            if ((afterDate.HasValue && referenceDateTime.ToLocalTime() < afterDate)
                || (beforeDate.HasValue && referenceDateTime.ToLocalTime() > DateTimeUtility.GetEndOfDay(beforeDate.Value)))
            {
                return false;
            }

            return true;
        }
    }
}
