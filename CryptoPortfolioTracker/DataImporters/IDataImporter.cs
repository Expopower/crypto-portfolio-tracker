﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.DataImporters
{
    public interface IDataImporter<T>
    {
        public string DialogTitle { get; }
        public string DialogNote { get; }
        public string SaveActionLabel { get; }
        public string BaseConfirmationMessage { get; }

        public Task<(List<T>, List<T>, string?)> ImportDataAsync(string filePath, DateTime? afterDate, DateTime? beforeDate);
        public Task SaveImportedDataAsync(List<T> importedData);
    }
}
