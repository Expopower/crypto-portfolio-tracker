﻿using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.Utilities;
using CryptoPortfolioTracker.ViewModels;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace CryptoPortfolioTracker.Views
{
    /// <summary>
    /// Interaction logic for TaxCalculationView.xaml
    /// </summary>
    public partial class TaxCalculationView : UserControl
    {
        private TaxCalculationViewModel ViewModel
        {
            get
            {
                return (TaxCalculationViewModel)DataContext;
            }
        }

        public TaxCalculationView()
        {
            InitializeComponent();
            DataContext = new TaxCalculationViewModel();
        }

        private void CalculateTaxableEarningsButton_Click(object sender, RoutedEventArgs e)
        {
            CalculateTaxableEarnings();
        }

        private void GenerateTaxableEarningsCsvButton_Click(object sender, RoutedEventArgs e)
        {
            GenerateCsv();
        }

        private void CalculateTaxableEarnings()
        {
            CalculateText.Visibility = Visibility.Hidden;
            CalculateLoadingIcon.Visibility = Visibility.Visible;
            CalculateTaxableEarningsButton.IsEnabled = false;

            Dispatcher.Invoke(async () =>
            {
                try
                {
                    await ViewModel.RefreshAsync();
                }
                catch (Exception ex)
                {
                    // Necessary to handle exception explicitly, or it will get swallowed up on the other thread
                    // TODO: Perhaps implement a more graceful way to handle exceptions before it gets to this point?
                    MessageBox.Show(TextUtility.BuildExceptionMessage(ex), "Unhandled Exception",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    CalculateText.Visibility = Visibility.Visible;
                    CalculateLoadingIcon.Visibility = Visibility.Hidden;
                    CalculateTaxableEarningsButton.IsEnabled = true;
                }
            });
        }

        private void GenerateCsv()
        {
            GenerateText.Visibility = Visibility.Hidden;
            GenerateLoadingIcon.Visibility = Visibility.Visible;
            GenerateTaxableEarningsCsvButton.IsEnabled = false;

            Dispatcher.Invoke(async () =>
            {
                try
                {
                    await ViewModel.GenerateCsvAsync();
                    Process.Start("explorer", DatabaseFileService.DbDirectory);
                }
                catch (Exception ex)
                {
                    // Necessary to handle exception explicitly, or it will get swallowed up on the other thread
                    // TODO: Perhaps implement a more graceful way to handle exceptions before it gets to this point?
                    MessageBox.Show(TextUtility.BuildExceptionMessage(ex), "Unhandled Exception",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    GenerateText.Visibility = Visibility.Visible;
                    GenerateLoadingIcon.Visibility = Visibility.Hidden;
                    GenerateTaxableEarningsCsvButton.IsEnabled = true;
                }
            });
        }
    }
}
