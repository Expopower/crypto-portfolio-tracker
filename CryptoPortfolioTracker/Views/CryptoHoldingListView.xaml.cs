﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.Utilities;
using CryptoPortfolioTracker.ViewModels;
using CryptoPortfolioTracker.Windows;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryptoPortfolioTracker.Views
{
    /// <summary>
    /// Interaction logic for CryptoHoldingListView.xaml
    /// </summary>
    public partial class CryptoHoldingListView : UserControl
    {
        private CryptoHoldingListViewModel ViewModel
        {
            get
            {
                return (CryptoHoldingListViewModel)DataContext;
            }
        }

        public CryptoHoldingListView()
        {
            InitializeComponent();
            DataContext = new CryptoHoldingListViewModel();
            
            // Note: Disabled for now to minimize API overuse
            RefreshCryptoHoldings(false);
        }

        private void RefreshCryptoHoldingsButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshCryptoHoldings(true);
        }

        private void RefreshCryptoHoldings(bool queryLatestRates)
        {
            LoadingIcon.Visibility = Visibility.Visible;
            RefreshCryptoHoldingsButton.IsEnabled = false;

            Dispatcher.Invoke(async () =>
            {
                try
                {
                    await ViewModel.RefreshAsync(queryLatestRates);
                }
                catch (Exception ex)
                {
                    // Necessary to handle exception explicitly, or it will get swallowed up on the other thread
                    // TODO: Perhaps implement a more graceful way to handle exceptions before it gets to this point?
                    MessageBox.Show(TextUtility.BuildExceptionMessage(ex), "Unhandled Exception",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    LoadingIcon.Visibility = Visibility.Hidden;
                    RefreshCryptoHoldingsButton.IsEnabled = true;
                }
            });
        }
    }
}
