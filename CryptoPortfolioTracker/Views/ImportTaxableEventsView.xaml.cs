﻿using CryptoPortfolioTracker.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryptoPortfolioTracker.Views
{
    /// <summary>
    /// Interaction logic for ImportTaxableEventsView.xaml
    /// </summary>
    public partial class ImportTaxableEventsView : UserControl
    {
        private ImportTaxableEventsViewModel ViewModel
        {
            get
            {
                return (ImportTaxableEventsViewModel)DataContext;
            }
        }

        public ImportTaxableEventsView()
        {
            InitializeComponent();
        }

        public bool CanImport()
        {
            return ViewModel.CanImport();
        }

        public async Task<bool> ImportAndSaveDataAsync()
        {
            try
            {
                await ViewModel.ImportDataAsync();

                MessageBoxResult confirmationResult = MessageBox.Show(
                    ViewModel.SaveConfirmationMessage, "Confirm Import", MessageBoxButton.OKCancel);

                if (confirmationResult == MessageBoxResult.OK)
                {
                    await ViewModel.SaveDataAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Failed to import data", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            return false;
        }
    }
}
