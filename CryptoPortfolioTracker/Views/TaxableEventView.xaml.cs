﻿using CryptoPortfolioTracker.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryptoPortfolioTracker.Views
{
    /// <summary>
    /// Interaction logic for TaxableEventView.xaml
    /// </summary>
    public partial class TaxableEventView : UserControl
    {
        private TaxableEventViewModel ViewModel
        {
            get
            {
                return (TaxableEventViewModel)DataContext;
            }
        }

        public TaxableEventView()
        {
            InitializeComponent();
        }

        public async Task SaveAsync()
        {
            ViewModel.IsModified = true;
            await ViewModel.SaveAsync();
        }
    }
}
