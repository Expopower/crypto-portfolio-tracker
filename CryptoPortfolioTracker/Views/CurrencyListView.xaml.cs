﻿using CryptoPortfolioTracker.Utilities;
using CryptoPortfolioTracker.ViewModels;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace CryptoPortfolioTracker.Views
{
    /// <summary>
    /// Interaction logic for CurrencyListView.xaml
    /// </summary>
    public partial class CurrencyListView : UserControl
    {
        private CurrencyListViewModel ViewModel
        {
            get
            {
                return (CurrencyListViewModel)DataContext;
            }
        }

        public CurrencyListView()
        {
            InitializeComponent();
        }

        public async Task SaveAsync()
        {
            await ViewModel.SaveAsync();
        }

        private void TrimRateRange_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(async () =>
            {
                try
                {
                    await ViewModel.TrimExchangeRateRange();
                }
                catch (Exception ex)
                {
                    // Necessary to handle exception explicitly, or it will get swallowed up on the other thread
                    // TODO: Perhaps implement a more graceful way to handle exceptions before it gets to this point?
                    MessageBox.Show(TextUtility.BuildExceptionMessage(ex), "Unhandled Exception",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            });
        }
    }
}
