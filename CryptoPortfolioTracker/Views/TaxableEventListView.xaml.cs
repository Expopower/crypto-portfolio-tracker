﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.Utilities;
using CryptoPortfolioTracker.ViewModels;
using CryptoPortfolioTracker.Windows;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CryptoPortfolioTracker.Views
{
    /// <summary>
    /// Interaction logic for TaxableEventsListView.xaml
    /// </summary>
    public partial class TaxableEventListView : UserControl
    {
        private TaxableEventListViewModel ViewModel
        {
            get
            {
                return (TaxableEventListViewModel)DataContext;
            }
        }

        public TaxableEventListView()
        {
            InitializeComponent();
            DataContext = new TaxableEventListViewModel();
        }

        private void NewEvent_Click(object sender, RoutedEventArgs e)
        {
            new TaxableEventWindow(new TaxableEventViewModel())
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditSelectedEvent();
        }

        private void CreateConcurrentEventMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel.SelectedTaxableEvent == null)
            {
                return;
            }

            new TaxableEventWindow(new TaxableEventViewModel()
            {
                CreatedDateTime = ViewModel.SelectedTaxableEvent.CreatedDateTime
            })
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void EditEventMenuItem_Click(object sender, RoutedEventArgs e)
        {
            EditSelectedEvent();
        }

        private void DeleteEventMenuItem_Click(object sender, RoutedEventArgs e)
        {
            DeletedSelectedTaxableEvents();
        }

        private void ListView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                DeletedSelectedTaxableEvents();
            }
        }

        private void EditSelectedEvent()
        {
            if (ViewModel.SelectedTaxableEvent == null)
            {
                return;
            }

            new TaxableEventWindow(ViewModel.SelectedTaxableEvent)
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void NewEventTemplates_Click(object sender, RoutedEventArgs e)
        {
            new TaxableEventTemplatesWindow
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            }
            .ShowDialog();
        }

        private void ClearModifiedEvents_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(async () =>
            {
                try
                {
                    await ViewModel.ClearModifiedTaxableEventsAsync();
                }
                catch (Exception ex)
                {
                    // Necessary to handle exception explicitly, or it will get swallowed up on the other thread
                    // TODO: Perhaps implement a more graceful way to handle exceptions before it gets to this point?
                    MessageBox.Show(TextUtility.BuildExceptionMessage(ex), "Unhandled Exception",
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
            });
        }

        private void DeletedSelectedTaxableEvents()
        {
            var taxableEvents = ListView.SelectedItems
                .Cast<TaxableEventViewModel>()
                .ToList();

            if (taxableEvents.Count > 0)
            {
                MessageBoxResult confirmationResult = MessageBox.Show(
                    $"Delete {taxableEvents.Count} Taxable Event(s)?", "Confirm Deletion", MessageBoxButton.YesNo);

                if (confirmationResult == MessageBoxResult.Yes)
                {
                    ViewModel.DeleteTaxableEvents(taxableEvents);
                }
            }
        }

        private void ClearFilters_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.ClearFilters();
        }

        private void EnsureCurrencyValues_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult confirmationResult = MessageBox.Show(
                $"Calculate all missing fiat/crypto values? This may be an expensive operation.", "Confirm Recalculation",
                MessageBoxButton.YesNo);

            if (confirmationResult == MessageBoxResult.Yes)
            {
                Dispatcher.Invoke(async () =>
                {
                    try
                    {
                        await ViewModel.EnsureCurrencyValues();
                    }
                    catch (Exception ex)
                    {
                        // Necessary to handle exception explicitly, or it will get swallowed up on the other thread
                        // TODO: Perhaps implement a more graceful way to handle exceptions before it gets to this point?
                        MessageBox.Show(TextUtility.BuildExceptionMessage(ex), "Unhandled Exception",
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                });
            }
        }
    }
}
