﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Utilities
{
    internal static class TextUtility
    {
        public static string BuildExceptionMessage(Exception e)
        {
            string message = e.ToString();

            if (e.InnerException != null)
            {
                message += $"\n\nInner exception: {BuildExceptionMessage(e.InnerException)}";
            }

            return message;
        }
    }
}
