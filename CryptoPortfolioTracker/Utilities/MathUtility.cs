﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Utilities
{
    internal static class MathUtility
    {
        public static decimal SafeDivide(decimal dividend, decimal divisor, decimal defaultValueIfNan = 0)
        {
            return divisor != 0
                ? dividend / divisor
                : defaultValueIfNan;
        }

        public static decimal? Abs(decimal? value)
        {
            return value.HasValue
                ? Math.Abs(value.Value)
                : null;
        }
    }
}
