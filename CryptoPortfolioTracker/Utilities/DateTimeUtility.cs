﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Utilities
{
    internal static class DateTimeUtility
    {
        public static long ConvertToUtcUnixTime(DateTime date)
        {
            return ((DateTimeOffset)date)
                .ToUniversalTime()
                .ToUnixTimeSeconds();
        }

        public static DateTime ConvertFromUtcUnixTime(long unixTime)
        {
            return DateTimeOffset.FromUnixTimeSeconds(unixTime)
                .LocalDateTime;
        }

        public static DateTime GetMin(DateTime a, DateTime b)
        {
            return new DateTime(Math.Min(a.Ticks, b.Ticks));
        }

        public static DateTime GetEndOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59);
        }
    }
}
