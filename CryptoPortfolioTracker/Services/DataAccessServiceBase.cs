﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Services
{
    /// <summary>
    /// Base class for services that require database access.
    /// </summary>
    public abstract class DataAccessServiceBase
    {
        private readonly DbContextOptions<CryptoPortfolioContext>? _dbOptions;

        protected DbContextOptions<CryptoPortfolioContext>? DbOptions
        {
            get
            {
                return _dbOptions;
            }
        }

        public DataAccessServiceBase()
        {
        }

        public DataAccessServiceBase(DbContextOptions<CryptoPortfolioContext>? dbOptions)
        {
            _dbOptions = dbOptions;
        }

        protected CryptoPortfolioContext GetDbContext()
        {
            if (_dbOptions == null)
            {
                return new CryptoPortfolioContext();
            }
            else
            {
                return new CryptoPortfolioContext(_dbOptions);
            }
        }

        /// <summary>
        /// Updates the original models list using the updated models list.
        /// Updates are done in-place.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="originalModels"></param>
        /// <param name="updatedModels"></param>
        /// <param name="applyAdditionalUpdate">Can be used to supply additional logic
        /// to execute on a per-item basis. Useful if a basic shallow update of values
        /// is insufficient.</param>
        internal void ApplyModelCollectionUpdates<T>(List<T> originalModels, List<T> updatedModels,
            Action<T, T>? applyAdditionalUpdate = null) where T : IModel
        {
            using (var db = GetDbContext())
            {
                originalModels.RemoveAll(originalModel =>
                    !updatedModels
                        .Any(updatedModel => updatedModel.ID == originalModel.ID));

                foreach (T updatedModel in updatedModels)
                {
                    T? originalModel = originalModels.
                        FirstOrDefault(originalModel =>
                            originalModel.ID == updatedModel.ID
                            && updatedModel.ID != Guid.Empty);

                    if (originalModel != null)
                    {
                        db.Entry(originalModel).CurrentValues.SetValues(updatedModel);
                        if (applyAdditionalUpdate != null)
                        {
                            applyAdditionalUpdate(originalModel, updatedModel);
                        }
                    }
                    else
                    {
                        originalModels.Add(updatedModel);
                    }
                }
            }
        }

        internal async Task SaveModelsAsync<T>(List<T> latestModels) where T : class, IModel
        {
            if (latestModels == null)
            {
                throw new ArgumentNullException(nameof(latestModels));
            }

            HashSet<Guid> updatedModelIDs = latestModels
                .Select(model => model.ID)
                .ToHashSet();

            using (var db = GetDbContext())
            {
                DbSet<T> dbSet = db.Set<T>();

                List<T> originalModels = await dbSet.ToListAsync();
                List<T> deletedModels = originalModels
                    .Where(model => !updatedModelIDs.Contains(model.ID)
                        && model.ID != Guid.Empty)
                    .ToList();
                List<T> addedModels = latestModels
                    .Where(model => model.ID == Guid.Empty)
                    .ToList();

                dbSet.RemoveRange(deletedModels);
                dbSet.AddRange(addedModels);

                foreach (T latestModel in latestModels)
                {
                    T? originalModel = originalModels.
                        FirstOrDefault(model =>
                            model.ID == latestModel.ID
                            && latestModel.ID != Guid.Empty);

                    if (originalModel != null)
                    {
                        db.Entry(originalModel).CurrentValues.SetValues(latestModel);
                    }
                }

                await db.SaveChangesAsync();
            }
        }
    }
}
