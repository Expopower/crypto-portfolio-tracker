﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Properties;
using CryptoPortfolioTracker.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Services
{
    public class TaxableEventService : DataAccessServiceBase
    {
        private TransactionService _transactionService;

        public TaxableEventService() : base()
        {
            _transactionService = new TransactionService();
        }

        public TaxableEventService(DbContextOptions<CryptoPortfolioContext>? dbOptions) : base(dbOptions)
        {
            _transactionService = new TransactionService(dbOptions);
        }

        public TaxableEventService(DbContextOptions<CryptoPortfolioContext>? dbOptions,
            TransactionService transactionService) : base(dbOptions)
        {
            _transactionService = transactionService;
        }

        public IEnumerable<TaxableEvent> GetTaxableEvents()
        {
            using (var db = GetDbContext())
            {
                db.TaxableEvents
                    .Include(t => t.Transactions)
                    .ThenInclude(tx => tx.Currency)
                    .Include(t => t.Transactions)
                    .ThenInclude(tx => tx.TransactionValues)
                    .Load();
                return db.TaxableEvents.Local
                    .ToObservableCollection()
                    .OrderByDescending(evt => evt.CreatedDateTime);
            }
        }

        public async Task SaveTaxableEventAsync(TaxableEventViewModel savedEventModel)
        {
            if (savedEventModel == null)
            {
                throw new ArgumentNullException(nameof(savedEventModel));
            }

            await SaveTaxableEventAsync(savedEventModel.TaxableEvent);
        }

        public async Task SaveTaxableEventsAsync(List<TaxableEventViewModel> savedEventModels)
        {
            if (savedEventModels == null)
            {
                throw new ArgumentNullException(nameof(savedEventModels));
            }

            await SaveTaxableEventsAsync(savedEventModels
                .Select(viewModel => viewModel.TaxableEvent)
                .ToList());
        }

        public async Task SaveTaxableEventAsync(TaxableEvent savedEvent)
        {
            if (savedEvent == null)
            {
                throw new ArgumentNullException(nameof(savedEvent));
            }

            await SaveTaxableEventsAsync(new List<TaxableEvent>() { savedEvent });
        }

        public async Task SaveTaxableEventsAsync(List<TaxableEvent> savedEvents)
        {
            if (savedEvents == null)
            {
                throw new ArgumentNullException(nameof(savedEvents));
            }

            using (var db = GetDbContext())
            {
                foreach (TaxableEvent savedEvent in savedEvents)
                {
                    await RecalculateValuesAsync(savedEvent);

                    // Create or update existing taxable event
                    TaxableEvent? originalEvent = await db.TaxableEvents
                        .FirstOrDefaultAsync(taxableEvent => taxableEvent.ID == savedEvent.ID);

                    if (originalEvent == null)
                    {
                        db.TaxableEvents.Add(savedEvent);
                    }
                    else
                    {
                        await db.Entry(originalEvent)
                            .Collection(evt => evt.Transactions)
                            .Query()
                            .Include(tx => tx.TransactionValues)
                            .LoadAsync();

                        db.Entry(originalEvent).CurrentValues.SetValues(savedEvent);
                        ApplyModelCollectionUpdates(originalEvent.Transactions, savedEvent.Transactions, ApplyTransactionValueChanges);
                    }
                }

                await db.SaveChangesAsync();
            }
        }

        public void DeleteTaxableEvent(TaxableEventViewModel deletedEventViewModel)
        {
            if (deletedEventViewModel == null)
            {
                throw new ArgumentNullException(nameof(deletedEventViewModel));
            }

            DeleteTaxableEvent(deletedEventViewModel.TaxableEvent);
        }

        public void DeleteTaxableEvent(TaxableEvent deletedEvent)
        {
            if (deletedEvent == null)
            {
                throw new ArgumentNullException(nameof(deletedEvent));
            }

            using (var db = GetDbContext())
            {
                db.TaxableEvents.Remove(deletedEvent);
                db.SaveChanges();
            }
        }

        public void DeleteTaxableEvents(List<TaxableEventViewModel> deletedEventViewModels)
        {
            if (deletedEventViewModels == null)
            {
                throw new ArgumentNullException(nameof(deletedEventViewModels));
            }

            DeleteTaxableEvents(deletedEventViewModels
                .Select(evt => evt.TaxableEvent)
                .ToList());
        }

        public void DeleteTaxableEvents(List<TaxableEvent> deletedEvents)
        {
            if (deletedEvents == null)
            {
                throw new ArgumentNullException(nameof(deletedEvents));
            }

            using (var db = GetDbContext())
            {
                db.TaxableEvents.RemoveRange(deletedEvents);
                db.SaveChanges();
            }
        }

        public async Task ClearModifiedTaxableEventsAsync()
        {
            using (var db = GetDbContext())
            {
                List<TaxableEvent> newTaxableEvents = await db.TaxableEvents
                    .Where(evt => evt.IsModified == true)
                    .ToListAsync();

                newTaxableEvents.ForEach(evt => evt.IsModified = false);

                await db.SaveChangesAsync();
            }
        }

        public List<int> GetTaxYears()
        {
            using (var db = GetDbContext())
            {
                return db.TaxableEvents
                    .Select(evt => evt.CreatedDateTime.Year)
                    .Distinct()
                    .OrderByDescending(year => year)
                    .ToList();
            }
        }

        public async Task RecalculateValuesAsync(TaxableEvent taxableEvent)
        {
            await _transactionService.EnsureAlternativeCurrencyAmountsForTransactionsAsync(
                taxableEvent.CreatedDateTime, taxableEvent.Transactions);

            taxableEvent.NetValueInFiat = taxableEvent.Transactions
                .Sum(tx =>
                    tx.OverrideFiatValue
                    ?? tx.GetValue(Settings.Default.PreferredFiatCurrencyID)?.Amount
                    ?? 0);
            taxableEvent.IsMissingTransactionValueInFiat = taxableEvent.Transactions
                .Any(tx =>
                    !tx.OverrideFiatValue.HasValue
                    && tx.GetValue(Settings.Default.PreferredFiatCurrencyID) == null);
        }

        private void ApplyTransactionValueChanges(Transaction originalTransaction, Transaction updatedTransaction)
        {
            ApplyModelCollectionUpdates(originalTransaction.TransactionValues, updatedTransaction.TransactionValues);
        }
    }
}
