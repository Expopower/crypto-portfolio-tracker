﻿using CryptoPortfolioTracker.Messaging;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Services
{
    public static class DatabaseFileService
    {
        public const string DbDialogExt = "db";
        public const string DbDialogFilter = "Database Files (*.db)|*.db";

        public static string DbDirectory
        {
            get
            {
                string configuredDbPath = Properties.Settings.Default.DatabaseDirectory;
                return !string.IsNullOrWhiteSpace(configuredDbPath)
                    ? configuredDbPath
                    : $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}{Path.DirectorySeparatorChar}" +
                        "CryptoPortfolioTracker";

            }
        }

        public static string DbFileName
        {
            get
            {
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    return "CryptoPortfolioData_Debug";
                }

                return "CryptoPortfolioData";
            }
        }

        public static string DbPath
        {
            get
            {
                return GetDbPathForFileName(DbFileName);
            }
        }

        public static string GetDbPath(string dbDirectory, string fileName)
        {
            return $"{dbDirectory}{Path.DirectorySeparatorChar}{fileName}.db";
        }

        public static string GetDbPathForFileName(string fileName)
        {
            return GetDbPath(DbDirectory, fileName);
        }


        internal static void CreateCheckpoint()
        {
            using (var db = new CryptoPortfolioContext())
            {
                db.Database.ExecuteSqlRaw("PRAGMA wal_checkpoint(TRUNCATE);");
            }
        }

        internal static void CopyDatabaseFile(string newPath)
        {
            EnsureDeleted(newPath);
            File.Copy(DbPath, newPath);
        }

        internal static string GetDefaultBackupFileName()
        {
            return $"{DbFileName}_{DateTime.Now:yyyy-MM-dd-hhmmsstt}";
        }

        internal static void ReplaceDatabaseFile(string replacementPath)
        {
            string backupFilePath = CreateInternalBackup($"TempBackup_{Guid.NewGuid()}");

            try
            {
                EnsureDatabaseDeleted();
                File.Copy(replacementPath, DbPath);
                EnsureDeleted(backupFilePath);
                Messenger.Instance.SendMessage(null, MessageType.TaxableEventsUpdated);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                EnsureDatabaseDeleted();
                File.Copy(backupFilePath, DbPath);
                EnsureDeleted(backupFilePath);
                Messenger.Instance.SendMessage(null, MessageType.TaxableEventsUpdated);

                throw;
            }
        }

        internal static void EnsureDbDirectory()
        {
            if (!Directory.Exists(DbDirectory))
            {
                Directory.CreateDirectory(DbDirectory);
            }
        }

        internal static void UpdateDatabaseDirectory(string newDirectory)
        {
            if (newDirectory == Properties.Settings.Default.DatabaseDirectory)
            {
                return;
            }

            try
            {
                string newDbPath = GetDbPath(newDirectory, DbFileName);
                CopyDatabaseFile(newDbPath);
                DisconnectDatabase();

                Properties.Settings.Default.DatabaseDirectory = newDirectory;
                Properties.Settings.Default.Save();
                Messenger.Instance.SendMessage(null, MessageType.TaxableEventsUpdated);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        private static string CreateInternalBackup(string backupName)
        {
            CreateCheckpoint();
            string backupFilePath = GetDbPathForFileName(backupName);
            CopyDatabaseFile(backupFilePath);

            return backupFilePath;
        }

        private static void EnsureDeleted(string filePath)
        {
            var fileInfo = new FileInfo(filePath);
            if (fileInfo.Exists)
            {
                File.Delete(filePath);
            }
        }

        private static void DisconnectDatabase()
        {
            using (var db = new CryptoPortfolioContext())
            {
                db.Database.CloseConnection();
            }
        }

        private static void EnsureDatabaseDeleted()
        {
            DisconnectDatabase();

            using (var db = new CryptoPortfolioContext())
            {
                db.Database.EnsureDeleted();
            }
        }
    }
}
