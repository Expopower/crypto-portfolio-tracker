﻿using CryptoPortfolioTracker.ExternalApis;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Services
{
    public class CurrencyService : DataAccessServiceBase
    {
        public static Guid CadCurrencyID = new("758c888a-0ce2-45b3-a863-434796c451ce");
        public static Guid UsdCurrencyID = new("f69c219e-ee7c-48f2-bc1d-77b3afe70f5d");

        public static Guid BtcCurrencyID = new("5b7573a6-95c3-4869-bc32-0642f7c130c2");
        public static Guid CroCurrencyID = new("f16fc08c-973a-422c-a600-b1c65381ec0f");
        public static Guid EthCurrencyID = new("aca57051-72ae-4eab-8191-fef754132d98");
        public static Guid TcadCurrencyID = new("521d94ae-b1a1-4912-b9cb-0aef8158161b");
        public static Guid UsdcCurrencyID = new("237a9a8b-737b-4b34-b03e-3165e1c52eb3");

        private IExchangeRateApi _cryptoExchangeRateApi;
        private IExchangeRateApi _fiatExchangeRateApi;

        public CurrencyService() : base()
        {
            _cryptoExchangeRateApi = new CoinAPIExchangeRateApi();
            _fiatExchangeRateApi = new FiatExchangeRateApi();
        }

        public CurrencyService(DbContextOptions<CryptoPortfolioContext>? dbOptions) : base(dbOptions)
        {
            _cryptoExchangeRateApi = new CoinAPIExchangeRateApi();
            _fiatExchangeRateApi = new FiatExchangeRateApi();
        }

        public CurrencyService(DbContextOptions<CryptoPortfolioContext>? dbOptions,
            IExchangeRateApi cryptoExchangeRateApi, IExchangeRateApi fiatExchangeRateApi) : base(dbOptions)
        {
            _cryptoExchangeRateApi = cryptoExchangeRateApi;
            _fiatExchangeRateApi = fiatExchangeRateApi;
        }

        public Currency? GetCurrency(Guid currencyID)
        {
            using (var db = GetDbContext())
            {
                return db.Currencies
                    .FirstOrDefault(currency => currency.ID == currencyID);
            }
        }

        public List<Currency> GetCurrencies(CurrencyType? currencyType = null)
        {
            using (var db = GetDbContext())
            {
                db.Currencies
                    .Include(currency => currency.ReferenceExchanges)
                    .Include(currency => currency.RateRetrievalRanges)
                    .Load();
                return db.Currencies.Local
                    .ToObservableCollection()
                    .Where(c => !currencyType.HasValue
                        || c.CurrencyType == currencyType.Value)
                    .OrderBy(currency => currency.CurrencyCode)
                    .ToList();
            }
        }

        public List<Currency> GetFiatCurrencies()
        {
            return GetCurrencies(CurrencyType.Fiat);
        }

        public List<Currency> GetCryptoCurrencies()
        {
            return GetCurrencies(CurrencyType.Crypto);
        }

        public async Task SaveCurrenciesAsync(IEnumerable<CurrencyViewModel> latestCurrencyModels, CurrencyType currencyType)
        {
            if (latestCurrencyModels == null)
            {
                throw new ArgumentNullException(nameof(latestCurrencyModels));
            }

            List<Currency> latestCurrencies = latestCurrencyModels
                .Select(cm => cm.Currency)
                .ToList();

            await SaveCurrenciesAsync(latestCurrencies, currencyType);
        }

        public async Task SaveCurrenciesAsync(List<Currency> latestCurrencies, CurrencyType currencyType)
        {
            HashSet<Guid> updatedCurrencyIDs = latestCurrencies
                .Select(c => c.ID)
                .ToHashSet();

            using (var db = GetDbContext())
            {
                List<Currency> originalCurrencies = await db.Currencies
                    .Where(c => c.CurrencyType == currencyType)
                    .ToListAsync();
                List<Currency> deletedCurrencies = originalCurrencies
                    .Where(c => !updatedCurrencyIDs.Contains(c.ID)
                        && c.ID != Guid.Empty)
                    .ToList();
                List<Currency> addedCurrencies = latestCurrencies
                    .Where(c => c.ID == Guid.Empty)
                    .ToList();

                db.Currencies.RemoveRange(deletedCurrencies);
                db.Currencies.AddRange(addedCurrencies);

                foreach (Currency latestCurrency in latestCurrencies)
                {
                    Currency? originalCurrency = originalCurrencies.
                        FirstOrDefault(originalC =>
                            originalC.ID == latestCurrency.ID
                            && latestCurrency.ID != Guid.Empty);

                    if (originalCurrency != null)
                    {
                        await db.Entry(originalCurrency)
                            .Collection(currency => currency.ReferenceExchanges)
                            .LoadAsync();
                        await db.Entry(originalCurrency)
                            .Collection(currency => currency.RateRetrievalRanges)
                            .LoadAsync();

                        db.Entry(originalCurrency).CurrentValues.SetValues(latestCurrency);
                        ApplyModelCollectionUpdates(originalCurrency.ReferenceExchanges, latestCurrency.ReferenceExchanges);
                        ApplyModelCollectionUpdates(originalCurrency.RateRetrievalRanges, latestCurrency.RateRetrievalRanges);
                    }
                }

                await db.SaveChangesAsync();
            }
        }

        public async Task<Currency> EnsureCurrencyForCodeAsync(string currencyCode, CurrencyType currencyType)
        {
            using (var db = GetDbContext())
            {
                var existingCurrency = await db.Currencies
                .FirstOrDefaultAsync(c =>
                    c.CurrencyCode == currencyCode
                    && c.CurrencyType == currencyType);

                if (existingCurrency != null)
                {
                    return existingCurrency;
                }
                else
                {
                    if (currencyType == CurrencyType.Crypto)
                    {
                        db.Currencies.Add(
                            await _cryptoExchangeRateApi.GetCurrencyAsync(currencyCode));
                    }
                    else
                    {
                        db.Currencies.Add(
                            await _fiatExchangeRateApi.GetCurrencyAsync(currencyCode));
                    }
                    await db.SaveChangesAsync();

                    return await db.Currencies.FirstAsync(c =>
                        c.CurrencyCode == currencyCode
                        && c.CurrencyType == currencyType);
                }
            }
        }
    }
}
