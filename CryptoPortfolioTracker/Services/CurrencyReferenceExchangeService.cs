﻿using CryptoPortfolioTracker.ExternalApis;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Services
{
    public class CurrencyReferenceExchangeService : DataAccessServiceBase
    {
        private IExchangeRateApi _cryptoExchangeRateApi;
        private IExchangeRateApi _fiatExchangeRateApi;

        public CurrencyReferenceExchangeService() : base()
        {
            _cryptoExchangeRateApi = new CoinAPIExchangeRateApi();
            _fiatExchangeRateApi = new FiatExchangeRateApi();
        }

        public CurrencyReferenceExchangeService(DbContextOptions<CryptoPortfolioContext>? dbOptions) : base(dbOptions)
        {
            _cryptoExchangeRateApi = new CoinAPIExchangeRateApi();
            _fiatExchangeRateApi = new FiatExchangeRateApi();
        }

        public CurrencyReferenceExchangeService(DbContextOptions<CryptoPortfolioContext>? dbOptions,
            IExchangeRateApi cryptoExchangeRateApi, IExchangeRateApi fiatExchangeRateApi) : base(dbOptions)
        {
            _cryptoExchangeRateApi = cryptoExchangeRateApi;
            _fiatExchangeRateApi = fiatExchangeRateApi;
        }

        public async Task<CurrencyReferenceExchange> EnsureCurrencyReferenceExchange(Guid fromCurrencyID, Guid toCurrencyID)
        {
            using (var db = GetDbContext())
            {
                CurrencyReferenceExchange? referenceExchange = await db.CurrencyReferenceExchanges
                    .FirstOrDefaultAsync(exchange => exchange.FromCurrencyID == fromCurrencyID
                        && exchange.ToCurrencyID == toCurrencyID);

                if (referenceExchange != null)
                {
                    return referenceExchange;
                }
                else
                {
                    string? defaultExchangeName;
                    bool inverted = false;
                    Currency fromCurrency = await db.Currencies.FirstAsync(currency => currency.ID == fromCurrencyID);
                    Currency toCurrency = await db.Currencies.FirstAsync(currency => currency.ID == toCurrencyID);
                    CurrencyType exchangeCurrencyType =
                        (fromCurrency.CurrencyType == CurrencyType.Crypto || toCurrency.CurrencyType == CurrencyType.Crypto)
                            ? CurrencyType.Crypto
                            : CurrencyType.Fiat;

                    IExchangeRateApi effectiveExchange = exchangeCurrencyType == CurrencyType.Crypto
                        ? _cryptoExchangeRateApi
                        : _fiatExchangeRateApi;

                    defaultExchangeName = (await effectiveExchange.GetAvailableExchangesAsync(
                        fromCurrency.CurrencyCode, toCurrency.CurrencyCode)).FirstOrDefault();

                    if (defaultExchangeName == null)
                    {
                        defaultExchangeName = (await effectiveExchange.GetAvailableExchangesAsync(
                            toCurrency.CurrencyCode, fromCurrency.CurrencyCode)).FirstOrDefault();
                        inverted = defaultExchangeName != null;
                    }

                    referenceExchange = new CurrencyReferenceExchange()
                    {
                        FromCurrencyID = fromCurrencyID,
                        ToCurrencyID = toCurrencyID,
                        ExchangeName = defaultExchangeName,
                        ExchangeCurrencyType = exchangeCurrencyType,
                        IsInverted = inverted
                    };

                    db.CurrencyReferenceExchanges.Add(referenceExchange);
                }

                await db.SaveChangesAsync();

                return referenceExchange;
            }
        }
    }
}
