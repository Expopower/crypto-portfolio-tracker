﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Properties;
using CryptoPortfolioTracker.Utilities;
using CryptoPortfolioTracker.ViewModels;
using CsvHelper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Services
{
    public class AccountingService : DataAccessServiceBase
    {
        private CurrencyRateService _currencyRateService;

        public AccountingService() : base()
        {
            _currencyRateService = new CurrencyRateService();
        }

        public AccountingService(DbContextOptions<CryptoPortfolioContext>? dbOptions) : base(dbOptions)
        {
            _currencyRateService = new CurrencyRateService(dbOptions);
        }

        public AccountingService(DbContextOptions<CryptoPortfolioContext>? dbOptions, CurrencyRateService currencyRateService)
            : base(dbOptions)
        {
            _currencyRateService = currencyRateService;
        }

        public async Task<List<CryptoHoldingViewModel>> CalculateCryptoHoldingsAsync(bool queryLatestRates)
        {
            var cryptoHoldings = new Dictionary<Guid, CryptoHoldingViewModel>();

            // TODO: Ensure all transaction CAD values are populated

            using (var db = GetDbContext())
            {
                List<Transaction> transactions = await db.Transactions
                    .Include(tx => tx.Currency)
                    .Include(tx => tx.TaxableEvent)
                    .Include(tx => tx.TransactionValues)
                    .Where(tx => !tx.TaxableEvent.IsDisabled)
                    .OrderBy(tx => tx.TaxableEvent.CreatedDateTime)
                    .ToListAsync();

                foreach (Transaction transaction in transactions)
                {
                    if (transaction.Currency.CurrencyType != CurrencyType.Crypto)
                    {
                        continue;
                    }

                    var holdingView = EnsureHoldingViewForCurrency(cryptoHoldings, transaction.Currency);
                    holdingView.AddTransaction(transaction);
                }

                foreach (KeyValuePair<Guid, CryptoHoldingViewModel> cryptoHolding in cryptoHoldings)
                {
                    try
                    {
                        cryptoHolding.Value.FiatValuePerUnit = await _currencyRateService.GetCurrencyRateAsync(
                            cryptoHolding.Key, Settings.Default.PreferredFiatCurrencyID, DateTime.Now, queryLatestRates);

                        cryptoHolding.Value.CryptoValuePerUnit = await _currencyRateService.GetCurrencyRateAsync(
                            cryptoHolding.Key, Settings.Default.PreferredCryptoCurrencyID, DateTime.Now, queryLatestRates);

                        cryptoHolding.Value.CurrentFiatValue = cryptoHolding.Value.Balance
                            * cryptoHolding.Value.FiatValuePerUnit;

                        decimal valueDeltaFromHistorical =
                            cryptoHolding.Value.CurrentFiatValue.Value - cryptoHolding.Value.FiatValue.HistoricalValue;

                        cryptoHolding.Value.ReturnOnInvestment = cryptoHolding.Value.CurrentFiatValue.HasValue
                            ? MathUtility.SafeDivide(valueDeltaFromHistorical, cryptoHolding.Value.FiatValue.HistoricalValue) * 100
                            : null;
                    }
                    catch (Exception ex)
                    {
                        cryptoHolding.Value.LogError("Failed to calculate some values for this holding", ex.ToString());
                    }
                }
            }

            return cryptoHoldings.Values
                .OrderBy(holding => holding.CurrencyDisplayName)
                .ToList();
        }

        public async Task<TaxableIncomeViewModel> CalculateTaxableIncomeAsync(int taxYear)
        {
            var taxableIncomeView = new TaxableIncomeViewModel()
            {
                TaxYear = taxYear
            };
            var cryptoHoldings = new Dictionary<Guid, CryptoHoldingViewModel>();

            using (var db = GetDbContext())
            {
                List<Transaction> transactions = await db.Transactions
                    .Include(tx => tx.Currency)
                    .Include(tx => tx.TaxableEvent)
                    .Include(tx => tx.TransactionValues)
                    .Where(tx => tx.TaxableEvent.CreatedDateTime.Year <= taxYear
                        && !tx.TaxableEvent.IsDisabled)
                    .OrderBy(tx => tx.TaxableEvent.CreatedDateTime)
                    .ToListAsync();

                foreach (Transaction transaction in transactions)
                {
                    if (transaction.Currency.CurrencyType != CurrencyType.Crypto)
                    {
                        continue;
                    }

                    var holdingView = EnsureHoldingViewForCurrency(cryptoHoldings, transaction.Currency);

                    // Tax calculation must occur before updating the holding view, since the AdjustedCostBase
                    // before the transaction is applied is needed for calculating capital gains/losses.
                    if (transaction.TaxableEvent.CreatedDateTime.Year == taxYear)
                    {
                        taxableIncomeView.AddTransaction(holdingView.FiatValue.AdjustedCostBase, transaction);
                    }

                    holdingView.AddTransaction(transaction);
                }
            }

            return taxableIncomeView;
        }

        public async Task GenerateTaxableIncomeCsvAsync(int taxYear)
        {
            TaxableIncomeViewModel viewModel = await CalculateTaxableIncomeAsync(taxYear);
            using (var writer = new StreamWriter($"{DatabaseFileService.DbDirectory}\\TaxCalculation-{taxYear}.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.CurrentCulture))
            {
                csv.WriteRecords(viewModel.CurrencySummaries);
            }
        }

        private CryptoHoldingViewModel EnsureHoldingViewForCurrency(
            Dictionary<Guid, CryptoHoldingViewModel> holdingViews, Currency currency)
        {
            if (!holdingViews.ContainsKey(currency.ID))
            {
                holdingViews.Add(currency.ID, new CryptoHoldingViewModel(currency));
            }

            return holdingViews[currency.ID];
        }
    }
}
