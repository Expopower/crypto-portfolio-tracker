﻿using CryptoPortfolioTracker.ExternalApis;
using CryptoPortfolioTracker.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Services
{
    public class CurrencyRateService : DataAccessServiceBase
    {
        private IExchangeRateApi _cryptoExchangeRateApi;
        private IExchangeRateApi _fiatExchangeRateApi;
        private CurrencyRateRetrievalDateRangeService _currencyRateRetrievalDateRangeService;
        private CurrencyReferenceExchangeService _currencyReferenceExchangeService;

        public CurrencyRateService() : base()
        {
            _cryptoExchangeRateApi = new CoinAPIExchangeRateApi();
            _fiatExchangeRateApi = new FiatExchangeRateApi();
            _currencyRateRetrievalDateRangeService = new CurrencyRateRetrievalDateRangeService();
            _currencyReferenceExchangeService = new CurrencyReferenceExchangeService();
        }

        public CurrencyRateService(DbContextOptions<CryptoPortfolioContext>? dbOptions) : base(dbOptions)
        {
            _cryptoExchangeRateApi = new CoinAPIExchangeRateApi();
            _fiatExchangeRateApi = new FiatExchangeRateApi();
            _currencyRateRetrievalDateRangeService = new CurrencyRateRetrievalDateRangeService(dbOptions);
            _currencyReferenceExchangeService = new CurrencyReferenceExchangeService(dbOptions);
        }

        public CurrencyRateService(DbContextOptions<CryptoPortfolioContext>? dbOptions,
            IExchangeRateApi cryptoExchangeRateApi, IExchangeRateApi fiatExchangeRateApi,
            CurrencyRateRetrievalDateRangeService currencyRateRetrievalDateRangeService,
            CurrencyReferenceExchangeService currencyReferenceExchangeService) : base(dbOptions)
        {
            _cryptoExchangeRateApi = cryptoExchangeRateApi;
            _fiatExchangeRateApi = fiatExchangeRateApi;
            _currencyRateRetrievalDateRangeService = currencyRateRetrievalDateRangeService;
            _currencyReferenceExchangeService = currencyReferenceExchangeService;
        }

        public async Task EnsureCurrencyRatesForDateRangeAsync(Currency fromCurrency, Currency toCurrency,
            DateTime startDate, DateTime endDate)
        {
            if (AreEquivalent(fromCurrency.ID, toCurrency.ID))
            {
                return;
            }

            List<CurrencyRate>? retrievedExchangeRates = null;
            List<CurrencyExchangePair> currencyPairs =
                await InterpolateCurrencyExchangePairs(fromCurrency, toCurrency);

            foreach (CurrencyExchangePair pair in currencyPairs)
            {
                CurrencyReferenceExchange referenceExchange = await _currencyReferenceExchangeService.EnsureCurrencyReferenceExchange(
                    fromCurrency.ID, toCurrency.ID);

                if (referenceExchange.ExchangeCurrencyType == CurrencyType.Crypto)
                {
                    retrievedExchangeRates = await _cryptoExchangeRateApi.GetExchangeRatesAsync(pair.FromCurrency.CurrencyCode,
                        pair.ToCurrency.CurrencyCode, referenceExchange.ExchangeName, referenceExchange.IsInverted,
                        startDate, endDate);
                }
                else
                {
                    retrievedExchangeRates = await _fiatExchangeRateApi.GetExchangeRatesAsync(pair.FromCurrency.CurrencyCode,
                        pair.ToCurrency.CurrencyCode, referenceExchange.ExchangeName, referenceExchange.IsInverted,
                        startDate, endDate);
                }

                if (retrievedExchangeRates == null || retrievedExchangeRates.Count == 0)
                {
                    return;
                }

                await CreateOrUpdateMatchingCurrencyRates(
                    pair.FromCurrency.ID, pair.ToCurrency.ID, retrievedExchangeRates);
                await _currencyRateRetrievalDateRangeService.CreateOrUpdateRateRetrievalDateRange(
                    pair.FromCurrency.ID, pair.ToCurrency.ID, startDate, endDate);
            }
        }

        public async Task<List<CurrencyRate>> GetCurrencyRatesAsync(Guid fromCurrencyID, Guid toCurrencyID)
        {
            using (var db = GetDbContext())
            {
                return await db.CurrencyRates
                    .Where(rate => rate.FromCurrencyID == fromCurrencyID && rate.ToCurrencyID == toCurrencyID)
                    .ToListAsync();
            }
        }

        public async Task<CurrencyRate?> GetNearestEffectiveRateAsync(Guid fromCurrencyID, Guid toCurrencyID,
            IQueryable<CurrencyRate> currencyRates, DateTime referenceDateTime)
        {
            // Note: Requests that lack a rate may validly look to the past due to missing rates from
            // market closures (e.g. weekends, holidays). However, requests that lack even a past rate
            // will probably require manual intervention, so no accommodations are made for that case.
            return await currencyRates
                .Where(cr => cr.EffectiveDateTime <= referenceDateTime
                    && cr.FromCurrencyID == fromCurrencyID
                    && cr.ToCurrencyID == toCurrencyID)
                .OrderByDescending(cr => cr.EffectiveDateTime)
                .FirstOrDefaultAsync();
        }

        public async Task<decimal?> GetCurrencyRateAsync(Guid fromCurrencyID, Guid toCurrencyID, DateTime referenceDateTime, bool queryLatestRates)
        {
            if (AreEquivalent(fromCurrencyID, toCurrencyID))
            {
                return 1;
            }

            using (var db = GetDbContext())
            {
                decimal? rate = 1;
                Currency fromCurrency = await db.Currencies.FirstAsync(c => c.ID == fromCurrencyID);
                Currency toCurrency = await db.Currencies.FirstAsync(c => c.ID == toCurrencyID);

                if (queryLatestRates)
                {
                    await EnsureCurrencyRatesForDateTimeAsync(fromCurrency, toCurrency, referenceDateTime);
                }

                List<CurrencyExchangePair> currencyPairs =
                    await InterpolateCurrencyExchangePairs(fromCurrency, toCurrency);

                foreach (CurrencyExchangePair pair in currencyPairs)
                {
                    CurrencyRate? effectiveRate = await GetNearestEffectiveRateAsync(pair.FromCurrency.ID,
                        pair.ToCurrency.ID, db.CurrencyRates, referenceDateTime);
                    rate *= effectiveRate?.Rate;
                }

                if (!rate.HasValue)
                {
                    // TODO: Log or flag missing value somewhere
                    Trace.WriteLine($"Unable to calculate rate from currency {fromCurrencyID} to {toCurrencyID}.");
                }

                return rate;
            }
        }

        public async Task EnsureCurrencyRatesForDateTimeAsync(Guid fromCurrencyID, Guid toCurrencyID, DateTime referenceDateTime)
        {
            if (AreEquivalent(fromCurrencyID, toCurrencyID))
            {
                return;
            }

            using (var db = GetDbContext())
            {
                Currency fromCurrency = await db.Currencies.FirstAsync(c => c.ID == fromCurrencyID);
                Currency toCurrency = await db.Currencies.FirstAsync(c => c.ID == toCurrencyID);

                await EnsureCurrencyRatesForDateTimeAsync(fromCurrency, toCurrency, referenceDateTime);
            }
        }

        public async Task EnsureCurrencyRatesForDateTimeAsync(Currency fromCurrency, Currency toCurrency, DateTime referenceDateTime)
        {
            if (AreEquivalent(fromCurrency.ID, toCurrency.ID))
            {
                return;
            }

            List<CurrencyExchangePair> currencyPairs =
                await InterpolateCurrencyExchangePairs(fromCurrency, toCurrency);

            using (var db = GetDbContext())
            {
                foreach(CurrencyExchangePair pair in currencyPairs)
                {
                    if (await _currencyRateRetrievalDateRangeService.IsWithinRateRetrievalDateRange(
                        pair.FromCurrency.ID, pair.ToCurrency.ID, referenceDateTime))
                    {
                        continue;
                    }

                    // Note: Disabled for now to avoid expensive API calls
                    // Retrieve the entire year to minimize calls to the external API
                    //DateTime startDate = new(referenceDateTime.Year, 1, 1);
                    //DateTime endDate = DateTimeUtility.GetMin(
                    //    new DateTime(referenceDateTime.Year + 1, 1, 1), DateTime.UtcNow.Date);

                    DateTime startDate = referenceDateTime.Date.AddDays(-1);
                    DateTime endDate = referenceDateTime.Date.AddDays(1);

                    await EnsureCurrencyRatesForDateRangeAsync(pair.FromCurrency, pair.ToCurrency,
                        startDate, endDate);
                }

                await db.SaveChangesAsync();
            }
        }

        public bool AreEquivalent(Guid currencyID1, Guid currencyID2)
        {
            // TODO: If additional stablecoin support is needed, move this to a new table and create data relationships
            return currencyID1 == currencyID2
                || (currencyID1 == CurrencyService.CadCurrencyID && currencyID2 == CurrencyService.TcadCurrencyID)
                || (currencyID2 == CurrencyService.CadCurrencyID && currencyID1 == CurrencyService.TcadCurrencyID);
        }

        private async Task CreateOrUpdateMatchingCurrencyRates(Guid fromCurrencyID, Guid toCurrencyID, List<CurrencyRate> referenceRates)
        {
            using (var db = GetDbContext())
            {
                foreach (CurrencyRate rate in referenceRates)
                {
                    CurrencyRate? existingRate = await db.CurrencyRates.FirstOrDefaultAsync(
                        ccr => ccr.EffectiveDateTime == rate.EffectiveDateTime
                        && ccr.FromCurrencyID == fromCurrencyID
                        && ccr.ToCurrencyID == toCurrencyID);

                    if (existingRate != null)
                    {
                        if (existingRate.Rate != rate.Rate)
                        {
                            existingRate.Rate = rate.Rate;
                        }
                    }
                    else
                    {
                        db.CurrencyRates.Add(new CurrencyRate()
                        {
                            FromCurrencyID = fromCurrencyID,
                            ToCurrencyID = toCurrencyID,
                            EffectiveDateTime = rate.EffectiveDateTime,
                            Rate = rate.Rate
                        });
                    }
                }

                await db.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Creates pairs of Currencies representing exchanges between them,
        /// including any intermediate exchanges that need to be performed.
        /// </summary>
        /// <param name="fromCurrency"></param>
        /// <param name="toCurrency"></param>
        /// <returns></returns>
        private async Task<List<CurrencyExchangePair>> InterpolateCurrencyExchangePairs(Currency fromCurrency, Currency toCurrency)
        {
            List<CurrencyExchangePair> pairs = new List<CurrencyExchangePair>();

            using (var db = GetDbContext())
            {
                if (RequiresIntermediateUsdExchange(fromCurrency, toCurrency))
                {
                    Currency usdCurrency = await db.Currencies.FirstAsync(c => c.ID == CurrencyService.UsdCurrencyID);
                    pairs.Add(new CurrencyExchangePair(fromCurrency, usdCurrency));
                    pairs.Add(new CurrencyExchangePair(usdCurrency, toCurrency));
                }
                else
                {
                    pairs.Add(new CurrencyExchangePair(fromCurrency, toCurrency));
                }
            }

            return pairs;
        }

        private bool RequiresIntermediateUsdExchange(Currency fromCurrency, Currency toCurrency)
        {
            // In a crypto/fiat pair, the crypto must always exchange to/from USD
            return (
                fromCurrency.CurrencyType == CurrencyType.Crypto
                && toCurrency.CurrencyType == CurrencyType.Fiat
                && toCurrency.ID != CurrencyService.UsdCurrencyID
            ) || (
                fromCurrency.CurrencyType == CurrencyType.Fiat
                && toCurrency.CurrencyType == CurrencyType.Crypto
                && fromCurrency.ID != CurrencyService.UsdCurrencyID
            );
        }

        private class CurrencyExchangePair
        {
            public Currency FromCurrency { get; }
            public Currency ToCurrency { get; }

            public CurrencyExchangePair(Currency fromCurrency, Currency toCurrency)
            {
                FromCurrency = fromCurrency;
                ToCurrency = toCurrency;
            }
        }
    }
}
