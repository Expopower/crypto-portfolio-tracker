﻿using CryptoPortfolioTracker.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Services
{
    public class CurrencyRateRetrievalDateRangeService : DataAccessServiceBase
    {
        public CurrencyRateRetrievalDateRangeService() : base()
        {
        }

        public CurrencyRateRetrievalDateRangeService(DbContextOptions<CryptoPortfolioContext>? dbOptions) : base(dbOptions)
        {
        }

        public async Task<CurrencyRateRetrievalDateRange?> GetRateRetrievalDateRange(Guid fromCurrencyID, Guid toCurrencyID)
        {
            using (var db = GetDbContext())
            {
                return await db.CurrencyRateRetrievalDateRanges
                    .FirstOrDefaultAsync(range => range.FromCurrencyID == fromCurrencyID
                        && range.ToCurrencyID == toCurrencyID);
            }
        }

        public async Task<bool> IsWithinRateRetrievalDateRange(Guid fromCurrencyID, Guid toCurrencyID, DateTime dateTime)
        {
            CurrencyRateRetrievalDateRange? dateRange = await GetRateRetrievalDateRange(fromCurrencyID, toCurrencyID);

            if (dateRange == null)
            {
                return false;
            }

            return dateRange.StartDate <= dateTime.Date
                && dateTime.Date <= dateRange.EndDate;
        }

        public async Task CreateOrUpdateRateRetrievalDateRange(Guid fromCurrencyID, Guid toCurrencyID,
            DateTime startDate, DateTime endDate)
        {
            using (var db = GetDbContext())
            {
                CurrencyRateRetrievalDateRange? dateRange = await db.CurrencyRateRetrievalDateRanges
                    .FirstOrDefaultAsync(range => range.FromCurrencyID == fromCurrencyID
                        && range.ToCurrencyID == toCurrencyID);

                if (dateRange != null)
                {
                    if (dateRange.StartDate > startDate)
                    {
                        dateRange.StartDate = startDate;
                    }
                    if (dateRange.EndDate < endDate)
                    {
                        dateRange.EndDate = endDate;
                    }
                }
                else
                {
                    db.CurrencyRateRetrievalDateRanges.Add(
                        new CurrencyRateRetrievalDateRange()
                        {
                            FromCurrencyID = fromCurrencyID,
                            ToCurrencyID = toCurrencyID,
                            StartDate = startDate,
                            EndDate = endDate
                        });
                }

                await db.SaveChangesAsync();
            }
        }
    }
}
