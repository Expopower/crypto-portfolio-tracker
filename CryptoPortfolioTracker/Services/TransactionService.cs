﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Properties;
using CryptoPortfolioTracker.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Services
{
    public class TransactionService : DataAccessServiceBase
    {
        private CurrencyRateService _currencyRateService;

        public TransactionService() : base()
        {
            _currencyRateService = new CurrencyRateService();
        }

        public TransactionService(DbContextOptions<CryptoPortfolioContext>? dbOptions) : base(dbOptions)
        {
            _currencyRateService = new CurrencyRateService(dbOptions);
        }

        public TransactionService(DbContextOptions<CryptoPortfolioContext>? dbOptions, CurrencyRateService currencyRateService)
            : base(dbOptions)
        {
            _currencyRateService = currencyRateService;
        }

        public async Task EnsureAlternativeCurrencyAmountsForTransactionsAsync(DateTime referenceDateTime, List<Transaction> transactions)
        {
            if (!transactions.Any())
            {
                return;
            }

            await EnsureExchangeRatesForTransactionsAsync(Settings.Default.PreferredFiatCurrencyID, referenceDateTime, transactions);
            await EnsureExchangeRatesForTransactionsAsync(Settings.Default.PreferredCryptoCurrencyID, referenceDateTime, transactions);

            foreach (Transaction transaction in transactions)
            {
                await AddOrUpdateTransactionValueAsync(transaction, Settings.Default.PreferredFiatCurrencyID, referenceDateTime);
                await AddOrUpdateTransactionValueAsync(transaction, Settings.Default.PreferredCryptoCurrencyID, referenceDateTime);
            }
        }

        public async Task EnsureExchangeRatesForTransactionsAsync(Guid toCurrencyID, DateTime referenceDateTime,
            List<Transaction> transactions)
        {
            IEnumerable<Guid> currencyIDs = transactions
                .Select(tx => tx.CurrencyID);

            foreach (Guid currencyID in currencyIDs)
            {
                await _currencyRateService.EnsureCurrencyRatesForDateTimeAsync(currencyID, toCurrencyID, referenceDateTime);
            }
        }

        // TODO: Determine if we have a use case for this - for example, if we wipe all CAD values and currency rates
        // due to a change in API data.
        public async Task EnsureAlternativeCurrencyAmountsForExistingTransactionsAsync(int? year = null)
        {
            using (var db = GetDbContext())
            {
                List<Transaction> transactions = await db.Transactions
                    .Include(tx => tx.TaxableEvent)
                    .Include(tx => tx.Currency)
                    .Include(tx => tx.TransactionValues)
                    .ToListAsync();

                transactions = transactions
                    .Where(tx =>
                        tx.GetValue(Settings.Default.PreferredFiatCurrencyID) == null
                        || tx.GetValue(Settings.Default.PreferredCryptoCurrencyID) == null)
                    .ToList();

                if (year.HasValue)
                {
                    transactions = transactions
                        .Where(tx => tx.TaxableEvent.CreatedDateTime.Year == year.Value)
                        .ToList();
                }

                if (!transactions.Any())
                {
                    return;
                }

                await EnsureExchangeRatesForExistingTransactionsAsync(transactions);

                foreach (Transaction transaction in transactions)
                {
                    await AddOrUpdateTransactionValueAsync(transaction, Settings.Default.PreferredFiatCurrencyID,
                        transaction.TaxableEvent.CreatedDateTime);
                    await AddOrUpdateTransactionValueAsync(transaction, Settings.Default.PreferredCryptoCurrencyID,
                        transaction.TaxableEvent.CreatedDateTime);
                }

                await db.SaveChangesAsync();
            }
        }

        public async Task EnsureExchangeRatesForExistingTransactionsAsync(List<Transaction> transactions)
        {
            using (var db = GetDbContext())
            {
                Dictionary<Currency, List<Transaction>> groupedTransactions = transactions
                    .GroupBy(tx => tx.Currency)
                    .ToDictionary(group => group.Key, group => group.ToList());

                Currency fiatCurrency = await db.Currencies.FirstAsync(c => c.ID == Settings.Default.PreferredFiatCurrencyID);
                Currency cryptoCurrency = await db.Currencies.FirstAsync(c => c.ID == Settings.Default.PreferredCryptoCurrencyID);

                foreach (KeyValuePair<Currency, List<Transaction>> transactionGroup in groupedTransactions)
                {
                    DateTime startDate = transactionGroup.Value.Min(tx => tx.TaxableEvent.CreatedDateTime);
                    DateTime endDate = transactionGroup.Value.Max(tx => tx.TaxableEvent.CreatedDateTime);

                    await _currencyRateService.EnsureCurrencyRatesForDateRangeAsync(
                        transactionGroup.Key, fiatCurrency, startDate, endDate);
                    await _currencyRateService.EnsureCurrencyRatesForDateRangeAsync(
                        transactionGroup.Key, cryptoCurrency, startDate, endDate);
                }
            }
        }

        public async Task AddOrUpdateTransactionValueAsync(Transaction transaction, Guid targetCurrencyID, DateTime referenceDateTime)
        {
            using (var db = GetDbContext())
            {
                Currency contextCurrency = transaction.Currency
                    ?? await db.Currencies.FirstAsync(c => c.ID == transaction.CurrencyID);
                Currency targetCurrency = await db.Currencies.FirstAsync(c => c.ID == targetCurrencyID);
                TransactionValue? transactionValue = transaction.GetValue(targetCurrencyID);
                decimal? calculatedValue = null;

                if (transactionValue == null)
                {
                    transactionValue = new TransactionValue()
                    {
                        CurrencyID = targetCurrencyID
                    };
                    transaction.TransactionValues.Add(transactionValue);
                }

                // Don't attempt rate conversions for matching currencies
                if (_currencyRateService.AreEquivalent(transaction.CurrencyID, targetCurrencyID))
                {
                    calculatedValue = transaction.Amount;
                }
                else
                {
                    calculatedValue = transaction.Amount
                        * await _currencyRateService.GetCurrencyRateAsync(
                            contextCurrency.ID, targetCurrencyID, referenceDateTime, true);
                }

                // If we cannot calculate a value, remove any prior recorded one
                if (calculatedValue.HasValue)
                {
                    transactionValue.Amount = calculatedValue.Value;
                }
                else
                {
                    transaction.TransactionValues.Remove(transactionValue);
                }
            }
        }

        public async Task SynchronizeToAndFromTransactionsAsync(Transaction toTransaction, Transaction fromTransaction,
            TaxableEventType taxableEventType, DateTime referenceDateTime)
        {
            if (toTransaction == null || fromTransaction == null)
            {
                return;
            }

            switch (taxableEventType)
            {
                case TaxableEventType.Exchange:
                    if (toTransaction.CurrencyID != CurrencyService.TcadCurrencyID)
                    {
                        // Since an exchange is treated as a disposal and acquisition event, ensure
                        // the value of the disposal is used as the cost basis of the acquisition.
                        await AddOrUpdateTransactionValueAsync(fromTransaction, Settings.Default.PreferredFiatCurrencyID, referenceDateTime);
                        toTransaction.OverrideFiatValue =
                            MathUtility.Abs(fromTransaction.GetValue(Settings.Default.PreferredFiatCurrencyID)?.Amount);
                    }
                    else
                    {
                        // For a TCAD (stablecoin) transaction, we operate in reverse since the value
                        // of the acquisition is pegged to fiat, and is effectively a sale.
                        // TODO: Maybe generalize for all stablecoins?
                        await AddOrUpdateTransactionValueAsync(toTransaction, Settings.Default.PreferredFiatCurrencyID, referenceDateTime);
                        fromTransaction.OverrideFiatValue =
                            MathUtility.Abs(toTransaction.GetValue(Settings.Default.PreferredFiatCurrencyID)?.Amount) * -1;
                    }
                    break;
                case TaxableEventType.Sale:
                    // Ensure the exact amount received for the sale is assigned to the "from" transaction.
                    await AddOrUpdateTransactionValueAsync(toTransaction, Settings.Default.PreferredFiatCurrencyID, referenceDateTime);
                    fromTransaction.OverrideFiatValue =
                        MathUtility.Abs(toTransaction.GetValue(Settings.Default.PreferredFiatCurrencyID)?.Amount) * -1;
                    break;
                case TaxableEventType.Purchase:
                    // Ensure the exact amount spent on the purchase is assigned to the "to" transaction.
                    await AddOrUpdateTransactionValueAsync(fromTransaction, Settings.Default.PreferredFiatCurrencyID, referenceDateTime);
                    toTransaction.OverrideFiatValue =
                        MathUtility.Abs(fromTransaction.GetValue(Settings.Default.PreferredFiatCurrencyID)?.Amount);
                    break;
                default:
                    break;
            }
        }
    }
}
