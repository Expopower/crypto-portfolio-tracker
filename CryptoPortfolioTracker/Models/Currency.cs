﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CryptoPortfolioTracker.Models
{
    [Index(nameof(CurrencyCode), nameof(CurrencyType), IsUnique = true)]
    public class Currency : IModel
    {
        public Guid ID { get; set; }
        public CurrencyType CurrencyType { get; set; }
        public string CurrencyCode { get; set; }
        public string Name { get; set; }

        [InverseProperty("FromCurrency")]
        public List<CurrencyReferenceExchange> ReferenceExchanges { get; set; } = new List<CurrencyReferenceExchange>();
        [InverseProperty("FromCurrency")]
        public List<CurrencyRateRetrievalDateRange> RateRetrievalRanges { get; set; } = new List<CurrencyRateRetrievalDateRange>();
    }

    public enum CurrencyType
    {
        Crypto = 0,
        Fiat = 1
    }
}
