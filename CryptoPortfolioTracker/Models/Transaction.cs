﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Models
{
    public class Transaction : IModel
    {
        public Guid ID { get; set; }
        public decimal Amount { get; set; }
        public decimal? OverrideFiatValue { get; set; }
        public List<TransactionValue> TransactionValues { get; set; } = new List<TransactionValue>();

        public Guid TaxableEventID { get; set; }
        public TaxableEvent TaxableEvent { get; set; }

        public Guid CurrencyID { get; set; }
        public Currency Currency { get; set; }

        public TransactionValue? GetValue(Guid currencyID)
        {
            return TransactionValues.FirstOrDefault(value => value.CurrencyID == currencyID);
        }

        public decimal GetTaxBasis(Guid currencyID)
        {
            return OverrideFiatValue
                ?? GetValue(currencyID)?.Amount
                ?? 0;
        }
    }
}
