﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CryptoPortfolioTracker.Models
{
    [Index(nameof(FromCurrencyID), nameof(ToCurrencyID), IsUnique = true)]
    public class CurrencyRateRetrievalDateRange : IModel
    {
        public Guid ID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public Guid FromCurrencyID { get; set; }
        [ForeignKey("FromCurrencyID")]
        public Currency FromCurrency { get; set; }

        public Guid ToCurrencyID { get; set; }
        [ForeignKey("ToCurrencyID")]
        public Currency ToCurrency { get; set; }
    }
}
