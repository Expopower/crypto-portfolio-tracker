﻿using System;
using System.Collections.Generic;

namespace CryptoPortfolioTracker.Models
{
    public enum TaxableEventType
    {
        Exchange = 0,
        Interest = 1,
        Sale = 2,
        Purchase = 3,
        Cashback = 4,
        Bonus = 5,
        Variable = 6,
        Unknown = 7,
        Fee = 8
    }

    public class TaxableEvent : IModel
    {
        public Guid ID { get; set; }
        public TaxableEventType EventType { get; set; }
        public decimal? NetValueInFiat { get; set; }
        public bool IsMissingTransactionValueInFiat { get; set; }
        public string? Description { get; set; }
        public List<Transaction> Transactions { get; set; } = new List<Transaction>();
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;
        public bool? IsModified { get; set; }
        public bool IsDisabled { get; set; }
    }
}
