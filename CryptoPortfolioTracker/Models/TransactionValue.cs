﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Models
{
    [Index(nameof(CurrencyID), nameof(TransactionID), IsUnique = true)]
    public class TransactionValue :IModel
    {
        public Guid ID { get; set; }
        public decimal Amount { get; set; }

        public Guid TransactionID { get; set; }
        public Transaction Transaction { get; set; }

        public Guid CurrencyID { get; set; }
        public Currency Currency { get; set; }
    }
}
