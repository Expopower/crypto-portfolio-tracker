﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Models
{
    [Index(nameof(FromCurrencyID), nameof(ToCurrencyID), IsUnique = true)]
    public class CurrencyReferenceExchange : IModel
    {
        public Guid ID { get; set; }
        public string? ExchangeName { get; set; }
        public CurrencyType ExchangeCurrencyType { get; set; }
        public bool IsInverted { get; set; }

        public Guid FromCurrencyID { get; set; }
        [ForeignKey("FromCurrencyID")]
        public Currency FromCurrency { get; set; }

        public Guid ToCurrencyID { get; set; }
        [ForeignKey("ToCurrencyID")]
        public Currency ToCurrency { get; set; }
    }
}
