﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Models
{
    [Index(nameof(FromCurrencyID), nameof(ToCurrencyID), nameof(EffectiveDateTime), IsUnique = true)]
    public class CurrencyRate : IModel
    {
        public Guid ID { get; set; }
        public decimal Rate { get; set; }
        public DateTime EffectiveDateTime { get; set; }

        public Guid FromCurrencyID { get; set; }
        [ForeignKey("FromCurrencyID")]
        public Currency FromCurrency { get; set; }

        public Guid ToCurrencyID { get; set; }
        [ForeignKey("ToCurrencyID")]
        public Currency ToCurrency { get; set; }
    }
}
