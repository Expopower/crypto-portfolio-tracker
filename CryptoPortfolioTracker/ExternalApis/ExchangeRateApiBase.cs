﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ExternalApis
{
    /// <summary>
    /// Base class for integrations with currency exchange rate APIs.
    /// </summary>
    public abstract class ExchangeRateApiBase : IExchangeRateApi
    {
        public virtual async Task<Currency> GetCurrencyAsync(string currencyCode)
        {
            throw new NotImplementedException();
        }

        public virtual async Task<List<string>> GetAvailableExchangesAsync(string fromCurrencyCode, string toCurrencyCode)
        {
            return new List<string>();
        }

        public virtual async Task<List<CurrencyRate>?> GetExchangeRatesAsync(string fromCurrencyCode, string toCurrencyCode,
            string? referenceExchange, bool invert, DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }
    }
}
