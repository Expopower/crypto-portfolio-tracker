﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ExternalApis
{
    /// <summary>
    /// https://docs.coinapi.io/market-data/rest-api/
    /// </summary>
    internal class CoinAPIExchangeRateApi : ExchangeRateApiBase
    {
        private const string ApiKey = "6C7300E7-A643-4232-9119-6BB3E77B6B76";
        private const string ApiUrl = "https://rest.coinapi.io/v1/";
        private const string AssetEndpoint = "assets?filter_asset_id={0}";
        private const string PriceHistoryEndpoint = "exchangerate/{0}/{1}/history";
        private const string PeriodLength = "6HRS";

        public override async Task<Currency> GetCurrencyAsync(string currencyCode)
        {
            string assetUrl = ApiUrl + string.Format(AssetEndpoint, currencyCode.ToLower());
            var requestBuilder = new UriBuilder(assetUrl);

            try
            {
                AssetItem[]? assetItems = await MakeRequestAsync<AssetItem[]>(requestBuilder.Uri);
                AssetItem? asset = assetItems?.FirstOrDefault();

                if (asset == null)
                {
                    throw new Exception($"No cryptocurrency asset returned for {currencyCode}.");
                }

                return new Currency()
                {
                    CurrencyCode = asset.asset_id.ToUpper(),
                    Name = asset.name,
                    CurrencyType = CurrencyType.Crypto
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public override async Task<List<string>> GetAvailableExchangesAsync(string fromCurrencyCode, string toCurrencyCode)
        {
            return new List<string>() { "CoinAPI Default" };
        }

        public override async Task<List<CurrencyRate>?> GetExchangeRatesAsync(string fromCurrencyCode, string toCurrencyCode,
            string? referenceExchange, bool invert, DateTime startDate, DateTime endDate)
        {
            // Broaden the range by a day on either end, so we don't have to
            // worry about missing values due to conversion to GMT.
            DateTime adjustedStartDate = startDate.AddDays(-1);
            DateTime adjustedEndDate = endDate.AddDays(1);

            // If only the inverted rates are available, then we should retrieve the opposite pair
            string effectiveFromCurrencyCode = invert ? toCurrencyCode : fromCurrencyCode;
            string effectiveToCurrencyCode = invert ? fromCurrencyCode : toCurrencyCode;

            string priceUrl = ApiUrl + string.Format(PriceHistoryEndpoint,
                effectiveFromCurrencyCode.ToLower(),
                effectiveToCurrencyCode.ToLower());
            string priceQuery = $"period_id={PeriodLength}" +
                    $"&time_start={adjustedStartDate.ToString("o", System.Globalization.CultureInfo.InvariantCulture)}" +
                    $"&time_end={adjustedEndDate.ToString("o", System.Globalization.CultureInfo.InvariantCulture)}" +
                    $"&limit=1000";

            var requestBuilder = new UriBuilder(priceUrl)
            {
                Query = priceQuery
            };

            try
            {
                TimeseriesItem[]? timeseriesItems = await MakeRequestAsync<TimeseriesItem[]>(requestBuilder.Uri);

                if (timeseriesItems == null || timeseriesItems.Length == 0)
                {
                    throw new Exception($"No OHLC price values returned for exchanging {effectiveFromCurrencyCode} to {effectiveToCurrencyCode}.");
                }

                return timeseriesItems
                    .Select(item => new CurrencyRate()
                    {
                        EffectiveDateTime = item.time_close,
                        Rate = invert
                            ? MathUtility.SafeDivide(1, item.rate_close)
                            : item.rate_close
                    })
                    .ToList();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
            }

            return new List<CurrencyRate>();
        }

        private async Task<T?> MakeRequestAsync<T>(Uri uri)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-CoinAPI-Key", ApiKey);
            HttpResponseMessage response = await client.GetAsync(uri);
            string result = await response.Content.ReadAsStringAsync();

            try
            {
                return JsonConvert.DeserializeObject<T>(result);
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to call API ({uri}):\nResponse: {result}\nException: {ex}");
            }
        }

        public class AssetItem
        {
            public string asset_id;
            public string name;
            public int type_is_crypto;
        }

        public class TimeseriesItem
        {
            public DateTime time_open;
            public DateTime time_close;
            public decimal rate_open;
            public decimal rate_high;
            public decimal rate_low;
            public decimal rate_close;
        }
    }
}
