﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ExternalApis
{
    /// <summary>
    /// https://docs.cryptowat.ch/rest-api
    /// </summary>
    internal class CryptoWatchExchangeRateApi : ExchangeRateApiBase
    {
        private const string DefaultReferenceExchange = "bittrex";
        private const string ApiUrl = "https://api.cryptowat.ch/";
        private const string AssetEndpoint = "assets/{0}";
        private const string PairEndpoint = "pairs/{0}{1}";
        private const string PriceHistoryEndpoint = "markets/{0}/{1}{2}/ohlc";
        //private const string PeriodLength = "86400"; // Daily
        private const string PeriodLength = "3600"; // Hourly

        public override async Task<Currency> GetCurrencyAsync(string currencyCode)
        {
            string assetUrl = ApiUrl + string.Format(AssetEndpoint, currencyCode.ToLower());
            var requestBuilder = new UriBuilder(assetUrl);

            try
            {
                AssetResponseBody? responseBody = await MakeRequestAsync<AssetResponseBody>(requestBuilder.Uri);

                if (responseBody?.result == null)
                {
                    throw new Exception($"No cryptocurrency asset returned for {currencyCode}.");
                }

                return new Currency()
                {
                    CurrencyCode = responseBody.result.symbol.ToUpper(),
                    Name = responseBody.result.name,
                    CurrencyType = CurrencyType.Crypto
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public override async Task<List<string>> GetAvailableExchangesAsync(string fromCurrencyCode, string toCurrencyCode)
        {
            string pairUrl = ApiUrl + string.Format(PairEndpoint, fromCurrencyCode.ToLower(), toCurrencyCode.ToLower());
            var requestBuilder = new UriBuilder(pairUrl);

            try
            {
                PairResponseBody? responseBody = await MakeRequestAsync<PairResponseBody>(requestBuilder.Uri);

                if (responseBody?.result == null)
                {
                    throw new Exception($"No cryptocurrency pair returned for {fromCurrencyCode} and {toCurrencyCode}.");
                }

                List<string> activeExchanges = responseBody.result.markets
                    .Where(market => market.active)
                    .Select(market => market.exchange)
                    .OrderBy(exchange => exchange)
                    .ThenBy(exchange => exchange == DefaultReferenceExchange ? 2 : 1)
                    .ToList();

                if (!activeExchanges.Any())
                {
                    throw new Exception($"No exchanges available for cryptocurrency pair {fromCurrencyCode} and {toCurrencyCode}.");
                }

                return activeExchanges;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return new List<string>();
        }

        public override async Task<List<CurrencyRate>?> GetExchangeRatesAsync(string fromCurrencyCode, string toCurrencyCode,
            string? referenceExchange, bool invert, DateTime startDate, DateTime endDate)
        {
            // Broaden the range by a day on either end, so we don't have to
            // worry about missing values due to conversion to GMT.
            DateTime adjustedStartDate = startDate.AddDays(-1);
            DateTime adjustedEndDate = endDate.AddDays(1);

            // If only the inverted rates are available, then we should retrieve the opposite pair
            string effectiveFromCurrencyCode = invert ? toCurrencyCode : fromCurrencyCode;
            string effectiveToCurrencyCode = invert ? fromCurrencyCode : toCurrencyCode;

            string priceUrl = ApiUrl + string.Format(PriceHistoryEndpoint,
                referenceExchange ?? DefaultReferenceExchange,
                effectiveFromCurrencyCode.ToLower(),
                effectiveToCurrencyCode.ToLower());
            string priceQuery = $"periods={PeriodLength}" +
                    $"&after={DateTimeUtility.ConvertToUtcUnixTime(adjustedStartDate)}" +
                    $"&before={DateTimeUtility.ConvertToUtcUnixTime(adjustedEndDate)}";

            var requestBuilder = new UriBuilder(priceUrl)
            {
                Query = priceQuery
            };

            try
            {
                PriceResponseBody? responseBody = await MakeRequestAsync<PriceResponseBody>(requestBuilder.Uri);

                if (responseBody?.result?.root == null)
                {
                    throw new Exception($"No OHLC price values returned for exchanging {effectiveFromCurrencyCode} to {effectiveToCurrencyCode}.");
                }

                // Eliminate duplicate time periods, which can occur for hourly periods
                // when the local timezone observes Daylight Savings Time.
                var ohlcViews = responseBody.result.root
                    .Select(item => new OpenHighLowCloseView(item))
                    .GroupBy(item => item.CloseDateTime)
                    .Select(groupedItems => groupedItems.First());

                return ohlcViews
                    .Select(item => new CurrencyRate()
                    {
                        EffectiveDateTime = item.CloseDateTime,
                        Rate = invert
                            ? MathUtility.SafeDivide(1, item.ClosePrice)
                            : item.ClosePrice
                    })
                    .ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return new List<CurrencyRate>();
        }

        private async Task<T?> MakeRequestAsync<T>(Uri uri) where T : ResponseBody
        {
            var client = new HttpClient();
            string? result = await client.GetStringAsync(uri);
            T? responseBody = JsonConvert.DeserializeObject<T>(result);

            if (responseBody == null)
            {
                throw new Exception($"No response body ({uri})");
            }

            if (responseBody?.error != null)
            {
                throw new Exception($"API error encountered ({uri}): {responseBody.error}");
            }

            return responseBody;
        }

        public abstract class ResponseBody
        {
            public Allowance allowance;
            public string error;

            public class Allowance
            {
                public float cost;
                public float remaining;
            }
        }

        public class AssetResponseBody : ResponseBody
        {
            public Result result;

            public class Result
            {
                public int id;
                public string symbol;
                public string name;
                public bool fiat;
            }
        }

        public class PairResponseBody : ResponseBody
        {
            public Result result;

            public class Result
            {
                public int id;
                [JsonProperty("base")]
                public Base asset;
                public Market[] markets;

                public class Base
                {
                    public int id;
                    public string symbol;
                    public string name;
                    public bool fiat;
                }

                public class Market
                {
                    public int id;
                    public string exchange;
                    public bool active;
                }
            }
        }

        public class PriceResponseBody : ResponseBody
        {
            public Result result;

            public class Result {
                [JsonProperty(PropertyName = PeriodLength)]
                public decimal[][] root;
            }
        }

        public class OpenHighLowCloseView
        {
            public DateTime CloseDateTime { get; }
            public decimal OpenPrice { get; }
            public decimal HighPrice { get; }
            public decimal LowPrice { get; }
            public decimal ClosePrice { get; }
            //public decimal Volume { get; }
            //public decimal QuoteVolume { get; }


            public OpenHighLowCloseView(decimal[] inputValues)
            {
                CloseDateTime = DateTimeUtility.ConvertFromUtcUnixTime((long)inputValues[0]);
                OpenPrice = inputValues[1];
                HighPrice = inputValues[2];
                LowPrice = inputValues[3];
                ClosePrice = inputValues[4];
            }
        }
    }
}
