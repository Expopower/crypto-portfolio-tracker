﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ExternalApis
{
    /// <summary>
    /// https://www.bankofcanada.ca/valet/docs
    /// </summary>
    internal class FiatExchangeRateApi : ExchangeRateApiBase
    {
        private const string ApiUrl = "https://www.bankofcanada.ca/valet/";
        private const string ExchangePair = "FX{0}{1}";
        private const string SeriesEndpoint = "series/";
        private const string PricesEndpoint = "observations/";

        public override async Task<Currency> GetCurrencyAsync(string currencyCode)
        {
            string pair = string.Format(ExchangePair, currencyCode.ToUpper());
            string pairUrl = $"{ApiUrl}{SeriesEndpoint}{pair}";
            var requestBuilder = new UriBuilder(pairUrl);

            try
            {
                SeriesResponseBody? responseBody = await MakeRequestAsync<SeriesResponseBody>(requestBuilder.Uri);

                if (responseBody?.seriesDetails == null)
                {
                    throw new Exception($"No exchange pair returned for {currencyCode}.");
                }

                return new Currency()
                {
                    CurrencyCode = currencyCode,
                    Name = responseBody.seriesDetails.label,
                    CurrencyType = CurrencyType.Fiat
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public override async Task<List<CurrencyRate>?> GetExchangeRatesAsync(string fromCurrencyCode, string toCurrencyCode,
            string? referenceExchange, bool invert, DateTime startDate, DateTime endDate)
        {
            // Broaden the range by a week on either end, so we don't have
            // to worry about missing values due to weekends/holidays.
            DateTime adjustedStartDate = startDate.AddDays(-7);
            DateTime adjustedEndDate = endDate.AddDays(7);

            // If only the inverted rates are available, then we should retrieve the opposite pair
            string effectiveFromCurrencyCode = invert ? toCurrencyCode : fromCurrencyCode;
            string effectiveToCurrencyCode = invert ? fromCurrencyCode : toCurrencyCode;

            string pair = string.Format(ExchangePair,
                effectiveFromCurrencyCode.ToUpper(),
                effectiveToCurrencyCode.ToUpper());
            string priceUrl = $"{ApiUrl}{PricesEndpoint}{pair}";
            string priceQuery = $"start_date={adjustedStartDate.ToString("yyyy-MM-dd")}" +
                    $"&end_date={adjustedEndDate.ToString("yyyy-MM-dd")}";

            var requestBuilder = new UriBuilder(priceUrl)
            {
                Query = priceQuery
            };

            try
            {
                var serializerSettings = new JsonSerializerSettings()
                {
                    ContractResolver = new PriceResponseBody.CustomContractResolver(pair)
                };
                PriceResponseBody? responseBody = await MakeRequestAsync<PriceResponseBody>(requestBuilder.Uri, serializerSettings);

                if (responseBody?.observations == null)
                {
                    throw new Exception($"No historical prices returned for exchanging {effectiveFromCurrencyCode} to {effectiveToCurrencyCode}.");
                }

                return responseBody.observations
                    .Select(item => new CurrencyRate()
                    {
                        EffectiveDateTime = item.d,
                        Rate = invert
                            ? MathUtility.SafeDivide(1, item.price.v)
                            : item.price.v
                    })
                    .ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return new List<CurrencyRate>();
        }

        private async Task<T?> MakeRequestAsync<T>(Uri uri, JsonSerializerSettings? serializerSettings = null) where T : ResponseBody
        {
            var client = new HttpClient();
            string? result = client.GetStringAsync(uri).Result;
            T? responseBody = JsonConvert.DeserializeObject<T>(result, serializerSettings);

            if (responseBody == null)
            {
                throw new Exception("No response body");
            }

            if (responseBody?.message != null)
            {
                throw new Exception($"API error encountered: {responseBody.message}");
            }

            return responseBody;
        }

        public abstract class ResponseBody
        {
            public string message;
            public string docs;
            public Terms terms;

            public class Terms
            {
                public string url;
            }
        }

        public class SeriesResponseBody : ResponseBody
        {
            public SeriesDetails seriesDetails;

            public class SeriesDetails
            {
                public string name;
                public string label;
                public string description;
            }
        }

        public class PriceResponseBody : ResponseBody
        {
            public Observation[] observations;

            public class Observation
            {
                public DateTime d;
                public Price price;

                public class Price
                {
                    public decimal v;
                }
            }

            public class CustomContractResolver : DefaultContractResolver
            {
                public string PairName { get; }

                public CustomContractResolver(string pairName)
                {
                    PairName = pairName;
                }

                protected override string ResolvePropertyName(string propertyName)
                {
                    // Because the property name is dynamic based on the exchange pair, it is
                    // necessary to remap it to a known name so it can be deserialized.
                    if (propertyName == "price")
                    {
                        return PairName;
                    }
                    else
                    {
                        return base.ResolvePropertyName(propertyName);
                    }
                }
            }
        }
    }
}
