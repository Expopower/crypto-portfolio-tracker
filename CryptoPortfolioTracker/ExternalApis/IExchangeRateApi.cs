﻿using CryptoPortfolioTracker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ExternalApis
{
    public interface IExchangeRateApi
    {
        public Task<Currency> GetCurrencyAsync(string currencyCode);
        public Task<List<string>> GetAvailableExchangesAsync(string fromCurrencyCode, string toCurrencyCode);
        public Task<List<CurrencyRate>?> GetExchangeRatesAsync(string fromCurrencyCode, string toCurrencyCode,
            string? referenceExchange, bool invert, DateTime startDate, DateTime endDate);
    }
}
