﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CryptoPortfolioTracker.Windows
{
    /// <summary>
    /// Interaction logic for CurrencyListWindow.xaml
    /// </summary>
    public partial class CurrencyListWindow : Window
    {
        public CurrencyListWindow(CurrencyType currencyType)
        {
            InitializeComponent();
            DataContext = new CurrencyListViewModel(currencyType);
            CurrencyList.DataContext = DataContext;
        }

        private async Task DialogControls_SuccessAsync(object sender, EventArgs e)
        {
            await CurrencyList.SaveAsync();
            DialogResult = true;
        }

        private void DialogControls_Failure(object sender, EventArgs e)
        {
            DialogResult = false;
        }
    }
}
