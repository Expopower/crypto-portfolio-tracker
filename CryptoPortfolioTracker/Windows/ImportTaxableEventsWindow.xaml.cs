﻿using CryptoPortfolioTracker.DataImporters;
using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CryptoPortfolioTracker.Windows
{
    /// <summary>
    /// Interaction logic for ImportTaxableEventsWindow.xaml
    /// </summary>
    public partial class ImportTaxableEventsWindow : Window
    {
        public ImportTaxableEventsWindow(IDataImporter<TaxableEvent> dataImporter)
        {
            InitializeComponent();
            DataContext = new ImportTaxableEventsViewModel(dataImporter);
            ImportTaxableEvents.DataContext = DataContext;
        }

        private async Task DialogControls_SuccessAsync(object sender, EventArgs e)
        {
            if (!ImportTaxableEvents.CanImport())
            {
                DialogControls.Reset();
                return;
            }

            bool importSuccessful = await ImportTaxableEvents.ImportAndSaveDataAsync();

            if (importSuccessful)
            {
                DialogResult = true;
            }
            else
            {
                DialogControls.Reset();
            }
        }

        private void DialogControls_Failure(object sender, EventArgs e)
        {
            DialogResult = false;
        }
    }
}
