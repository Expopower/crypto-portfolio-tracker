﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CryptoPortfolioTracker.Windows
{
    /// <summary>
    /// Interaction logic for PreferredCurrenciesWindow.xaml
    /// </summary>
    public partial class PreferredCurrenciesWindow : Window
    {
        public PreferredCurrenciesWindow()
        {
            InitializeComponent();
            PreferredCurrencies.DataContext = new PreferredCurrenciesViewModel();
        }

        private void DialogControls_Success(object sender, EventArgs e)
        {
            PreferredCurrencies.Save();
            DialogResult = true;
        }

        private void DialogControls_Failure(object sender, EventArgs e)
        {
            DialogResult = false;
        }
    }
}
