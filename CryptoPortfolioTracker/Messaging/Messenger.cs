﻿using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CryptoPortfolioTracker.Messaging
{
    public enum MessageType
    {
        TaxableEventsUpdated,
        CurrenciesUpdated,
        PreferredCurrenciesUpdated
    }

    internal class Messenger
    {
        private static Messenger? _instance = null;
        private Dictionary<MessageType, Dictionary<object, List<Action>>> registeredRecipients = new();
        private Dictionary<MessageType, Dictionary<object, List<Func<Task>>>> registeredRecipientsAsync = new();

        public static Messenger Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Messenger();
                }
                return _instance;
            }
        }

        public void Register(object recipient, Action action, MessageType messageType)
        {
            if (!registeredRecipients.ContainsKey(messageType))
            {
                registeredRecipients.Add(messageType, new Dictionary<object, List<Action>>());
            }

            if (!registeredRecipients[messageType].ContainsKey(recipient))
            {
                registeredRecipients[messageType][recipient] = new List<Action>();
            }

            registeredRecipients[messageType][recipient].Add(action);
        }

        public void Register(object recipient, Func<Task> actionAsync, MessageType messageType)
        {
            if (!registeredRecipientsAsync.ContainsKey(messageType))
            {
                registeredRecipientsAsync.Add(messageType, new Dictionary<object, List<Func<Task>>>());
            }

            if (!registeredRecipientsAsync[messageType].ContainsKey(recipient))
            {
                registeredRecipientsAsync[messageType][recipient] = new List<Func<Task>>();
            }

            registeredRecipientsAsync[messageType][recipient].Add(actionAsync);
        }

        public void Unregister(object recipient, MessageType messageType)
        {
            if (registeredRecipients.ContainsKey(messageType))
            {
                registeredRecipients[messageType].Remove(recipient);
            }

            if (registeredRecipientsAsync.ContainsKey(messageType))
            {
                registeredRecipientsAsync[messageType].Remove(recipient);
            }
        }

        public async void SendMessage(object? sender, MessageType messageType)
        {
            try
            {
                if (registeredRecipients.ContainsKey(messageType))
                {
                    foreach (KeyValuePair<object, List<Action>> recipient in registeredRecipients[messageType])
                    {
                        if (recipient.Key == sender)
                        {
                            continue;
                        }

                        foreach (Action action in recipient.Value)
                        {
                            action();
                        }
                    }
                }

                if (registeredRecipientsAsync.ContainsKey(messageType))
                {
                    foreach (KeyValuePair<object, List<Func<Task>>> recipient in registeredRecipientsAsync[messageType])
                    {
                        if (recipient.Key == sender)
                        {
                            continue;
                        }

                        foreach (Func<Task> actionAsync in recipient.Value)
                        {
                            await actionAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // TODO: Perhaps implement a more graceful way to handle exceptions before it gets to this point?
                MessageBox.Show(TextUtility.BuildExceptionMessage(ex), "Unhandled Exception",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
