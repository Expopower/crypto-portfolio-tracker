﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using Microsoft.EntityFrameworkCore;

namespace CryptoPortfolioTracker
{
    public class CryptoPortfolioContext : DbContext
    {
        public DbSet<TaxableEvent> TaxableEvents { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<TransactionValue> TransactionValues { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<CurrencyRate> CurrencyRates { get; set; }
        public DbSet<CurrencyRateRetrievalDateRange> CurrencyRateRetrievalDateRanges { get; set; }
        public DbSet<CurrencyReferenceExchange> CurrencyReferenceExchanges { get; set; }

        public CryptoPortfolioContext()
        {
            DatabaseFileService.EnsureDbDirectory();
        }

        public CryptoPortfolioContext(DbContextOptions<CryptoPortfolioContext> options) : base(options)
        {
            DatabaseFileService.EnsureDbDirectory();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured) {
                options.UseSqlite($"Data Source={DatabaseFileService.DbPath}")
                    .EnableSensitiveDataLogging();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Set default values
            modelBuilder.Entity<TaxableEvent>()
                .Property(evt => evt.IsModified)
                .HasDefaultValue(true);

            // Enforce data integrity
            modelBuilder.Entity<Transaction>()
                .HasOne(tx => tx.Currency)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            // Seed basic data
            modelBuilder.Entity<Currency>().HasData(
                new Currency()
                {
                    ID = CurrencyService.CadCurrencyID,
                    CurrencyType = CurrencyType.Fiat,
                    CurrencyCode = "CAD",
                    Name = "Canadian Dollars"
                },
                new Currency()
                {
                    ID = CurrencyService.UsdCurrencyID,
                    CurrencyType = CurrencyType.Fiat,
                    CurrencyCode = "USD",
                    Name = "United States Dollars"
                },
                new Currency()
                {
                    ID = CurrencyService.CroCurrencyID,
                    CurrencyType = CurrencyType.Crypto,
                    CurrencyCode = "CRO",
                    Name = "Crypto.com Coin"
                },
                new Currency()
                {
                    ID = CurrencyService.BtcCurrencyID,
                    CurrencyType = CurrencyType.Crypto,
                    CurrencyCode = "BTC",
                    Name = "Bitcoin"
                },
                new Currency()
                {
                    ID = CurrencyService.EthCurrencyID,
                    CurrencyType = CurrencyType.Crypto,
                    CurrencyCode = "ETH",
                    Name = "Ethereum"
                },
                new Currency()
                {
                    ID = CurrencyService.UsdcCurrencyID,
                    CurrencyType = CurrencyType.Crypto,
                    CurrencyCode = "USDC",
                    Name = "USD Coin"
                },
                new Currency()
                {
                    ID = CurrencyService.TcadCurrencyID,
                    CurrencyType = CurrencyType.Crypto,
                    CurrencyCode = "TCAD",
                    Name = "TrueCAD"
                });
        }
    }
}
