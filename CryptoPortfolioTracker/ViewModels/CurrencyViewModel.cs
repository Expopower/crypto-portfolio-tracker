﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    public class CurrencyViewModel : ViewModelBase
    {
        private Currency _currency;
        private ObservableCollection<CurrencyExchangePairViewModel> _existingExchangePairs;
        private ObservableCollection<CurrencyOptionViewModel> _exchangeableCurrencies;

        public Currency Currency
        {
            get
            {
                return _currency;
            }
            set
            {
                _currency = value;
                OnPropertyChanged(nameof(Currency));
            }
        }

        public Guid CurrencyID
        {
            get
            {
                return _currency.ID;
            }
            set
            {
                _currency.ID = value;
                OnPropertyChanged(nameof(CurrencyID));
            }
        }

        public CurrencyType CurrencyType
        {
            get
            {
                return _currency.CurrencyType;
            }
            set
            {
                _currency.CurrencyType = value;
                OnPropertyChanged(nameof(CurrencyType));
            }
        }

        public string CurrencyCode
        {
            get
            {
                return _currency.CurrencyCode;
            }
            set
            {
                _currency.CurrencyCode = value;
                OnPropertyChanged(nameof(CurrencyCode));
            }
        }

        public string Name
        {
            get
            {
                return _currency.Name;
            }
            set
            {
                _currency.Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        public ObservableCollection<CurrencyExchangePairViewModel> ExistingExchangePairs
        {
            get
            {
                return _existingExchangePairs;
            }
            set
            {
                _existingExchangePairs = value;
                OnPropertyChanged(nameof(ExistingExchangePairs));
            }
        }

        public ObservableCollection<CurrencyOptionViewModel> ExchangeableCurrencies
        {
            get
            {
                return _exchangeableCurrencies;
            }
            set
            {
                _exchangeableCurrencies = value;
                OnPropertyChanged(nameof(ExchangeableCurrencies));
            }
        }

        public CurrencyViewModel()
        {
            _currency = new Currency();
            _existingExchangePairs = new ObservableCollection<CurrencyExchangePairViewModel>();
            _existingExchangePairs.CollectionChanged += OnExchangePairsChanged;
            LoadExchangeableCurrencies();
        }

        public CurrencyViewModel(Currency currency)
        {
            _currency = currency;
            LoadExistingExchangePairs();
            LoadExchangeableCurrencies();
        }

        private void LoadExistingExchangePairs()
        {
            var dateRangeService = new CurrencyRateRetrievalDateRangeService();
            ExistingExchangePairs = new ObservableCollection<CurrencyExchangePairViewModel>(
                Currency.ReferenceExchanges
                    .Select(exchange => new CurrencyExchangePairViewModel(
                        exchange,
                        Currency.RateRetrievalRanges.FirstOrDefault(range =>
                            range.FromCurrencyID == exchange.FromCurrencyID && range.ToCurrencyID == exchange.ToCurrencyID))));
            ExistingExchangePairs.CollectionChanged += OnExchangePairsChanged;
        }

        private void LoadExchangeableCurrencies()
        {
            IEnumerable<Currency> exchangeableCurrencies = new CurrencyService().GetCurrencies()
                .Where(currency =>
                    currency.ID != Currency.ID);

            ExchangeableCurrencies = new ObservableCollection<CurrencyOptionViewModel>(
                exchangeableCurrencies
                    .Select(currency => new CurrencyOptionViewModel(currency)));
        }

        private void OnExchangePairsChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            ApplyViewModelChangeToInternalCollection<CurrencyReferenceExchange, CurrencyExchangePairViewModel>(
                e,
                (itemList) => {
                    return itemList?
                        .OfType<CurrencyExchangePairViewModel>()
                        .Select(viewModel => viewModel.CurrencyReferenceExchange)
                        .FirstOrDefault();
                },
                Currency.ReferenceExchanges);
        }
    }
}
