﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    public class PreferredCurrenciesViewModel : ViewModelBase
    {
        private Guid _selectedFiatCurrencyID;
        private Guid _selectedCryptoCurrencyID;
        private List<CurrencyOptionViewModel> _fiatCurrencies;
        private List<CurrencyOptionViewModel> _cryptoCurrencies;

        public Guid SelectedFiatCurrencyID
        {
            get
            {
                return _selectedFiatCurrencyID;
            }
            set
            {
                _selectedFiatCurrencyID = value;
                OnPropertyChanged(nameof(SelectedFiatCurrencyID));
            }
        }

        public Guid SelectedCryptoCurrencyID
        {
            get
            {
                return _selectedCryptoCurrencyID;
            }
            set
            {
                _selectedCryptoCurrencyID = value;
                OnPropertyChanged(nameof(SelectedCryptoCurrencyID));
            }
        }

        public List<CurrencyOptionViewModel> FiatCurrencies
        {
            get
            {
                return _fiatCurrencies;
            }
            set
            {
                _fiatCurrencies = value;
                OnPropertyChanged(nameof(FiatCurrencies));
            }
        }

        public List<CurrencyOptionViewModel> CryptoCurrencies
        {
            get
            {
                return _cryptoCurrencies;
            }
            set
            {
                _cryptoCurrencies = value;
                OnPropertyChanged(nameof(CryptoCurrencies));
            }
        }

        public PreferredCurrenciesViewModel()
        {
            LoadPreferredCurrencies();
            LoadFiatCurrencies();
            LoadCryptoCurrencies();
        }

        public bool AreCurrenciesChanged()
        {
            return SelectedFiatCurrencyID != Properties.Settings.Default.PreferredFiatCurrencyID
                || SelectedCryptoCurrencyID != Properties.Settings.Default.PreferredCryptoCurrencyID;
        }

        public void Save()
        {
            Properties.Settings.Default.PreferredFiatCurrencyID = SelectedFiatCurrencyID;
            Properties.Settings.Default.PreferredCryptoCurrencyID = SelectedCryptoCurrencyID;
            Properties.Settings.Default.Save();
            Messenger.Instance.SendMessage(this, MessageType.PreferredCurrenciesUpdated);
        }

        private void LoadPreferredCurrencies()
        {
            SelectedFiatCurrencyID = Properties.Settings.Default.PreferredFiatCurrencyID;
            SelectedCryptoCurrencyID = Properties.Settings.Default.PreferredCryptoCurrencyID;
        }

        private void LoadFiatCurrencies()
        {
            FiatCurrencies = new CurrencyService().GetFiatCurrencies()
                .Select(c => new CurrencyOptionViewModel(c))
                .ToList();
        }

        private void LoadCryptoCurrencies()
        {
            CryptoCurrencies = new CurrencyService().GetCryptoCurrencies()
                .Select(c => new CurrencyOptionViewModel(c))
                .ToList();
        }
    }
}
