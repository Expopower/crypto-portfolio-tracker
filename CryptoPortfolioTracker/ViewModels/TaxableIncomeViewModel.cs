﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Properties;
using CryptoPortfolioTracker.Services;
using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    public class TaxableIncomeViewModel : ViewModelBase
    {
        private Dictionary<Guid, CurrencySummaryViewModel> _currencySummaries = new Dictionary<Guid, CurrencySummaryViewModel>();
        private string _taxBasisHeader;

        public int TaxYear { get; set; }
        public decimal CapitalGains
        {
            get
            {
                return CurrencySummaries.Sum(summary => summary.CapitalGains);
            }
        }

        public decimal Income
        {
            get
            {
                return CurrencySummaries.Sum(summary => summary.Income);
            }
        }

        public List<TransactionViewModel> ApplicableTransactions { get; set; } = new List<TransactionViewModel>();

        public List<CurrencySummaryViewModel> CurrencySummaries
        {
            get
            {
                return _currencySummaries.Values
                    .OrderBy(summary => summary.CurrencyDisplayName)
                    .ToList();
            }
        }

        public string TaxBasisHeader
        {
            get
            {
                return _taxBasisHeader;
            }
            set
            {
                _taxBasisHeader = value;
                OnPropertyChanged(nameof(TaxBasisHeader));
            }
        }

        public TaxableIncomeViewModel()
        {
            LoadCurrencyHeaders();
            Messenger.Instance.Register(this, LoadCurrencyHeaders, MessageType.PreferredCurrenciesUpdated);
        }

        public void AddTransaction(decimal currentAdjustedCostBase, Transaction transaction)
        {
            CurrencySummaryViewModel currencySummary = EnsureCurrencySummary(transaction.Currency);

            if (transaction.TaxableEvent.EventType == TaxableEventType.Exchange
                || transaction.TaxableEvent.EventType == TaxableEventType.Sale)
            {
                // Only disposal events result in changes to capital gains
                if (transaction.Amount < 0)
                {
                    decimal costBasis = currentAdjustedCostBase * transaction.Amount;
                    currencySummary.CapitalGains += Math.Abs(transaction.GetTaxBasis(Settings.Default.PreferredFiatCurrencyID))
                        - Math.Abs(costBasis);
                    ApplicableTransactions.Add(new TransactionViewModel(transaction));
                }
            }
            else if (transaction.TaxableEvent.EventType == TaxableEventType.Interest
                || transaction.TaxableEvent.EventType == TaxableEventType.Bonus)
            {
                currencySummary.Income += transaction.GetTaxBasis(Settings.Default.PreferredFiatCurrencyID);
                ApplicableTransactions.Add(new TransactionViewModel(transaction));
            }
            else if (transaction.TaxableEvent.EventType == TaxableEventType.Fee)
            {
                if (transaction.Amount < 0)
                {
                    currencySummary.CapitalGains += transaction.GetTaxBasis(Settings.Default.PreferredFiatCurrencyID);
                    ApplicableTransactions.Add(new TransactionViewModel(transaction));
                }
            }
        }

        private void LoadCurrencyHeaders()
        {
            Currency? fiatCurrency = new CurrencyService()
                .GetCurrency(Settings.Default.PreferredFiatCurrencyID);

            TaxBasisHeader = $"Tax Basis ({fiatCurrency?.CurrencyCode})";
        }

        private CurrencySummaryViewModel EnsureCurrencySummary(Currency currency)
        {
            if (_currencySummaries.ContainsKey(currency.ID))
            {
                return _currencySummaries[currency.ID];
            }
            else
            {
                var newSummary = new CurrencySummaryViewModel(currency);
                _currencySummaries.Add(currency.ID, newSummary);
                return newSummary;
            }
        }

        public class CurrencySummaryViewModel
        {
            private Currency _currency;

            [Name("Currency")]
            public string CurrencyDisplayName
            {
                get
                {
                    return $"{_currency.Name} ({_currency.CurrencyCode})";
                }
            }

            [Name("Capital Gains")]
            public decimal CapitalGains { get; set; }
            public decimal Income { get; set; }

            public CurrencySummaryViewModel(Currency currency)
            {
                _currency = currency;
            }
        }
    }
}
