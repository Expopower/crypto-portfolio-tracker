﻿using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Properties;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    public class TransactionViewModel : ViewModelBase
    {
        private Transaction _transaction;
        public Transaction Transaction
        {
            get
            {
                return _transaction;
            }
            set
            {
                _transaction = value;
                OnPropertyChanged(nameof(Transaction));
            }
        }

        public Guid TransactionID
        {
            get
            {
                return Transaction.ID;
            }
            set
            {
                Transaction.ID = value;
                OnPropertyChanged(nameof(TransactionID));
            }
        }

        public decimal Amount
        {
            get
            {
                return Transaction.Amount;
            }
            set
            {
                Transaction.Amount = value;
                OnPropertyChanged(nameof(Amount));
            }
        }

        public decimal? OverrideFiatValue
        {
            get
            {
                return Transaction.OverrideFiatValue;
            }
            set
            {
                Transaction.OverrideFiatValue = value;
                OnPropertyChanged(nameof(OverrideFiatValue));
                OnPropertyChanged(nameof(TaxBasisInFiat));
            }
        }

        public decimal? FiatValue
        {
            get
            {
                return Transaction.GetValue(Settings.Default.PreferredFiatCurrencyID)?.Amount;
            }
        }

        public decimal? CryptoValue
        {
            get
            {
                return Transaction.GetValue(Settings.Default.PreferredCryptoCurrencyID)?.Amount;
            }
        }

        public Guid TaxableEventID
        {
            get
            {
                return Transaction.TaxableEventID;
            }
            set
            {
                Transaction.TaxableEventID = value;
                OnPropertyChanged(nameof(TaxableEventID));
            }
        }

        public Guid CurrencyID
        {
            get
            {
                return Transaction.CurrencyID;
            }
            set
            {
                Transaction.CurrencyID = value;
                OnPropertyChanged(nameof(CurrencyID));
            }
        }

        public decimal TaxBasisInFiat
        {
            get
            {
                return Transaction.OverrideFiatValue ?? Transaction.GetValue(Settings.Default.PreferredFiatCurrencyID)?.Amount ?? 0;
            }
        }

        public TransactionViewModel()
        {
            _transaction = new Transaction();
        }

        public TransactionViewModel(Transaction transaction)
        {
            _transaction = transaction;
        }
    }
}
