﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Properties;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    public class CryptoHoldingListViewModel : ViewModelBase
    {
        private ObservableCollection<CryptoHoldingViewModel> _cryptoHoldings;
        private string _fiatValueHeader;
        private string _bookFiatUnitValueHeader;
        private string _bookCryptoUnitValueHeader;
        private string _marketFiatUnitValueHeader;
        private string _marketCryptoUnitValueHeader;

        public ObservableCollection<CryptoHoldingViewModel> CryptoHoldings
        {
            get
            {
                return _cryptoHoldings;
            }
            set
            {
                _cryptoHoldings = value;
                OnPropertyChanged(nameof(CryptoHoldings));
            }
        }

        public string ValueHeader
        {
            get
            {
                return _fiatValueHeader;
            }
            set
            {
                _fiatValueHeader = value;
                OnPropertyChanged(nameof(ValueHeader));
            }
        }

        public string BookFiatUnitValueHeader
        {
            get
            {
                return _bookFiatUnitValueHeader;
            }
            set
            {
                _bookFiatUnitValueHeader = value;
                OnPropertyChanged(nameof(BookFiatUnitValueHeader));
            }
        }

        public string BookCryptoUnitValueHeader
        {
            get
            {
                return _bookCryptoUnitValueHeader;
            }
            set
            {
                _bookCryptoUnitValueHeader = value;
                OnPropertyChanged(nameof(BookCryptoUnitValueHeader));
            }
        }


        public string MarketFiatUnitValueHeader
        {
            get
            {
                return _marketFiatUnitValueHeader;
            }
            set
            {
                _marketFiatUnitValueHeader = value;
                OnPropertyChanged(nameof(MarketFiatUnitValueHeader));
            }
        }

        public string MarketCryptoUnitValueHeader
        {
            get
            {
                return _marketCryptoUnitValueHeader;
            }
            set
            {
                _marketCryptoUnitValueHeader = value;
                OnPropertyChanged(nameof(MarketCryptoUnitValueHeader));
            }
        }

        public CryptoHoldingListViewModel()
        {
            _cryptoHoldings = new ObservableCollection<CryptoHoldingViewModel>();
            LoadCurrencyHeaders();
            Messenger.Instance.Register(this, RefreshAsyncStatic, MessageType.TaxableEventsUpdated);
            Messenger.Instance.Register(this, RefreshAsyncStatic, MessageType.CurrenciesUpdated);
            Messenger.Instance.Register(this, LoadCurrencyHeaders, MessageType.PreferredCurrenciesUpdated);
        }

        public async Task RefreshAsync()
        {
            await RefreshAsync(true);
        }

        public async Task RefreshAsyncStatic()
        {
            await RefreshAsync(false);
        }

        public async Task RefreshAsync(bool queryLatestRates)
        {
            List<CryptoHoldingViewModel> cryptoHoldingViews =
                await Task.Run(() => new AccountingService().CalculateCryptoHoldingsAsync(queryLatestRates));
            CryptoHoldings = new ObservableCollection<CryptoHoldingViewModel>(cryptoHoldingViews);
        }

        private void LoadCurrencyHeaders()
        {
            Currency? fiatCurrency = new CurrencyService()
                .GetCurrency(Settings.Default.PreferredFiatCurrencyID);
            Currency? cryptoCurrency = new CurrencyService()
                .GetCurrency(Settings.Default.PreferredCryptoCurrencyID);

            ValueHeader = $"Value ({fiatCurrency?.CurrencyCode})";
            BookFiatUnitValueHeader = $"Book Cost\nper Unit ({fiatCurrency?.CurrencyCode})";
            BookCryptoUnitValueHeader = $"Book Cost\nper Unit ({cryptoCurrency?.CurrencyCode})";
            MarketFiatUnitValueHeader = $"Market Cost\nper Unit ({fiatCurrency?.CurrencyCode})";
            MarketCryptoUnitValueHeader = $"Market Cost\nper Unit ({cryptoCurrency?.CurrencyCode})";
        }
    }
}
