﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Services;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    public class TaxCalculationViewModel : ViewModelBase
    {
        private TaxableIncomeViewModel? _taxableIncome;
        private ObservableCollection<int>? _taxYears;
        private int _selectedTaxYear;

        public int SelectedTaxYear
        {
            get
            {
                return _selectedTaxYear;
            }
            set
            {
                _selectedTaxYear = value;
                OnPropertyChanged(nameof(SelectedTaxYear));
            }
        }

        public TaxableIncomeViewModel? TaxableIncome
        {
            get
            {
                return _taxableIncome;
            }
            set
            {
                _taxableIncome = value;
                OnPropertyChanged(nameof(TaxableIncome));
            }
        }

        public ObservableCollection<int>? TaxYears
        {
            get
            {
                return _taxYears;
            }
            set
            {
                _taxYears = value;
                OnPropertyChanged(nameof(TaxYears));
            }
        }

        public TaxCalculationViewModel()
        {
            LoadTaxYears();
            Messenger.Instance.Register(this, LoadTaxYears, MessageType.TaxableEventsUpdated);
        }

        private void LoadTaxYears()
        {
            TaxYears = new ObservableCollection<int>(new TaxableEventService().GetTaxYears());
            SetDefaultTaxYear();
        }

        private void SetDefaultTaxYear()
        {
            if (TaxYears?.Any() == true)
            {
                SelectedTaxYear = TaxYears.First();
            }
        }

        public async Task RefreshAsync()
        {
            TaxableIncome = await Task.Run(() => new AccountingService().CalculateTaxableIncomeAsync(SelectedTaxYear));
        }

        public async Task GenerateCsvAsync()
        {
            await Task.Run(() => new AccountingService().GenerateTaxableIncomeCsvAsync(SelectedTaxYear));
        }
    }
}
