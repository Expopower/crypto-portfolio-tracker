﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Properties;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace CryptoPortfolioTracker.ViewModels
{
    public class TransactionInputViewModel : ViewModelBase
    {
        private List<Currency> _availableCurrencies;
        private Transaction _transaction;
        private Currency? _selectedCurrency;
        private bool _allowOverridingAmount;
        private string _overrideValueHeader;

        public List<Currency> AvailableCurrencies
        {
            get
            {
                return _availableCurrencies;
            }
            set
            {
                _availableCurrencies = value;
                OnPropertyChanged(nameof(AvailableCurrencies));
            }
        }

        public Transaction Transaction
        {
            get
            {
                return _transaction;
            }
            set
            {
                _transaction = value;
                OnPropertyChanged(nameof(Transaction));
            }
        }

        public decimal Amount
        {
            get
            {
                return Transaction.Amount;
            }
            set
            {
                Transaction.Amount = value;
                OnPropertyChanged(nameof(Amount));
            }
        }

        public decimal? OverrideFiatValue
        {
            get
            {
                return Transaction.OverrideFiatValue;
            }
            set
            {
                Transaction.OverrideFiatValue = value;
                OnPropertyChanged(nameof(OverrideFiatValue));
            }
        }

        public Guid CurrencyID
        {
            get
            {
                return Transaction.CurrencyID;
            }
            set
            {
                Transaction.CurrencyID = value;
                OnPropertyChanged(nameof(CurrencyID));
            }
        }

        // TODO: Should this be a ViewModel instead of the model directly?
        public Currency? SelectedCurrency
        {
            get
            {
                return _selectedCurrency;
            }
            set
            {
                _selectedCurrency = value;
                OnPropertyChanged(nameof(SelectedCurrency));
            }
        }

        public bool AllowOverridingAmount
        {
            get
            {
                return _allowOverridingAmount;
            }
            set
            {
                _allowOverridingAmount = value;
                OnPropertyChanged(nameof(AllowOverridingAmount));
            }
        }

        public string OverrideValueHeader
        {
            get
            {
                return _overrideValueHeader;
            }
            set
            {
                _overrideValueHeader = value;
                OnPropertyChanged(nameof(OverrideValueHeader));
            }
        }

        public TransactionInputViewModel(CurrencyType currencyType, bool isDeduction, bool allowOverridingAmount)
        {
            LoadAvailableCurrencies(currencyType);
            LoadCurrencyHeaders();
            _transaction = new Transaction();
            _allowOverridingAmount = allowOverridingAmount;
            _selectedCurrency = _availableCurrencies?.FirstOrDefault();

            if (isDeduction)
            {
                PropertyChanged += EnsureNegativeTransactionAmounts;
            }

            Messenger.Instance.Register(this, LoadCurrencyHeaders, MessageType.PreferredCurrenciesUpdated);
        }

        private void LoadAvailableCurrencies(CurrencyType currencyType)
        {
            if (currencyType == CurrencyType.Crypto)
            {
                AvailableCurrencies = new CurrencyService().GetCryptoCurrencies();
            }
            else
            {
                AvailableCurrencies = new CurrencyService().GetFiatCurrencies();
            }
        }

        private void LoadCurrencyHeaders()
        {
            Currency? fiatCurrency = new CurrencyService()
                .GetCurrency(Settings.Default.PreferredFiatCurrencyID);

            OverrideValueHeader = $"Override Value ({fiatCurrency?.CurrencyCode})";
        }

        private void EnsureNegativeTransactionAmounts(object? sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Transaction.Amount))
            {
                Transaction.Amount = EnsureNegative(Transaction.Amount);
            }
            if (e.PropertyName == nameof(Transaction.OverrideFiatValue))
            {
                Transaction.OverrideFiatValue = EnsureNegative(Transaction.OverrideFiatValue);
            }
        }

        private decimal EnsureNegative(decimal input)
        {
            return Math.Abs(input) * -1;
        }

        private decimal? EnsureNegative(decimal? input)
        {
            if (!input.HasValue)
            {
                return input;
            }

            return EnsureNegative(input.Value);
        }
    }
}
