﻿using CryptoPortfolioTracker.DataImporters;
using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CryptoPortfolioTracker.ViewModels
{
    public class ImportTaxableEventsViewModel : ViewModelBase
    {
        private IDataImporter<TaxableEvent> _dataImporter;
        private DateTime? _importAfterDate;
        private DateTime? _importBeforeDate;
        private bool _skipDuplicates;
        private FileInputViewModel _importFile;

        private List<TaxableEvent> _importedTaxableEvents;
        private List<TaxableEvent> _duplicateTaxableEvents;
        private string _saveConfirmationMessage;

        public string DialogTitle
        {
            get
            {
                return _dataImporter.DialogTitle;
            }
        }

        public string DialogNote
        {
            get
            {
                return _dataImporter.DialogNote;
            }
        }

        public bool HasDialogNote
        {
            get
            {
                return !string.IsNullOrWhiteSpace(_dataImporter.DialogNote);
            }
        }

        public string SaveActionLabel
        {
            get
            {
                return _dataImporter.SaveActionLabel;
            }
        }

        public string SaveConfirmationMessage
        {
            get
            {
                return _saveConfirmationMessage;
            }
            set
            {
                _saveConfirmationMessage = value;
                OnPropertyChanged(nameof(SaveConfirmationMessage));
            }
        }

        public DateTime? ImportAfterDate
        {
            get
            {
                return _importAfterDate;
            }
            set
            {
                _importAfterDate = value;
                OnPropertyChanged(nameof(ImportAfterDate));
            }
        }

        public DateTime? ImportBeforeDate
        {
            get
            {
                return _importBeforeDate;
            }
            set
            {
                _importBeforeDate = value;
                OnPropertyChanged(nameof(ImportBeforeDate));
            }
        }

        public bool SkipDuplicates
        {
            get
            {
                return _skipDuplicates;
            }
            set
            {
                _skipDuplicates = value;
                OnPropertyChanged(nameof(SkipDuplicates));
            }
        }

        public FileInputViewModel ImportFile
        {
            get
            {
                return _importFile;
            }
            set
            {
                _importFile = value;
                OnPropertyChanged(nameof(ImportFile));
            }
        }

        public ImportTaxableEventsViewModel(IDataImporter<TaxableEvent> dataImporter)
        {
            _importFile = new FileInputViewModel();
            _saveConfirmationMessage = string.Empty;
            _dataImporter = dataImporter;
            _importedTaxableEvents = new List<TaxableEvent>();
            _duplicateTaxableEvents = new List<TaxableEvent>();
        }

        public bool CanImport()
        {
            return ImportFile.HasValue;
        }

        public async Task ImportDataAsync()
        {
            (_importedTaxableEvents, _duplicateTaxableEvents, string? importMessage) = await _dataImporter.ImportDataAsync(
                ImportFile.FilePath, ImportAfterDate, ImportBeforeDate);

            if (SkipDuplicates)
            {
                _importedTaxableEvents = _importedTaxableEvents
                    .Except(_duplicateTaxableEvents)
                    .ToList();
            }

            SaveConfirmationMessage =
                BuildSaveConfirmationMessage(_importedTaxableEvents.Count, _duplicateTaxableEvents.Count, importMessage);
        }

        public async Task SaveDataAsync()
        {
            await Task.Run(() =>_dataImporter.SaveImportedDataAsync(_importedTaxableEvents));
            Messenger.Instance.SendMessage(this, MessageType.TaxableEventsUpdated);
        }

        private string BuildSaveConfirmationMessage(int importedCount, int duplicateCount, string? importMessage)
        {
            string confirmationMessage = string.Format(_dataImporter.BaseConfirmationMessage, importedCount);
            
            if (duplicateCount > 0)
            {
                if (SkipDuplicates)
                {
                    confirmationMessage += $"\n\nSkipping {duplicateCount} possible duplicate(s).";
                }
                else
                {
                    confirmationMessage += $"\n\n{duplicateCount} possible duplicate(s) were identified." +
                        $" Consider backing up database before proceeding.";
                }
            }

            if (!string.IsNullOrWhiteSpace(importMessage))
            {
                confirmationMessage = $"{importMessage}\n\n{confirmationMessage}";
            }

            return confirmationMessage;
        }
    }
}
