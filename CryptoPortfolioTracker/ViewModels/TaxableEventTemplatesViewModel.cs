﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    public class TaxableEventTemplatesViewModel : ViewModelBase
    {
        // TODO: If down the road I expect two-way binding to be required here, then
        // the property changed event will need to be raised for each property.
        public TaxableEventType SelectedEventType { get; set; }
        public DateTime CreatedDate { get; set; }

        // Exchange
        public TransactionInputViewModel ExchangeFromTransaction { get; set; }
        public TransactionInputViewModel ExchangeToTransaction { get; set; }

        // Interest
        public TransactionInputViewModel InterestTransaction { get; set; }
        public bool IsInterestRecurring { get; set; }
        public int InterestFrequency { get; set; }
        public DateTime InterestStartDate { get; set; }
        public DateTime InterestEndDate { get; set; }

        // Sale
        public TransactionInputViewModel SaleFromTransaction { get; set; }
        public TransactionInputViewModel SaleToTransaction { get; set; }

        // Purchase
        public TransactionInputViewModel PurchaseFromTransaction { get; set; }
        public TransactionInputViewModel PurchaseToTransaction { get; set; }

        public TaxableEventTemplatesViewModel()
        {
            SelectedEventType = TaxableEventType.Exchange;
            CreatedDate = DateTime.Now;

            ExchangeFromTransaction = new TransactionInputViewModel(CurrencyType.Crypto, true, true);
            ExchangeToTransaction = new TransactionInputViewModel(CurrencyType.Crypto, false, false);

            InterestTransaction = new TransactionInputViewModel(CurrencyType.Crypto, false, true);
            InterestFrequency = 1;
            InterestStartDate = DateTime.Now;
            InterestEndDate = DateTime.Now;

            SaleFromTransaction = new TransactionInputViewModel(CurrencyType.Crypto, true, false);
            SaleToTransaction = new TransactionInputViewModel(CurrencyType.Fiat, false, true);

            PurchaseFromTransaction = new TransactionInputViewModel(CurrencyType.Fiat, true, true);
            PurchaseToTransaction = new TransactionInputViewModel(CurrencyType.Crypto, false, false);
        }

        public async Task SaveAsync()
        {
            List<TaxableEvent> newTaxableEvents = await BuildTaxableEventsAsync(SelectedEventType);
            await Task.Run(() => new TaxableEventService().SaveTaxableEventsAsync(newTaxableEvents));
            Messenger.Instance.SendMessage(this, MessageType.TaxableEventsUpdated);
        }

        private async Task<List<TaxableEvent>> BuildTaxableEventsAsync(TaxableEventType selectedType)
        {
            var taxableEvents = new List<TaxableEvent>();

            switch (selectedType)
            {
                case TaxableEventType.Exchange:
                    await AddExchangeTaxableEventAsync(taxableEvents);
                    break;
                case TaxableEventType.Interest:
                    AddInterestTaxableEvent(taxableEvents);
                    break;
                case TaxableEventType.Sale:
                    await AddSaleTaxableEventAsync(taxableEvents);
                    break;
                case TaxableEventType.Purchase:
                    await AddPurchaseTaxableEventAsync(taxableEvents);
                    break;
                default:
                    break;
            }

            return taxableEvents;
        }

        private async Task AddExchangeTaxableEventAsync(List<TaxableEvent> taxableEvents)
        {
            Transaction fromTransaction = ExchangeFromTransaction.Transaction;
            Transaction toTransaction = ExchangeToTransaction.Transaction;

            await new TransactionService().SynchronizeToAndFromTransactionsAsync(
                toTransaction, fromTransaction, TaxableEventType.Exchange, CreatedDate);

            var taxableEvent = new TaxableEvent()
            {
                EventType = TaxableEventType.Exchange,
                CreatedDateTime = CreatedDate,
                Description = $"Exchange from {ExchangeFromTransaction.SelectedCurrency?.CurrencyCode} to {ExchangeToTransaction.SelectedCurrency?.CurrencyCode}",
                Transactions = new List<Transaction>()
                {
                    fromTransaction,
                    toTransaction
                }
            };

            taxableEvents.Add(taxableEvent);
        }

        private void AddInterestTaxableEvent(List<TaxableEvent> taxableEvents)
        {
            if (IsInterestRecurring)
            {
                for (DateTime eventDate = InterestStartDate; eventDate <= InterestEndDate; eventDate = eventDate.AddDays(InterestFrequency))
                {
                    Transaction transaction = InterestTransaction.Transaction;
                    var taxableEvent = new TaxableEvent()
                    {
                        EventType = TaxableEventType.Interest,
                        CreatedDateTime = eventDate,
                        Description = $"Interest from {InterestTransaction.SelectedCurrency?.CurrencyCode} investment",
                        Transactions = new List<Transaction>()
                        {
                            transaction
                        }
                    };

                    taxableEvents.Add(taxableEvent);
                }
            }
            else
            {
                Transaction transaction = InterestTransaction.Transaction;
                var taxableEvent = new TaxableEvent()
                {
                    EventType = TaxableEventType.Interest,
                    CreatedDateTime = CreatedDate,
                    Description = $"Interest from {InterestTransaction.SelectedCurrency?.CurrencyCode} investment",
                    Transactions = new List<Transaction>()
                    {
                        transaction
                    }
                };

                taxableEvents.Add(taxableEvent);
            }
        }

        private async Task AddSaleTaxableEventAsync(List<TaxableEvent> taxableEvents)
        {
            Transaction fromTransaction = SaleFromTransaction.Transaction;
            Transaction toTransaction = SaleToTransaction.Transaction;

            await new TransactionService().SynchronizeToAndFromTransactionsAsync(
                toTransaction, fromTransaction, TaxableEventType.Sale, CreatedDate);

            var taxableEvent = new TaxableEvent()
            {
                EventType = TaxableEventType.Sale,
                CreatedDateTime = CreatedDate,
                Description = $"Sale of {SaleFromTransaction.SelectedCurrency?.CurrencyCode} for {SaleToTransaction.SelectedCurrency?.CurrencyCode}",
                Transactions = new List<Transaction>()
                {
                    fromTransaction,
                    toTransaction
                }
            };

            taxableEvents.Add(taxableEvent);
        }

        private async Task AddPurchaseTaxableEventAsync(List<TaxableEvent> taxableEvents)
        {
            Transaction fromTransaction = PurchaseFromTransaction.Transaction;
            Transaction toTransaction = PurchaseToTransaction.Transaction;

            await new TransactionService().SynchronizeToAndFromTransactionsAsync(
                toTransaction, fromTransaction, TaxableEventType.Purchase, CreatedDate);

            var taxableEvent = new TaxableEvent()
            {
                EventType = TaxableEventType.Purchase,
                CreatedDateTime = CreatedDate,
                Description = $"Purchase of {PurchaseToTransaction.SelectedCurrency?.CurrencyCode} with {PurchaseFromTransaction.SelectedCurrency?.CurrencyCode}",
                Transactions = new List<Transaction>()
                {
                    fromTransaction,
                    toTransaction
                }
            };

            taxableEvents.Add(taxableEvent);
        }
    }
}
