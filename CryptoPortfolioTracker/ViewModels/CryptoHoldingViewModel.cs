﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Properties;
using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    /// <summary>
    /// Only suitable for one-time databinding; can be updated if the situation changes.
    /// </summary>
    public class CryptoHoldingViewModel : ViewModelBase
    {
        private Currency _currency;

        public Guid CurrencyID
        {
            get
            {
                return _currency.ID;
            }
        }
        public string CurrencyDisplayName
        {
            get
            {
                return $"{_currency.Name} ({_currency.CurrencyCode})";
            }
        }

        public decimal Balance { get; set; }
        public CryptoHoldingValueViewModel FiatValue { get; } = new CryptoHoldingValueViewModel();
        public CryptoHoldingValueViewModel CryptoValue { get; } = new CryptoHoldingValueViewModel();
        public decimal? FiatValuePerUnit { get; set; }
        public decimal? CryptoValuePerUnit { get; set; }
        public decimal? CurrentFiatValue { get; set; }
        public decimal? ReturnOnInvestment { get; set; }

        public CryptoHoldingViewModel(Currency currency)
        {
            _currency = currency;
        }

        public void AddTransaction(Transaction transaction)
        {
            bool isDeduction = transaction.Amount < 0;

            // Fees from transfers (e.g. withdrawal/network fees) are applied at current value
            if (transaction.TaxableEvent.EventType == TaxableEventType.Fee)
            {
                FiatValue.AddValue(transaction.GetTaxBasis(Settings.Default.PreferredFiatCurrencyID));
                CryptoValue.AddValue(transaction.GetValue(Settings.Default.PreferredCryptoCurrencyID)?.Amount ?? 0);
            }
            // For deductions, use the historical adjusted cost base rather than the actual value.
            // Discrepancies between the two values will be assessed as a capital gain/loss elsewhere.
            else if (!isDeduction)
            {
                FiatValue.AddValue(transaction.GetTaxBasis(Settings.Default.PreferredFiatCurrencyID));
                CryptoValue.AddValue(transaction.GetValue(Settings.Default.PreferredCryptoCurrencyID)?.Amount ?? 0);
            }
            else
            {
                FiatValue.SubtractUnits(transaction.Amount);
                CryptoValue.SubtractUnits(transaction.Amount);
            }

            Balance += transaction.Amount;
            FiatValue.RecalculateCostBase(Balance);
            CryptoValue.RecalculateCostBase(Balance);

            if (Balance < 0)
            {
                LogError("Balance Calculation Discrepancy",
                    $"Balance dropped below 0 when adding transaction {transaction.ID}." +
                    $"\nSubtracted amount: {transaction.Amount} {transaction.Currency.CurrencyCode}." +
                    $"\nPossibly missing acquisitions before {transaction.TaxableEvent.CreatedDateTime}.");
            }
        }

        public class CryptoHoldingValueViewModel : ViewModelBase
        {
            public decimal HistoricalValue { get; set; }
            public decimal AdjustedCostBase { get; set; }

            public void AddValue(decimal value)
            {
                HistoricalValue += value;
            }

            public void SubtractUnits(decimal unitQuantity)
            {
                decimal negativeQuantity = Math.Abs(unitQuantity) * -1;
                decimal negativeValue = AdjustedCostBase * negativeQuantity;
                HistoricalValue += negativeValue;
            }

            public void RecalculateCostBase(decimal unitQuantity)
            {
                AdjustedCostBase = MathUtility.SafeDivide(HistoricalValue, unitQuantity);
            }
        }
    }
}
