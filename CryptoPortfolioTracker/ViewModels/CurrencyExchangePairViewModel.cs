﻿using CryptoPortfolioTracker.Models;
using System;

namespace CryptoPortfolioTracker.ViewModels
{
    public class CurrencyExchangePairViewModel : ViewModelBase
    {
        private CurrencyReferenceExchange _currencyReferenceExchange;
        private CurrencyRateRetrievalDateRange? _currencyRateRetrievalDateRange;

        public CurrencyReferenceExchange CurrencyReferenceExchange
        {
            get
            {
                return _currencyReferenceExchange;
            }
            set
            {
                _currencyReferenceExchange = value;
                OnPropertyChanged(nameof(CurrencyReferenceExchange));
            }
        }

        public CurrencyRateRetrievalDateRange? CurrencyRateRetrievalDateRange
        {
            get
            {
                return _currencyRateRetrievalDateRange;
            }
            set
            {
                _currencyRateRetrievalDateRange = value;
                OnPropertyChanged(nameof(CurrencyRateRetrievalDateRange));
            }
        }

        public Guid? CurrencyRateRetrievalDateRangeID
        {
            get
            {
                return CurrencyRateRetrievalDateRange?.ID;
            }
            set
            {
                if (_currencyRateRetrievalDateRange != null && value != null)
                {
                    _currencyRateRetrievalDateRange.ID = value.Value;
                }
                OnPropertyChanged(nameof(CurrencyRateRetrievalDateRangeID));
            }
        }

        public DateTime? StartDate
        {
            get
            {
                return _currencyRateRetrievalDateRange?.StartDate;
            }
            set
            {
                if (_currencyRateRetrievalDateRange != null && value != null)
                {
                    _currencyRateRetrievalDateRange.StartDate = value.Value;
                }
                OnPropertyChanged(nameof(StartDate));
            }
        }

        public DateTime? EndDate
        {
            get
            {
                return _currencyRateRetrievalDateRange?.EndDate;
            }
            set
            {
                if (_currencyRateRetrievalDateRange != null && value != null)
                {
                    _currencyRateRetrievalDateRange.EndDate = value.Value;
                }
                OnPropertyChanged(nameof(EndDate));
            }
        }

        public bool HasRateRetrievalRange
        {
            get
            {
                return _currencyRateRetrievalDateRange != null;
            }
        }

        public Guid CurrencyReferenceExchangeID
        {
            get
            {
                return _currencyReferenceExchange.ID;
            }
            set
            {
                _currencyReferenceExchange.ID = value;
                OnPropertyChanged(nameof(CurrencyReferenceExchangeID));
            }
        }

        public string? ExchangeName
        {
            get
            {
                return _currencyReferenceExchange.ExchangeName;
            }
            set
            {
                _currencyReferenceExchange.ExchangeName = value;
                OnPropertyChanged(nameof(ExchangeName));
            }
        }

        public Guid FromCurrencyID
        {
            get
            {
                return _currencyReferenceExchange.FromCurrencyID;
            }
            set
            {
                _currencyReferenceExchange.FromCurrencyID = value;
                OnPropertyChanged(nameof(FromCurrencyID));
            }
        }

        public Guid ToCurrencyID
        {
            get
            {
                return _currencyReferenceExchange.ToCurrencyID;
            }
            set
            {
                _currencyReferenceExchange.ToCurrencyID = value;
                OnPropertyChanged(nameof(ToCurrencyID));
            }
        }

        public bool IsInverted
        {
            get
            {
                return _currencyReferenceExchange.IsInverted;
            }
            set
            {
                _currencyReferenceExchange.IsInverted = value;
                OnPropertyChanged(nameof(IsInverted));
            }
        }

        public CurrencyExchangePairViewModel()
        {
            _currencyReferenceExchange = new CurrencyReferenceExchange();
        }

        public CurrencyExchangePairViewModel(CurrencyReferenceExchange currencyReferenceExchange,
            CurrencyRateRetrievalDateRange? currencyRateRetrievalDateRange)
        {
            _currencyReferenceExchange = currencyReferenceExchange;
            _currencyRateRetrievalDateRange = currencyRateRetrievalDateRange;
        }
    }
}
