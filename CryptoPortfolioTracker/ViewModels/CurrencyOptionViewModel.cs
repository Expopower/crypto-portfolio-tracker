﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    public class CurrencyOptionViewModel : ViewModelBase
    {
        private Currency _currency;

        public Currency Currency
        {
            get
            {
                return _currency;
            }
            set
            {
                _currency = value;
                OnPropertyChanged(nameof(Currency));
            }
        }

        public Guid CurrencyID
        {
            get
            {
                return _currency.ID;
            }
            set
            {
                _currency.ID = value;
                OnPropertyChanged(nameof(CurrencyID));
            }
        }

        public string CurrencyDisplayName
        {
            get
            {
                return _currency.CurrencyCode;
            }
        }

        public string CurrencyDisplayLongName
        {
            get
            {
                return $"{_currency.CurrencyCode} ({_currency.Name})";
            }
        }

        public CurrencyOptionViewModel(Currency currency)
        {
            _currency = currency;
        }
    }
}
