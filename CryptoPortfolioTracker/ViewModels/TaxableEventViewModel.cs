﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Properties;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CryptoPortfolioTracker.ViewModels
{
    public class TaxableEventViewModel : ViewModelBase
    {
        private TaxableEvent _taxableEvent;
        private ObservableCollection<TransactionViewModel> _transactions;

        private string _overrideValueHeader;
        private string _fiatValueHeader;
        private string _cryptoValueHeader;

        public TaxableEvent TaxableEvent
        {
            get
            {
                return _taxableEvent;
            }
            set
            {
                _taxableEvent = value;
                LoadTransactions();
                OnPropertyChanged(nameof(TaxableEvent));
            }
        }

        public ObservableCollection<TransactionViewModel> Transactions
        {
            get
            {
                return _transactions;
            }
            set
            {
                _transactions = value;
                OnPropertyChanged(nameof(Transactions));
            }
        }

        public Guid TaxableEventID
        {
            get
            {
                return TaxableEvent.ID;
            }
            set
            {
                TaxableEvent.ID = value;
                OnPropertyChanged(nameof(TaxableEventID));
            }
        }

        public TaxableEventType EventType
        {
            get
            {
                return TaxableEvent.EventType;
            }
            set
            {
                TaxableEvent.EventType = value;
                OnPropertyChanged(nameof(EventType));
            }
        }

        public decimal? NetValueInFiat
        {
            get
            {
                return TaxableEvent.NetValueInFiat;
            }
            set
            {
                TaxableEvent.NetValueInFiat = value;
                OnPropertyChanged(nameof(NetValueInFiat));
            }
        }

        public string? Description
        {
            get
            {
                return TaxableEvent.Description;
            }
            set
            {
                TaxableEvent.Description = value;
                OnPropertyChanged(nameof(Description));
            }
        }

        public DateTime CreatedDateTime
        {
            get
            {
                return TaxableEvent.CreatedDateTime;
            }
            set
            {
                TaxableEvent.CreatedDateTime = value;
                OnPropertyChanged(nameof(CreatedDateTime));
            }
        }

        public bool? IsModified
        {
            get
            {
                return TaxableEvent.IsModified;
            }
            set
            {
                TaxableEvent.IsModified = value;
                OnPropertyChanged(nameof(IsModified));
            }
        }

        public bool IsDisabled
        {
            get
            {
                return TaxableEvent.IsDisabled;
            }
            set
            {
                TaxableEvent.IsDisabled = value;
                OnPropertyChanged(nameof(IsDisabled));
            }
        }

        public string OverrideValueHeader
        {
            get
            {
                return _overrideValueHeader;
            }
            set
            {
                _overrideValueHeader = value;
                OnPropertyChanged(nameof(OverrideValueHeader));
            }
        }

        public string FiatValueHeader
        {
            get
            {
                return _fiatValueHeader;
            }
            set
            {
                _fiatValueHeader = value;
                OnPropertyChanged(nameof(FiatValueHeader));
            }
        }

        public string CryptoValueHeader
        {
            get
            {
                return _cryptoValueHeader;
            }
            set
            {
                _cryptoValueHeader = value;
                OnPropertyChanged(nameof(CryptoValueHeader));
            }
        }

        public TaxableEventViewModel()
        {
            _taxableEvent = new TaxableEvent();
            _transactions = new ObservableCollection<TransactionViewModel>();
            _transactions.CollectionChanged += OnTransactionsChanged;
            LoadCurrencyHeaders();
            Messenger.Instance.Register(this, LoadCurrencyHeaders, MessageType.PreferredCurrenciesUpdated);
            RegisterErrorChecks();
        }

        public TaxableEventViewModel(TaxableEvent taxableEvent)
        {
            _taxableEvent = taxableEvent;
            LoadTransactions();
            LoadCurrencyHeaders();
            Messenger.Instance.Register(this, LoadCurrencyHeaders, MessageType.PreferredCurrenciesUpdated);
            RegisterErrorChecks();
        }

        public async Task SaveAsync()
        {
            await Task.Run(() => new TaxableEventService().SaveTaxableEventAsync(this));
            Messenger.Instance.SendMessage(this, MessageType.TaxableEventsUpdated);
        }

        public void Delete()
        {
            new TaxableEventService().DeleteTaxableEvent(this);
            Messenger.Instance.SendMessage(this, MessageType.TaxableEventsUpdated);
        }

        private void LoadTransactions()
        {
            Transactions = new ObservableCollection<TransactionViewModel>(
                TaxableEvent.Transactions
                    .Select(tx => new TransactionViewModel(tx)));
            Transactions.CollectionChanged += OnTransactionsChanged;
        }

        private void LoadCurrencyHeaders()
        {
            Currency? fiatCurrency = new CurrencyService()
                .GetCurrency(Settings.Default.PreferredFiatCurrencyID);
            Currency? cryptoCurrency = new CurrencyService()
                .GetCurrency(Settings.Default.PreferredCryptoCurrencyID);

            OverrideValueHeader = $"Override Value ({fiatCurrency?.CurrencyCode})";
            FiatValueHeader = $"Value ({fiatCurrency?.CurrencyCode})";
            CryptoValueHeader = $"Value ({cryptoCurrency?.CurrencyCode})";
        }

        private void OnTransactionsChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            ApplyViewModelChangeToInternalCollection<Transaction, TransactionViewModel>(
                e,
                (itemList) => {
                    return itemList?
                        .OfType<TransactionViewModel>()
                        .Select(viewModel => viewModel.Transaction)
                        .FirstOrDefault();
                },
                TaxableEvent.Transactions);
        }

        private void RegisterErrorChecks()
        {
            RegisterErrorCheck(
                nameof(NetValueInFiat),
                "Missing Fiat Value",
                "One or more Transactions in this Taxable Event lack a value in preferred fiat currency.",
                IsMissingTransactionValueInFiat);

            RegisterErrorCheck(
                nameof(NetValueInFiat),
                "Event Type is Variable",
                "Manual review required before setting Event Type.",
                IsEventTypeVariable);
        }

        private bool IsMissingTransactionValueInFiat()
        {
            return TaxableEvent.IsMissingTransactionValueInFiat;
        }

        public bool IsEventTypeVariable()
        {
            return TaxableEvent.EventType == TaxableEventType.Variable;
        }
    }
}
