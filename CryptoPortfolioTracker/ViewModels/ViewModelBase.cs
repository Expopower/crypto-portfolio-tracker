﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        private Dictionary<string, string> _errors = new Dictionary<string, string>();
        private Dictionary<string, List<ErrorChecker>> _errorCheckers = new Dictionary<string, List<ErrorChecker>>();

        public event PropertyChangedEventHandler PropertyChanged;

        public string ErrorLog
        {
            get
            {
                return string.Join("\n\n", _errors
                    .Select(pair => $"{pair.Key}:\n{pair.Value}")
                    .ToList());
            }
        }

        public bool HasErrors
        {
            get
            {
                return _errors.Any();
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            //this.VerifyPropertyName(propertyName);
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                var e = new PropertyChangedEventArgs(propertyName);
                handler(this, e);
            }
            RunErrorChecks(propertyName);
        }

        protected void ApplyViewModelChangeToInternalCollection<TModel, TViewModel>(NotifyCollectionChangedEventArgs viewModelChangeEvent,
            Func<IList?, TModel?> retrieveSingleModel, IList<TModel> internalCollection) where TViewModel : ViewModelBase
        {
            // For now since we're using ObservableCollection, we can safely assume only a single item is changed at a time.
            switch (viewModelChangeEvent.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    TModel? newItem = retrieveSingleModel(viewModelChangeEvent.NewItems);
                    if (newItem != null)
                    {
                        internalCollection.Add(newItem);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    TModel? oldItem = retrieveSingleModel(viewModelChangeEvent.OldItems);
                    if (oldItem != null)
                    {
                        internalCollection.Remove(oldItem);
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Adds an error in the model's log.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public void LogError(string title, string message)
        {
            if (!_errors.ContainsKey(title))
            {
                _errors.Add(title, message);
                OnPropertyChanged(nameof(ErrorLog));
                OnPropertyChanged(nameof(HasErrors));
            }
        }

        /// <summary>
        /// Adds or removes the given error in the model's log.
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="isTriggered"></param>
        public void ToggleError(string title, string message, bool isTriggered)
        {
            if (!isTriggered)
            {
                _errors.Remove(title);
                OnPropertyChanged(nameof(ErrorLog));
                OnPropertyChanged(nameof(HasErrors));
            }
            else
            {
                LogError(title, message);
            }
        }

        /// <summary>
        /// Sets up an error that will be conditionally added to the log based on a supplied checking method.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="checkCondition"></param>
        /// <param name="executeImmediately">Whether the error condition should be immediately checked.</param>
        public void RegisterErrorCheck(string propertyName, string title, string message, Func<bool> checkCondition,
            bool executeImmediately = true)
        {
            if (!_errorCheckers.ContainsKey(propertyName))
            {
                _errorCheckers.Add(propertyName, new List<ErrorChecker>());
            }
            _errorCheckers[propertyName].Add(new ErrorChecker(title, message, checkCondition));

            if (executeImmediately)
            {
                ToggleError(title, message, checkCondition());
            }
        }

        private void RunErrorChecks(string propertyName)
        {
            if (_errorCheckers.ContainsKey(propertyName))
            {
                foreach (ErrorChecker errorChecker in _errorCheckers[propertyName])
                {
                    ToggleError(errorChecker.ErrorTitle, errorChecker.ErrorMessage, errorChecker.CheckCondition());
                }
            }
        }

        private class ErrorChecker
        {
            public string ErrorTitle { get; }
            public string ErrorMessage { get; }
            public Func<bool> CheckCondition { get; }

            public ErrorChecker(string errorTitle, string errorMessage, Func<bool> checkCondition)
            {
                ErrorTitle = errorTitle;
                ErrorMessage = errorMessage;
                CheckCondition = checkCondition;
            }
        }

        //[Conditional("DEBUG")]
        //[DebuggerStepThrough]
        //public void VerifyPropertyName(string propertyName)
        //{
        //    // Verify that the property name matches a real, 
        //    // public, instance property on this object. 
        //    if (TypeDescriptor.GetProperties(this)[propertyName] == null)
        //    {
        //        string msg = "Invalid property name: " + propertyName;
        //        if (this.ThrowOnInvalidPropertyName)
        //            throw new Exception(msg);
        //        else
        //            Debug.Fail(msg);
        //    }
        //}
    }
}
