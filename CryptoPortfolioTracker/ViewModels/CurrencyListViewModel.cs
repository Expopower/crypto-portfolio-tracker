﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.ViewModels
{
    public class CurrencyListViewModel : ViewModelBase
    {
        private CurrencyViewModel? _selectedCurrency;
        private CurrencyExchangePairViewModel? _selectedExchangePair;
        private ObservableCollection<CurrencyViewModel> _currencies;
        private CurrencyType _currencyType;

        public CurrencyViewModel? SelectedCurrency
        {
            get
            {
                return _selectedCurrency;
            }
            set
            {
                _selectedCurrency = value;
                OnPropertyChanged(nameof(SelectedCurrency));
            }
        }

        public CurrencyExchangePairViewModel? SelectedExchangePair
        {
            get
            {
                return _selectedExchangePair;
            }
            set
            {
                _selectedExchangePair = value;
                OnPropertyChanged(nameof(SelectedExchangePair));
            }
        }

        public ObservableCollection<CurrencyViewModel> Currencies
        {
            get
            {
                return _currencies;
            }
            set
            {
                _currencies = value;
                OnPropertyChanged(nameof(Currencies));
            }
        }

        public string ListTitle
        {
            get
            {
                return $"Manage {_currencyType} Currencies";
            }
        }

        public bool IsExchangeEditable
        {
            get
            {

                // For now, only a single source of data for fiat currencies
                return _currencyType != CurrencyType.Fiat;
            }
        }

        public CurrencyListViewModel(CurrencyType currencyType)
        {
            _currencyType = currencyType;
            LoadCurrencies();
            Currencies.CollectionChanged += OnCurrenciesChanged;
        }

        public async Task SaveAsync()
        {
            await Task.Run(() => new CurrencyService().SaveCurrenciesAsync(Currencies, _currencyType));
            Messenger.Instance.SendMessage(this, MessageType.CurrenciesUpdated);
        }

        public async Task TrimExchangeRateRange()
        {
            if (SelectedExchangePair != null)
            {
                List<CurrencyRate> currencyRates = await new CurrencyRateService().GetCurrencyRatesAsync(
                    SelectedExchangePair.FromCurrencyID, SelectedExchangePair.ToCurrencyID);
                DateTime minDate = currencyRates.Select(rate => rate.EffectiveDateTime).Min();
                DateTime maxDate = currencyRates.Select(rate => rate.EffectiveDateTime).Max();

                SelectedExchangePair.StartDate = minDate;
                SelectedExchangePair.EndDate = maxDate;
            }
        }

        private void LoadCurrencies()
        {
            Currencies = new ObservableCollection<CurrencyViewModel>(
                new CurrencyService().GetCurrencies(_currencyType)
                .Select(c => new CurrencyViewModel(c)));
        }

        private void OnCurrenciesChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            // For now since we're using ObservableCollection, we can safely assume only a single item is changed at a time.
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    CurrencyViewModel? newItem = e.NewItems?
                        .OfType<CurrencyViewModel>()
                        .FirstOrDefault();

                    // Ensure new items are consistent with the list context
                    if (newItem != null)
                    {
                        newItem.CurrencyType = _currencyType;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
