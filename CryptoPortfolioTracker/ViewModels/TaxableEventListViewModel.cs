﻿using CryptoPortfolioTracker.Messaging;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Properties;
using CryptoPortfolioTracker.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CryptoPortfolioTracker.ViewModels
{
    public class TaxableEventListViewModel : ViewModelBase
    {
        private ObservableCollection<TaxableEventViewModel> _taxableEvents;
        private CollectionViewSource? _taxableEventsViewSource;

        private TaxableEventType? _filterEventType;
        private string? _filterText;
        private bool _filterOnlyIsModified;
        private bool _filterOnlyHasErrors;
        private string _netValueHeader;

        public TaxableEventViewModel? SelectedTaxableEvent { get; set; }
        
        public ObservableCollection<TaxableEventViewModel> TaxableEvents
        {
            get
            {
                return _taxableEvents;
            }
            set
            {
                _taxableEvents = value;
                TaxableEventsViewSource.Source = value;
                OnPropertyChanged(nameof(FilteredTaxableEvents));
                OnPropertyChanged(nameof(TaxableEvents));
            }
        }

        public ICollectionView FilteredTaxableEvents
        {
            get
            {
                return TaxableEventsViewSource.View;
            }
        }

        public TaxableEventType? FilterEventType
        {
            get
            {
                return _filterEventType;
            }
            set
            {
                _filterEventType = value;
                TaxableEventsViewSource.View.Refresh();
                OnPropertyChanged(nameof(FilterEventType));
            }
        }

        public string? FilterText
        {
            get
            {
                return _filterText;
            }
            set
            {
                _filterText = value;
                TaxableEventsViewSource.View.Refresh();
                OnPropertyChanged(nameof(FilterText));
            }
        }

        public bool FilterOnlyIsModified
        {
            get
            {
                return _filterOnlyIsModified;
            }
            set
            {
                _filterOnlyIsModified = value;
                TaxableEventsViewSource.View.Refresh();
                OnPropertyChanged(nameof(FilterOnlyIsModified));
            }
        }

        public bool FilterOnlyHasErrors
        {
            get
            {
                return _filterOnlyHasErrors;
            }
            set
            {
                _filterOnlyHasErrors = value;
                TaxableEventsViewSource.View.Refresh();
                OnPropertyChanged(nameof(FilterOnlyHasErrors));
            }
        }

        public string NetValueHeader
        {
            get
            {
                return _netValueHeader;
            }
            set
            {
                _netValueHeader = value;
                OnPropertyChanged(nameof(NetValueHeader));
            }
        }

        private CollectionViewSource TaxableEventsViewSource
        {
            get
            {
                if (_taxableEventsViewSource == null)
                {
                    _taxableEventsViewSource = new CollectionViewSource();
                    _taxableEventsViewSource.Filter += FilterTaxableEvents;
                }

                return _taxableEventsViewSource;
            }
        }

        public TaxableEventListViewModel()
        {
            LoadTaxableEvents();
            LoadCurrencyHeaders();
            Messenger.Instance.Register(this, LoadTaxableEvents, MessageType.TaxableEventsUpdated);
            Messenger.Instance.Register(this, LoadCurrencyHeaders, MessageType.PreferredCurrenciesUpdated);
        }

        public void DeleteTaxableEvents(List<TaxableEventViewModel> taxableEventsToDelete)
        {
            if (taxableEventsToDelete.Count == 1)
            {
                TaxableEventViewModel deletedEvent = taxableEventsToDelete[0];

                TaxableEvents.Remove(deletedEvent);
                deletedEvent.Delete();
            }
            else
            {
                HashSet<Guid> deletedEventIDs = taxableEventsToDelete
                    .Select(evt => evt.TaxableEventID)
                    .ToHashSet();
                
                TaxableEvents = new ObservableCollection<TaxableEventViewModel>(
                    TaxableEvents.Where(evt => !deletedEventIDs.Contains(evt.TaxableEventID)));
                new TaxableEventService().DeleteTaxableEvents(taxableEventsToDelete);
            }
            Messenger.Instance.SendMessage(this, MessageType.TaxableEventsUpdated);
        }

        public async Task ClearModifiedTaxableEventsAsync()
        {
            await new TaxableEventService().ClearModifiedTaxableEventsAsync();
            LoadTaxableEvents();
        }

        public async Task EnsureCurrencyValues()
        {
            await new TransactionService().EnsureAlternativeCurrencyAmountsForExistingTransactionsAsync();
            LoadTaxableEvents();
        }

        public void ClearFilters()
        {
            // Avoid setting the properties so View is not refreshed multiple times
            _filterEventType = null;
            _filterText = null;
            _filterOnlyIsModified = false;
            _filterOnlyHasErrors = false;

            TaxableEventsViewSource.View.Refresh();
            OnPropertyChanged(nameof(FilterEventType));
            OnPropertyChanged(nameof(FilterText));
            OnPropertyChanged(nameof(FilterOnlyIsModified));
            OnPropertyChanged(nameof(FilterOnlyHasErrors));
        }

        private void LoadTaxableEvents()
        {
            TaxableEvents = new ObservableCollection<TaxableEventViewModel>(
                new TaxableEventService().GetTaxableEvents()
                    .Select(evt => new TaxableEventViewModel(evt)));
        }

        private void LoadCurrencyHeaders()
        {
            Currency? fiatCurrency = new CurrencyService()
                .GetCurrency(Settings.Default.PreferredFiatCurrencyID);

            NetValueHeader = $"Net Value ({fiatCurrency?.CurrencyCode})";
        }

        private void FilterTaxableEvents(object sender, FilterEventArgs e)
        {
            TaxableEventViewModel viewModel = (TaxableEventViewModel)e.Item;

            if (FilterOnlyIsModified)
            {
                if (viewModel.IsModified != true)
                {
                    e.Accepted = false;
                    return;
                }
            }

            if (FilterOnlyHasErrors)
            {
                if (!viewModel.HasErrors)
                {
                    e.Accepted = false;
                    return;
                }
            }

            if (FilterEventType.HasValue)
            {
                if (viewModel.EventType != FilterEventType.Value)
                {
                    e.Accepted = false;
                    return;
                }
            }

            if (!string.IsNullOrWhiteSpace(FilterText))
            {
                var matchedText = viewModel.Description?.Contains(
                    FilterText, StringComparison.InvariantCultureIgnoreCase);

                if (matchedText != true)
                {
                    matchedText = viewModel.Transactions?.Any(
                        transaction => transaction.Amount.ToString().Contains(FilterText));
                }

                if (matchedText != true)
                {
                    e.Accepted = false;
                    return;
                }
            }

            e.Accepted = true;
        }
    }
}
