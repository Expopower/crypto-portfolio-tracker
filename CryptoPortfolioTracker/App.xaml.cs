﻿using CryptoPortfolioTracker.Services;
using CryptoPortfolioTracker.Utilities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace CryptoPortfolioTracker
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            EnsureMenuAlignment();

            // Ensure database exists
            using (var db = new CryptoPortfolioContext())
            {
                db.Database.Migrate();
            }
        }

        private void EnsureMenuAlignment()
        {
            FieldInfo? menuDropAlignmentField = typeof(SystemParameters).GetField("_menuDropAlignment", BindingFlags.NonPublic | BindingFlags.Static);
            Action setAlignmentValue = () => {
                if (SystemParameters.MenuDropAlignment && menuDropAlignmentField != null) menuDropAlignmentField.SetValue(null, false);
            };
            setAlignmentValue();
            SystemParameters.StaticPropertyChanged += (sender, e) => { setAlignmentValue(); };
        }

        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            // TODO: Perhaps implement a more graceful way to handle exceptions before it gets to this point?
            MessageBox.Show(TextUtility.BuildExceptionMessage(e.Exception), "Unhandled Exception",
                MessageBoxButton.OK, MessageBoxImage.Error);

            e.Handled = true;
        }
    }
}
