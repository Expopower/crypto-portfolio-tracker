﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace CryptoPortfolioTracker.Converters
{
    // From https://stackoverflow.com/a/5182660/1387174
    public class NullConverter<T> : IValueConverter
    {
        public NullConverter(T trueValue, T falseValue)
        {
            True = trueValue;
            False = falseValue;
        }

        public T True { get; set; }
        public T False { get; set; }

        public virtual object Convert(object? value, Type targetType, object parameter, CultureInfo culture)
        {
            return value == null ? True : False;
        }

        public virtual object ConvertBack(object? value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
