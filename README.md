CryptoPortfolioManager development reminders

Notes for creating models:
- Models referencing another by foreign key should also have an instance of the referenced model in them

To create/update a database based on models:
- Install Microsoft.EntityFrameworkCore.Sqlite and Microsoft.EntityFrameworkCore.Tools using Package Manager
- Add-Migration MIGRATION_NAME (to undo last migration, user Remove-Migration)
- Update-Database

Objectives of this project:
- Track cryptocurrency transactions and balances
- Integrate with cryptocurrency and fiat currency APIs to record historical exchange rates
- Allow calculation of investment performance over time
- Allow calculation of tax liabilities in a given year (using CRA Adjusted Cost Basis principles)

Data model:
- All changes in balances are grouped into TaxableEvents
- Within each TaxableEvent, there are one or more Transactions which represent changes to a particular FiatCurrency or Cryptocurrency balance
- Attached to each FiatCurrency and Cryptocurrency is a collection of historical FiatCurrencyRates and CryptocurrencyRates, respectively

Functionality:
- The fiat value of each Transaction is determined using the corresponding FiatCurrencyRate or CryptocurrencyRate for that day
- If the rate is not immediately known, an appropriate API will be consulted and the required rate with be retrieved and stored
- This historical value allows the performance of each Cryptocurrency to be assessed at any given point in time
- This historical value also allows calculation of total capital gains and income derived from Cryptocurrency in any given year
- Taxes will be based on Adjusted Cost Basis, the average cost of all identical units of an investment at a given point in time