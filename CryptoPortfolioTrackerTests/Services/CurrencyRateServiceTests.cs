﻿using CryptoPortfolioTracker.MockHelpers;
using CryptoPortfolioTracker.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.Services.Tests
{
    [TestClass]
    public class CurrencyRateServiceTests : ServiceTestsBase
    {
        private CurrencyRateService _currencyRateService;

        public CurrencyRateServiceTests() : base()
        {
            var cryptoExchangeRateApi = new MockCryptoExchangeRateApi(TestConstants.CryptoRate);
            var fiatExchangeRateApi = new MockFiatExchangeRateApi(TestConstants.FiatRate);
            _currencyRateService = new CurrencyRateService(DbOptions, cryptoExchangeRateApi, fiatExchangeRateApi,
                new CurrencyRateRetrievalDateRangeService(DbOptions),
                new CurrencyReferenceExchangeService(DbOptions, cryptoExchangeRateApi, fiatExchangeRateApi));
        }

        [TestMethod]
        public async Task EnsureCurrencyRatesForDateRangeAsyncTest()
        {
            Currency currency;
            Currency usdCurrency;
            Currency btcCurrency;
            List<CurrencyRate> currencyRates;
            CurrencyRateRetrievalDateRange dateRange;
            
            DateTime startDate1 = DateTime.Now.AddDays(-30).Date;
            DateTime endDate1 = DateTime.Now.AddDays(-10).Date;
            DateTime startDate2 = DateTime.Now.AddDays(-20).Date;
            DateTime endDate2 = DateTime.Now.AddDays(-5).Date;

            using (var db = GetDbContext())
            {
                currency = db.Currencies.First(c => c.ID == TestConstants.CryptoCurrency1ID);
                usdCurrency = db.Currencies.First(c => c.ID == CurrencyService.UsdCurrencyID);
                btcCurrency = db.Currencies.First(c => c.ID == CurrencyService.BtcCurrencyID);
            }

            await _currencyRateService.EnsureCurrencyRatesForDateRangeAsync(currency, usdCurrency, startDate1, endDate1);

            using (var db = GetDbContext())
            {
                currency = db.Currencies.Include(c => c.ReferenceExchanges)
                    .First(c => c.ID == TestConstants.CryptoCurrency1ID);
                currencyRates = db.CurrencyRates
                    .Where(rate => rate.FromCurrencyID == TestConstants.CryptoCurrency1ID)
                    .ToList();
                dateRange = db.CurrencyRateRetrievalDateRanges
                    .First(range => range.FromCurrencyID == TestConstants.CryptoCurrency1ID
                        && range.ToCurrencyID == CurrencyService.UsdCurrencyID);
            }

            DateTime minDate = currencyRates.Min(rate => rate.EffectiveDateTime);
            DateTime maxDate = currencyRates.Max(rate => rate.EffectiveDateTime);

            // Ensure the exact number of entries were correctly created, on the correct dates
            Assert.AreEqual(21, currencyRates.Count);
            Assert.AreEqual(startDate1, minDate);
            Assert.AreEqual(endDate1, maxDate);
            Assert.AreEqual(startDate1, dateRange.StartDate);
            Assert.AreEqual(endDate1, dateRange.EndDate);
            Assert.AreEqual(1, currency.ReferenceExchanges.Count);

            await _currencyRateService.EnsureCurrencyRatesForDateRangeAsync(currency, usdCurrency, startDate2, endDate2);

            using (var db = GetDbContext())
            {
                currency = db.Currencies.Include(c => c.ReferenceExchanges)
                    .First(c => c.ID == TestConstants.CryptoCurrency1ID);
                currencyRates = db.CurrencyRates
                    .Where(rate => rate.FromCurrencyID == TestConstants.CryptoCurrency1ID)
                    .ToList();
                dateRange = db.CurrencyRateRetrievalDateRanges
                    .First(range => range.FromCurrencyID == TestConstants.CryptoCurrency1ID
                        && range.ToCurrencyID == CurrencyService.UsdCurrencyID);
            }

            minDate = currencyRates.Min(rate => rate.EffectiveDateTime);
            maxDate = currencyRates.Max(rate => rate.EffectiveDateTime);

            // Ensure the date range and rate count is the union of the two date ranges
            Assert.AreEqual(26, currencyRates.Count);
            Assert.AreEqual(startDate1, minDate);
            Assert.AreEqual(endDate2, maxDate);
            Assert.AreEqual(startDate1, dateRange.StartDate);
            Assert.AreEqual(endDate2, dateRange.EndDate);
            Assert.AreEqual(1, currency.ReferenceExchanges.Count);

            await _currencyRateService.EnsureCurrencyRatesForDateRangeAsync(currency, btcCurrency, startDate1, endDate1);

            using (var db = GetDbContext())
            {
                currency = db.Currencies.Include(c => c.ReferenceExchanges)
                    .First(c => c.ID == TestConstants.CryptoCurrency1ID);
            }

            // Ensure retrieving rates for a different pair will create a new reference exchange
            Assert.AreEqual(2, currency.ReferenceExchanges.Count);
        }

        [TestMethod]
        public async Task GetNearestEffectiveRateAsyncTest()
        {
            using (var db = GetDbContext())
            {
                DateTime startDate = DateTime.Now.AddDays(-30).Date;
                DateTime endDate = DateTime.Now.AddDays(-10).Date;
                DateTime referenceDateInRange = DateTime.Now.AddDays(-25).Date;
                DateTime referenceDateBeforeRange = DateTime.Now.AddDays(-35).Date;
                DateTime referenceDateAfterRange = DateTime.Now.AddDays(-5).Date;
                Currency currency = db.Currencies.First(c => c.ID == TestConstants.CryptoCurrency1ID);
                Currency usdCurrency = db.Currencies.First(c => c.ID == CurrencyService.UsdCurrencyID);
                await _currencyRateService.EnsureCurrencyRatesForDateRangeAsync(currency, usdCurrency, startDate, endDate);

                CurrencyRate? currencyRateInRange = await _currencyRateService.GetNearestEffectiveRateAsync(
                    TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID, db.CurrencyRates, referenceDateInRange);
                CurrencyRate? currencyRateBeforeRange = await _currencyRateService.GetNearestEffectiveRateAsync(
                    TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID, db.CurrencyRates, referenceDateBeforeRange);
                CurrencyRate? currencyRateAfterRange = await _currencyRateService.GetNearestEffectiveRateAsync(
                    TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID, db.CurrencyRates, referenceDateAfterRange);

                // Verify only in-range/future requests succeed, but future requests use the nearest date
                Assert.IsNotNull(currencyRateInRange);
                Assert.IsNotNull(currencyRateAfterRange);
                Assert.IsNull(currencyRateBeforeRange);

                Assert.AreEqual(referenceDateInRange, currencyRateInRange.EffectiveDateTime);
                Assert.AreEqual(endDate, currencyRateAfterRange.EffectiveDateTime);
            }
        }

        [TestMethod]
        public async Task GetCurrencyRateAsyncTest()
        {
            decimal? tcadRate = await _currencyRateService.GetCurrencyRateAsync(
                CurrencyService.TcadCurrencyID, CurrencyService.CadCurrencyID, DateTime.Now, true);
            decimal? cryptoRate = await _currencyRateService.GetCurrencyRateAsync(
                TestConstants.CryptoCurrency1ID, CurrencyService.CadCurrencyID, DateTime.Now, true);

            Assert.AreEqual(1, tcadRate);
            Assert.AreEqual(TestConstants.CryptoRate * TestConstants.FiatRate, cryptoRate);

            decimal? cadRate = await _currencyRateService.GetCurrencyRateAsync(
                CurrencyService.CadCurrencyID, CurrencyService.CadCurrencyID, DateTime.Now, true);
            decimal? fiatRate = await _currencyRateService.GetCurrencyRateAsync(
                TestConstants.FiatCurrency1ID, CurrencyService.CadCurrencyID, DateTime.Now, true);

            Assert.AreEqual(1, cadRate);
            Assert.AreEqual(TestConstants.FiatRate, fiatRate);
        }

        [TestMethod]
        public async Task EnsureCurrencyRatesForDateTimeAsyncTest()
        {
            using (var db = GetDbContext())
            {
                await _currencyRateService.EnsureCurrencyRatesForDateTimeAsync(TestConstants.CryptoCurrency1ID, CurrencyService.CadCurrencyID, DateTime.Now);
                await _currencyRateService.EnsureCurrencyRatesForDateTimeAsync(TestConstants.FiatCurrency1ID, CurrencyService.CadCurrencyID, DateTime.Now);

                CurrencyRate cryptoCurrencyRate = db.CurrencyRates
                    .First(rate =>
                        rate.FromCurrencyID == TestConstants.CryptoCurrency1ID
                        && rate.EffectiveDateTime == DateTime.Now.Date);
                CurrencyRate fiatCurrencyRate = db.CurrencyRates
                    .First(rate =>
                        rate.FromCurrencyID == TestConstants.FiatCurrency1ID
                        && rate.EffectiveDateTime == DateTime.Now.Date);

                Assert.AreEqual(TestConstants.CryptoRate, cryptoCurrencyRate.Rate);
                Assert.AreEqual(TestConstants.FiatRate, fiatCurrencyRate.Rate);
            }
        }
    }
}