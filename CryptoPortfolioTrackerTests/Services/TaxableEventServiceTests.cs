﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoPortfolioTracker.MockHelpers;
using CryptoPortfolioTracker.Models;
using Microsoft.EntityFrameworkCore;

namespace CryptoPortfolioTracker.Services.Tests
{
    [TestClass]
    public class TaxableEventServiceTests : ServiceTestsBase
    {
        private CurrencyRateService _currencyRateService;
        private TransactionService _transactionService;
        private TaxableEventService _taxableEventService;

        public TaxableEventServiceTests() : base()
        {
            var cryptoExchangeRateApi = new MockCryptoExchangeRateApi(TestConstants.CryptoRate);
            var fiatExchangeRateApi = new MockFiatExchangeRateApi(TestConstants.FiatRate);
            _currencyRateService = new CurrencyRateService(DbOptions, cryptoExchangeRateApi, fiatExchangeRateApi,
                new CurrencyRateRetrievalDateRangeService(DbOptions),
                new CurrencyReferenceExchangeService(DbOptions, cryptoExchangeRateApi, fiatExchangeRateApi));
            _transactionService = new TransactionService(DbOptions, _currencyRateService);
            _taxableEventService = new TaxableEventService(DbOptions, _transactionService);
        }

        [TestMethod]
        public void GetTaxableEventsTest()
        {
            using (var db = GetDbContext())
            {
                db.TaxableEvents.Add(new TaxableEvent()
                {
                    EventType = TaxableEventType.Interest
                });
                db.TaxableEvents.Add(new TaxableEvent()
                {
                    EventType = TaxableEventType.Exchange
                });
                db.SaveChanges();
            }

            List<TaxableEvent> taxableEvents = _taxableEventService.GetTaxableEvents().ToList();
            Assert.AreEqual(2, taxableEvents.Count);
        }

        [TestMethod]
        public async Task SaveTaxableEventsAsyncTest()
        {
            using (var db = GetDbContext())
            {
                var taxableEventsToSave = new List<TaxableEvent>()
                {
                    new TaxableEvent()
                    {
                        EventType = TaxableEventType.Interest,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency1ID,
                                Amount = 1m
                            }
                        }
                    },
                    new TaxableEvent()
                    {
                        EventType = TaxableEventType.Exchange,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency1ID,
                                Amount = -0.5m
                            },
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency2ID,
                                Amount = 0.5m
                            }
                        }
                    },
                    new TaxableEvent()
                    {
                        EventType = TaxableEventType.Sale,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency1ID,
                                Amount = -0.5m
                            },
                            new Transaction()
                            {
                                CurrencyID = TestConstants.FiatCurrency1ID,
                                Amount = 5m
                            }
                        }
                    }
                };
                await _taxableEventService.SaveTaxableEventsAsync(taxableEventsToSave);

                List<TaxableEvent> taxableEvents = db.TaxableEvents
                    .Include(evt => evt.Transactions)
                    .ThenInclude(tx => tx.TransactionValues)
                    .ToList();
                List<Transaction> transactions = db.Transactions.ToList();

                // Verify taxable events are created, and calculated fields are populated
                Assert.AreEqual(3, taxableEvents.Count);
                Assert.AreEqual(5, transactions.Count);
                Assert.IsTrue(transactions.All(tx => tx.GetValue(CurrencyService.CadCurrencyID) != null));
                Assert.IsTrue(taxableEvents.All(evt => evt.IsModified == true));
                Assert.IsTrue(taxableEvents.All(evt => evt.NetValueInFiat.HasValue));
                
                foreach (TaxableEvent taxableEvent in taxableEvents)
                {
                    Assert.AreEqual(taxableEvent.Transactions.Sum(tx => tx.GetValue(CurrencyService.CadCurrencyID)?.Amount),
                        taxableEvent.NetValueInFiat);
                }
            }
        }

        [TestMethod]
        public async Task DeleteTaxableEventTest()
        {
            using (var db = GetDbContext())
            {
                await _taxableEventService.SaveTaxableEventAsync(
                    new TaxableEvent()
                    {
                        EventType = TaxableEventType.Interest,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency1ID,
                                Amount = 1m
                            }
                        }
                    });

                Assert.AreEqual(1, db.TaxableEvents.Count());
                Assert.AreEqual(1, db.Transactions.Count());

                TaxableEvent taxableEvent = db.TaxableEvents.First();
                _taxableEventService.DeleteTaxableEvent(taxableEvent);

                // Verify taxable event and associated transaction are deleted
                Assert.AreEqual(0, db.TaxableEvents.Count());
                Assert.AreEqual(0, db.Transactions.Count());
            }
        }

        [TestMethod]
        public async Task ClearModifiedTaxableEventsAsyncTest()
        {
            using (var db = GetDbContext())
            {
                var taxableEventsToSave = new List<TaxableEvent>()
                {
                    new TaxableEvent()
                    {
                        EventType = TaxableEventType.Interest,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency1ID,
                                Amount = 1m
                            }
                        }
                    },
                    new TaxableEvent()
                    {
                        EventType = TaxableEventType.Exchange,
                        Transactions = new List<Transaction>()
                        {
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency1ID,
                                Amount = -0.5m
                            },
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency2ID,
                                Amount = 0.5m
                            }
                        }
                    }
                };
                await _taxableEventService.SaveTaxableEventsAsync(taxableEventsToSave);

                List<TaxableEvent> taxableEvents = db.TaxableEvents.ToList();
                Assert.AreEqual(2, taxableEvents.Count);
                Assert.IsTrue(taxableEvents.All(evt => evt.IsModified == true));
            }

            await _taxableEventService.ClearModifiedTaxableEventsAsync();

            using (var db = GetDbContext())
            {
                // Verify taxable events still exist but are no longer marked as modified
                List<TaxableEvent> taxableEvents = db.TaxableEvents.ToList();
                Assert.AreEqual(2, taxableEvents.Count);
                Assert.IsTrue(taxableEvents.All(evt => evt.IsModified == false));
            }
        }

        [TestMethod]
        public async Task GetTaxYearsTest()
        {
            var taxableEventsToSave = new List<TaxableEvent>()
            {
                new TaxableEvent()
                {
                    EventType = TaxableEventType.Interest,
                    CreatedDateTime = new DateTime(2012, 1, 1),
                    Transactions = new List<Transaction>()
                    {
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency1ID,
                            Amount = 1m
                        }
                    }
                },
                new TaxableEvent()
                {
                    EventType = TaxableEventType.Interest,
                    CreatedDateTime = new DateTime(2018, 1, 1),
                    Transactions = new List<Transaction>()
                    {
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency1ID,
                            Amount = 1m
                        }
                    }
                },
                new TaxableEvent()
                {
                    EventType = TaxableEventType.Exchange,
                    CreatedDateTime = new DateTime(2018, 1, 1),
                    Transactions = new List<Transaction>()
                    {
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency1ID,
                            Amount = -0.5m
                        },
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency2ID,
                            Amount = 0.5m
                        }
                    }
                }
            };
            await _taxableEventService.SaveTaxableEventsAsync(taxableEventsToSave);


            // Verify distinct list of years are returned
            List<int> taxYears = _taxableEventService.GetTaxYears();
            Assert.AreEqual(2, taxYears.Count);
            Assert.IsTrue(taxYears.Contains(2012));
            Assert.IsTrue(taxYears.Contains(2018));

            await _taxableEventService.SaveTaxableEventAsync(
                new TaxableEvent()
                {
                    EventType = TaxableEventType.Interest,
                    CreatedDateTime = new DateTime(2020, 1, 1),
                    Transactions = new List<Transaction>()
                    {
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency1ID,
                            Amount = 1m
                        }
                    }
                });

            // Verify new addition
            taxYears = _taxableEventService.GetTaxYears();
            Assert.AreEqual(3, taxYears.Count);
            Assert.IsTrue(taxYears.Contains(2020));
        }

        [TestMethod]
        public async Task RecalculateCadValuesAsyncTest()
        {
            var fiatTransaction = new Transaction()
            {
                CurrencyID = TestConstants.FiatCurrency1ID,
                Amount = -0.5m
            };
            var cryptoTransaction = new Transaction()
            {
                CurrencyID = TestConstants.CryptoCurrency1ID,
                Amount = 0.5m
            };
            var taxableEvent = new TaxableEvent()
            {
                EventType = TaxableEventType.Purchase,
                Transactions = new List<Transaction>()
                {
                    fiatTransaction,
                    cryptoTransaction
                }
            };

            await _taxableEventService.RecalculateValuesAsync(taxableEvent);

            TransactionValue? fiatTxCadValue = fiatTransaction.GetValue(CurrencyService.CadCurrencyID);
            TransactionValue? cryptoTxCadValue = cryptoTransaction.GetValue(CurrencyService.CadCurrencyID);

            Assert.IsNotNull(fiatTxCadValue);
            Assert.IsNotNull(cryptoTxCadValue);
            Assert.IsNotNull(taxableEvent.NetValueInFiat);
            Assert.AreEqual(fiatTransaction.Amount * TestConstants.FiatRate,
                fiatTxCadValue.Amount);
            Assert.AreEqual(cryptoTransaction.Amount * TestConstants.CryptoRate * TestConstants.FiatRate,
                cryptoTxCadValue.Amount);
            Assert.AreEqual(fiatTxCadValue.Amount + cryptoTxCadValue.Amount,
                taxableEvent.NetValueInFiat);
        }
    }
}