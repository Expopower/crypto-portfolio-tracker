﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.MockHelpers;

namespace CryptoPortfolioTracker.Services.Tests
{
    [TestClass]
    public class CurrencyServiceTests : ServiceTestsBase
    {
        private CurrencyService _currencyService;

        public CurrencyServiceTests() : base()
        {
            _currencyService = new CurrencyService(DbOptions, new MockCryptoExchangeRateApi(TestConstants.CryptoRate),
                new MockFiatExchangeRateApi(TestConstants.FiatRate));
        }

        [TestMethod]
        public void GetCurrenciesTest()
        {
            List<Currency> currencies = _currencyService.GetCurrencies();

            Assert.AreEqual(11, currencies.Count);
            Assert.AreEqual(true, currencies.Any(c => c.CurrencyType == CurrencyType.Crypto));
            Assert.AreEqual(true, currencies.Any(c => c.CurrencyType == CurrencyType.Fiat));
        }

        [TestMethod]
        public void GetFiatCurrenciesTest()
        {
            List<Currency> currencies = _currencyService.GetFiatCurrencies();

            Assert.AreEqual(4, currencies.Count);
            Assert.AreEqual(false, currencies.Any(c => c.CurrencyType == CurrencyType.Crypto));
            Assert.AreEqual(true, currencies.Any(c => c.CurrencyType == CurrencyType.Fiat));
        }

        [TestMethod]
        public void GetCryptoCurrenciesTest()
        {
            List<Currency> currencies = _currencyService.GetCryptoCurrencies();

            Assert.AreEqual(7, currencies.Count);
            Assert.AreEqual(true, currencies.Any(c => c.CurrencyType == CurrencyType.Crypto));
            Assert.AreEqual(false, currencies.Any(c => c.CurrencyType == CurrencyType.Fiat));
        }

        [TestMethod]
        public async Task SaveCurrenciesAsyncTest()
        {
            List<Currency> currencies = _currencyService.GetCryptoCurrencies();
            Currency? cryptoCurrency1 = currencies.First(c => c.ID == TestConstants.CryptoCurrency1ID);
            Currency? cryptoCurrency2 = currencies.First(c => c.ID == TestConstants.CryptoCurrency2ID);
            var newCurrency = new Currency()
            {
                CurrencyCode = "CurrencyCode",
                Name = "Currency Name"
            };

            cryptoCurrency1.CurrencyCode = "UpdatedCurrencyCode";
            currencies.Add(newCurrency);
            currencies.Remove(cryptoCurrency2);

            await _currencyService.SaveCurrenciesAsync(currencies, CurrencyType.Crypto);

            currencies = _currencyService.GetCryptoCurrencies();
            cryptoCurrency1 = currencies.FirstOrDefault(c => c.ID == cryptoCurrency1.ID);
            cryptoCurrency2 = currencies.FirstOrDefault(c => c.ID == cryptoCurrency2.ID);
            newCurrency = currencies.FirstOrDefault(c => c.CurrencyCode == newCurrency.CurrencyCode);

            // Verify currencies were added/deleted/updated correctly
            Assert.AreEqual(7, currencies.Count);
            Assert.IsNull(cryptoCurrency2);
            Assert.IsNotNull(cryptoCurrency1);
            Assert.IsNotNull(newCurrency);
            Assert.AreEqual("UpdatedCurrencyCode", cryptoCurrency1.CurrencyCode);
            Assert.AreNotEqual(Guid.Empty, newCurrency.ID);
        }

        [TestMethod]
        public async Task EnsureCurrencyForCodeAsyncTest()
        {
            int originalCurrencyCount = _currencyService.GetCryptoCurrencies().Count;

            Currency newCurrency = await _currencyService.EnsureCurrencyForCodeAsync("NewCode", CurrencyType.Crypto);

            // Verify new currency is created
            List<Currency> currencies = _currencyService.GetCryptoCurrencies();
            Currency? savedNewCurrency = currencies.FirstOrDefault(c => c.CurrencyCode == "NewCode");
            Assert.IsNotNull(savedNewCurrency);
            Assert.AreEqual(originalCurrencyCount + 1, currencies.Count);

            await _currencyService.EnsureCurrencyForCodeAsync("NewCode", CurrencyType.Crypto);

            // Verify that ensuring the same code does not create a new currency
            currencies = _currencyService.GetCryptoCurrencies();
            Assert.AreEqual(originalCurrencyCount + 1, currencies.Count);
        }
    }
}