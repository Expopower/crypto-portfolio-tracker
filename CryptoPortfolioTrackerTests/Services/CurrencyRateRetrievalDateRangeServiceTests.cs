﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoPortfolioTracker.MockHelpers;
using CryptoPortfolioTracker.Models;

namespace CryptoPortfolioTracker.Services.Tests
{
    [TestClass]
    public class CurrencyRateRetrievalDateRangeServiceTests : ServiceTestsBase
    {
        private CurrencyRateRetrievalDateRangeService _currencyRateRetrievalDateRangeService;

        public CurrencyRateRetrievalDateRangeServiceTests() : base()
        {
            _currencyRateRetrievalDateRangeService = new CurrencyRateRetrievalDateRangeService(DbOptions);
        }

        [TestMethod]
        public async Task IsWithinRetrievedRatesDatesTest()
        {
            await _currencyRateRetrievalDateRangeService.CreateOrUpdateRateRetrievalDateRange(
                TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID, DateTime.Now.AddDays(-10), DateTime.Now.AddDays(-5));

            bool isWithinDates = await _currencyRateRetrievalDateRangeService.IsWithinRateRetrievalDateRange(
                TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID, DateTime.Now.AddDays(-7));
            Assert.AreEqual(true, isWithinDates);
                
            isWithinDates = await _currencyRateRetrievalDateRangeService.IsWithinRateRetrievalDateRange(
                TestConstants.CryptoCurrency1ID, CurrencyService.CadCurrencyID, DateTime.Now.AddDays(-7));
            Assert.AreEqual(false, isWithinDates);

            isWithinDates = await _currencyRateRetrievalDateRangeService.IsWithinRateRetrievalDateRange(
                TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID, DateTime.Now.AddDays(-3));
            Assert.AreEqual(false, isWithinDates);

            isWithinDates = await _currencyRateRetrievalDateRangeService.IsWithinRateRetrievalDateRange(
                TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID, DateTime.Now.AddDays(-12));
            Assert.AreEqual(false, isWithinDates);
        }

        [TestMethod]
        public async Task CreateOrUpdateRateRetrievalDateRangeTest()
        {
            CurrencyRateRetrievalDateRange dateRange;

            DateTime initialStartDate = DateTime.Now.AddDays(-10);
            DateTime initialEndDate = DateTime.Now.AddDays(-5);
            await _currencyRateRetrievalDateRangeService.CreateOrUpdateRateRetrievalDateRange(
                TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID, initialStartDate, initialEndDate);

            DateTime newStartDate1 = DateTime.Now.AddDays(-8);
            DateTime newEndDate1 = DateTime.Now.AddDays(-5);                        
            await _currencyRateRetrievalDateRangeService.CreateOrUpdateRateRetrievalDateRange(
                TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID, newStartDate1, newEndDate1);

            using (var db = GetDbContext())
            {
                dateRange = db.CurrencyRateRetrievalDateRanges
                    .First(range => range.FromCurrencyID == TestConstants.CryptoCurrency1ID
                        && range.ToCurrencyID == CurrencyService.UsdCurrencyID);
            }
            Assert.AreEqual(initialStartDate, dateRange.StartDate);
            Assert.AreEqual(newEndDate1, dateRange.EndDate);

            DateTime newStartDate2 = DateTime.Now.AddDays(-12);
            DateTime newEndDate2 = DateTime.Now.AddDays(-7);
            await _currencyRateRetrievalDateRangeService.CreateOrUpdateRateRetrievalDateRange(
                TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID, newStartDate2, newEndDate2);

            using (var db = GetDbContext())
            {
                dateRange = db.CurrencyRateRetrievalDateRanges
                    .First(range => range.FromCurrencyID == TestConstants.CryptoCurrency1ID
                        && range.ToCurrencyID == CurrencyService.UsdCurrencyID);
            }
            Assert.AreEqual(newStartDate2, dateRange.StartDate);
            Assert.AreEqual(newEndDate1, dateRange.EndDate);
        }
    }
}