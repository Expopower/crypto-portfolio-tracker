﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoPortfolioTracker.MockHelpers;
using CryptoPortfolioTracker.Models;

namespace CryptoPortfolioTracker.Services.Tests
{
    [TestClass()]
    public class TransactionServiceTests : ServiceTestsBase
    {
        private CurrencyRateService _currencyRateService;
        private TransactionService _transactionService;

        public TransactionServiceTests() : base()
        {
            var cryptoExchangeRateApi = new MockCryptoExchangeRateApi(TestConstants.CryptoRate);
            var fiatExchangeRateApi = new MockFiatExchangeRateApi(TestConstants.FiatRate);
            _currencyRateService = new CurrencyRateService(DbOptions, cryptoExchangeRateApi, fiatExchangeRateApi,
                new CurrencyRateRetrievalDateRangeService(DbOptions),
                new CurrencyReferenceExchangeService(DbOptions, cryptoExchangeRateApi, fiatExchangeRateApi));
            _transactionService = new TransactionService(DbOptions, _currencyRateService);
        }

        [TestMethod]
        public async Task EnsureAlternativeCurrencyAmountsForTransactionsAsyncTest()
        {
            var fiatTransaction = new Transaction()
            {
                CurrencyID = TestConstants.FiatCurrency1ID,
                Amount = -5m
            };
            var cryptoTransaction = new Transaction()
            {
                CurrencyID = TestConstants.CryptoCurrency1ID,
                Amount = 14m
            };
            var transactions = new List<Transaction>()
            {
                fiatTransaction,
                cryptoTransaction
            };

            await _transactionService.EnsureAlternativeCurrencyAmountsForTransactionsAsync(DateTime.Now, transactions);

            TransactionValue? fiatTxCadValue = fiatTransaction.GetValue(CurrencyService.CadCurrencyID);
            TransactionValue? cryptoTxCadValue = cryptoTransaction.GetValue(CurrencyService.CadCurrencyID);
            TransactionValue? fiatTxBtcValue = fiatTransaction.GetValue(CurrencyService.BtcCurrencyID);
            TransactionValue? cryptoTxBtcValue = cryptoTransaction.GetValue(CurrencyService.BtcCurrencyID);

            Assert.IsNotNull(fiatTxCadValue);
            Assert.IsNotNull(cryptoTxCadValue);
            Assert.AreEqual(fiatTransaction.Amount * TestConstants.FiatRate,
                fiatTxCadValue.Amount);
            Assert.AreEqual(cryptoTransaction.Amount * TestConstants.CryptoRate * TestConstants.FiatRate,
                cryptoTxCadValue.Amount);

            Assert.IsNotNull(fiatTxBtcValue);
            Assert.AreEqual(fiatTransaction.Amount * TestConstants.CryptoRate * TestConstants.FiatRate,
                fiatTxBtcValue.Amount);
            Assert.IsNotNull(cryptoTxBtcValue);
            Assert.AreEqual(cryptoTransaction.Amount * TestConstants.CryptoRate,
                cryptoTxBtcValue.Amount);
        }

        [TestMethod]
        public async Task AddOrUpdateTransactionValueAsyncTest()
        {
            var transaction = new Transaction()
            {
                CurrencyID = TestConstants.CryptoCurrency1ID,
                Amount = -5m
            };

            await _transactionService.AddOrUpdateTransactionValueAsync(transaction, TestConstants.FiatCurrency1ID, DateTime.Now);
            await _transactionService.AddOrUpdateTransactionValueAsync(transaction, TestConstants.CryptoCurrency2ID, DateTime.Now);

            TransactionValue? fiat1TxValue = transaction.GetValue(TestConstants.FiatCurrency1ID);
            TransactionValue? fiat2TxValue = transaction.GetValue(TestConstants.FiatCurrency2ID);
            TransactionValue? crypto2TxValue = transaction.GetValue(TestConstants.CryptoCurrency2ID);

            Assert.IsNull(fiat2TxValue);
            Assert.IsNotNull(fiat1TxValue);
            Assert.IsNotNull(crypto2TxValue);
            Assert.AreEqual(transaction.Amount * TestConstants.CryptoRate * TestConstants.FiatRate,
                fiat1TxValue.Amount);
            Assert.AreEqual(transaction.Amount * TestConstants.CryptoRate,
                crypto2TxValue.Amount);

            transaction.Amount = 5m;

            await _transactionService.AddOrUpdateTransactionValueAsync(transaction, TestConstants.FiatCurrency1ID, DateTime.Now);
            await _transactionService.AddOrUpdateTransactionValueAsync(transaction, TestConstants.CryptoCurrency2ID, DateTime.Now);
            
            Assert.AreEqual(2, transaction.TransactionValues.Count);
            Assert.AreEqual(transaction.Amount * TestConstants.CryptoRate * TestConstants.FiatRate,
                fiat1TxValue.Amount);
            Assert.AreEqual(transaction.Amount * TestConstants.CryptoRate,
                crypto2TxValue.Amount);
        }

        [TestMethod]
        public async Task EnsureExchangeRatesForTransactionsAsyncTest()
        {
            var transactions = new List<Transaction>()
            {
                new Transaction()
                {
                    CurrencyID = TestConstants.FiatCurrency1ID,
                    Amount = -5m
                },
                new Transaction()
                {
                    CurrencyID = TestConstants.CryptoCurrency1ID,
                    Amount = 14m
                }
            };

            await _transactionService.EnsureExchangeRatesForTransactionsAsync(CurrencyService.CadCurrencyID, DateTime.Now, transactions);

            using (var db = GetDbContext())
            {
                CurrencyRate cryptoCurrencyRate = db.CurrencyRates
                    .First(rate =>
                        rate.FromCurrencyID == TestConstants.CryptoCurrency1ID
                        && rate.EffectiveDateTime == DateTime.Now.Date);
                CurrencyRate fiatCurrencyRate = db.CurrencyRates
                    .First(rate =>
                        rate.FromCurrencyID == TestConstants.FiatCurrency1ID
                        && rate.EffectiveDateTime == DateTime.Now.Date);

                Assert.AreEqual(TestConstants.CryptoRate, cryptoCurrencyRate.Rate);
                Assert.AreEqual(TestConstants.FiatRate, fiatCurrencyRate.Rate);
            }
        }

        [TestMethod]
        public async Task EnsureAmountInCadAsyncTest()
        {
            var cryptoTransaction = new Transaction()
            {
                CurrencyID = TestConstants.CryptoCurrency1ID,
                Amount = 14m
            };
            var cadTransaction = new Transaction()
            {
                CurrencyID = CurrencyService.CadCurrencyID,
                Amount = 14m
            };

            await _transactionService.AddOrUpdateTransactionValueAsync(cryptoTransaction, CurrencyService.CadCurrencyID, DateTime.Now);
            await _transactionService.AddOrUpdateTransactionValueAsync(cadTransaction, CurrencyService.CadCurrencyID, DateTime.Now);

            Assert.AreEqual(cryptoTransaction.Amount * TestConstants.CryptoRate * TestConstants.FiatRate,
                cryptoTransaction.GetValue(CurrencyService.CadCurrencyID)?.Amount);
            Assert.AreEqual(cadTransaction.Amount, cadTransaction.GetValue(CurrencyService.CadCurrencyID)?.Amount);
        }

        [TestMethod]
        public async Task SynchronizeToAndFromTransactionsAsyncTest()
        {
            var toTransaction = new Transaction()
            {
                CurrencyID = TestConstants.CryptoCurrency1ID,
                Amount = 5m
            };
            var fromTransaction = new Transaction()
            {
                CurrencyID = TestConstants.CryptoCurrency2ID,
                Amount = -2m
            };

            // Verify Exchange results in From transaction defining value of To transaction
            await _transactionService.SynchronizeToAndFromTransactionsAsync(
                toTransaction, fromTransaction, TaxableEventType.Exchange, DateTime.Now);

            TransactionValue? fromTxCadValue = fromTransaction.GetValue(CurrencyService.CadCurrencyID);

            Assert.IsNotNull(fromTxCadValue);
            Assert.IsNotNull(toTransaction.OverrideFiatValue);
            Assert.AreEqual(-fromTxCadValue.Amount, toTransaction.OverrideFiatValue);

            toTransaction = new Transaction()
            {
                CurrencyID = CurrencyService.TcadCurrencyID,
                Amount = 5m
            };
            fromTransaction = new Transaction()
            {
                CurrencyID = TestConstants.CryptoCurrency1ID,
                Amount = -2m
            };

            // Verify Exchange to TCAD results in an output akin to a sale
            await _transactionService.SynchronizeToAndFromTransactionsAsync(
                toTransaction, fromTransaction, TaxableEventType.Exchange, DateTime.Now);

            TransactionValue? toTxCadValue = toTransaction.GetValue(CurrencyService.CadCurrencyID);

            Assert.IsNotNull(fromTransaction.OverrideFiatValue);
            Assert.IsNotNull(toTxCadValue);
            Assert.AreEqual(-toTxCadValue.Amount, fromTransaction.OverrideFiatValue);

            toTransaction = new Transaction()
            {
                CurrencyID = TestConstants.FiatCurrency1ID,
                Amount = 5m
            };
            fromTransaction = new Transaction()
            {
                CurrencyID = TestConstants.CryptoCurrency1ID,
                Amount = -2m
            };

            // Verify Sale results in To transaction defining value of From transaction 
            await _transactionService.SynchronizeToAndFromTransactionsAsync(
                toTransaction, fromTransaction, TaxableEventType.Sale, DateTime.Now);

            toTxCadValue = toTransaction.GetValue(CurrencyService.CadCurrencyID);

            Assert.IsNotNull(toTxCadValue);
            Assert.IsNotNull(fromTransaction.OverrideFiatValue);
            Assert.AreEqual(-toTxCadValue.Amount, fromTransaction.OverrideFiatValue);

            // Verify Purchase
            toTransaction = new Transaction()
            {
                CurrencyID = TestConstants.CryptoCurrency1ID,
                Amount = 5m
            };
            fromTransaction = new Transaction()
            {
                CurrencyID = TestConstants.FiatCurrency1ID,
                Amount = -2m
            };

            // Verify Purchase results in From transaction defining value of To transaction
            await _transactionService.SynchronizeToAndFromTransactionsAsync(
                toTransaction, fromTransaction, TaxableEventType.Purchase, DateTime.Now);

            fromTxCadValue = fromTransaction.GetValue(CurrencyService.CadCurrencyID);

            Assert.IsNotNull(fromTxCadValue);
            Assert.IsNotNull(toTransaction.OverrideFiatValue);
            Assert.AreEqual(-fromTxCadValue.Amount, toTransaction.OverrideFiatValue);
        }
    }
}