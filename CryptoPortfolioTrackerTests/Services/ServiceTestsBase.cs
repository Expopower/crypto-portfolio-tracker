﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Transactions;
using CryptoPortfolioTracker.Models;

namespace CryptoPortfolioTracker.Services.Tests
{
    /// <summary>
    /// Base class for service methods that require database access.
    /// </summary>
    [TestClass]
    public abstract class ServiceTestsBase
    {
        private const string dbName = "CryptoPortfolioTrackerTests";
        private readonly DbContextOptions<CryptoPortfolioContext> _dbOptions;

        protected DbContextOptions<CryptoPortfolioContext> DbOptions
        {
            get
            {
                return _dbOptions;
            }
        }

        public ServiceTestsBase()
        {
            _dbOptions = new DbContextOptionsBuilder<CryptoPortfolioContext>()
                .UseSqlite($"Data Source=file:{dbName}?memory&cache=shared")
                .EnableSensitiveDataLogging()
                .Options;
        }

        [TestInitialize]
        public virtual void TestInitialize()
        {
            using (var db = GetDbContext())
            {
                db.Database.EnsureDeleted();
                db.Database.OpenConnection();
                db.Database.EnsureCreated();
            }

            CreateBasicTestData();
        }

        [TestCleanup]
        public virtual void TestCleanup()
        {
            using (var db = GetDbContext())
            {
                db.Database.CloseConnection();
                db.Database.EnsureDeleted();
            }
        }

        protected CryptoPortfolioContext GetDbContext()
        {
            return new CryptoPortfolioContext(DbOptions);
        }

        private void CreateBasicTestData()
        {
            using (var db = GetDbContext())
            {
                db.Currencies.Add(new Currency()
                {
                    ID = TestConstants.CryptoCurrency1ID,
                    CurrencyCode = "CRYPTO1",
                    Name = "Crypto Currency 1",
                    CurrencyType = CurrencyType.Crypto
                });
                db.Currencies.Add(new Currency()
                {
                    ID = TestConstants.CryptoCurrency2ID,
                    CurrencyCode = "CRYPTO2",
                    Name = "Crypto Currency 2",
                    CurrencyType = CurrencyType.Crypto
                });
                db.Currencies.Add(new Currency()
                {
                    ID = TestConstants.FiatCurrency1ID,
                    CurrencyCode = "FIAT1",
                    Name = "Fiat Currency 1",
                    CurrencyType = CurrencyType.Fiat
                });
                db.Currencies.Add(new Currency()
                {
                    ID = TestConstants.FiatCurrency2ID,
                    CurrencyCode = "FIAT2",
                    Name = "Crypto Currency 2",
                    CurrencyType = CurrencyType.Fiat
                });
                db.SaveChanges();
            }
        }

        protected class TestConstants
        {
            public static readonly Guid CryptoCurrency1ID = Guid.NewGuid();
            public static readonly Guid CryptoCurrency2ID = Guid.NewGuid();
            public static readonly Guid FiatCurrency1ID = Guid.NewGuid();
            public static readonly Guid FiatCurrency2ID = Guid.NewGuid();
            public const decimal CryptoRate = 1.5m;
            public const decimal FiatRate = 2.5m;
        }
    }
}