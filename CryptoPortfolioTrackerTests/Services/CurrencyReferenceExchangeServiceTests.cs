﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoPortfolioTracker.MockHelpers;
using CryptoPortfolioTracker.Models;

namespace CryptoPortfolioTracker.Services.Tests
{
    [TestClass]
    public class CurrencyReferenceExchangeServiceTests : ServiceTestsBase
    {
        private CurrencyReferenceExchangeService _currencyReferenceExchangeService;

        public CurrencyReferenceExchangeServiceTests() : base()
        {
            var cryptoExchangeRateApi = new MockCryptoExchangeRateApi(TestConstants.CryptoRate);
            var fiatExchangeRateApi = new MockFiatExchangeRateApi(TestConstants.FiatRate);
            _currencyReferenceExchangeService = new CurrencyReferenceExchangeService(DbOptions, cryptoExchangeRateApi, fiatExchangeRateApi);
        }

        [TestMethod]
        public async Task EnsureCurrencyReferenceExchangeTest()
        {
            List<CurrencyReferenceExchange> retrievedExchanges;
            CurrencyReferenceExchange cryptoReferenceExchange = await _currencyReferenceExchangeService.EnsureCurrencyReferenceExchange(
                TestConstants.CryptoCurrency1ID, CurrencyService.UsdCurrencyID);

            using (var db = GetDbContext())
            {
                retrievedExchanges = db.CurrencyReferenceExchanges.ToList();
            }

            Assert.AreEqual(CurrencyType.Crypto, cryptoReferenceExchange.ExchangeCurrencyType);
            Assert.AreEqual(1, retrievedExchanges.Count);

            CurrencyReferenceExchange fiatReferenceExchange = await _currencyReferenceExchangeService.EnsureCurrencyReferenceExchange(
                TestConstants.FiatCurrency1ID, CurrencyService.UsdCurrencyID);

            using (var db = GetDbContext())
            {
                retrievedExchanges = db.CurrencyReferenceExchanges.ToList();
            }

            Assert.AreEqual(CurrencyType.Fiat, fiatReferenceExchange.ExchangeCurrencyType);
            Assert.AreEqual(2, retrievedExchanges.Count);

            await _currencyReferenceExchangeService.EnsureCurrencyReferenceExchange(
                TestConstants.FiatCurrency1ID, CurrencyService.UsdCurrencyID);

            using (var db = GetDbContext())
            {
                retrievedExchanges = db.CurrencyReferenceExchanges.ToList();
            }

            // Ensure duplicated pair of currency IDs does not create a new row
            Assert.AreEqual(2, retrievedExchanges.Count);
        }
    }
}