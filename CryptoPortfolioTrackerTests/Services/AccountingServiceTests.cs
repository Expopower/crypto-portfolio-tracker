﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoPortfolioTracker.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoPortfolioTracker.Models;
using Microsoft.EntityFrameworkCore;
using CryptoPortfolioTracker.MockHelpers;
using CryptoPortfolioTracker.ViewModels;

namespace CryptoPortfolioTracker.Services.Tests
{
    [TestClass]
    public class AccountingServiceTests : ServiceTestsBase
    {
        private CurrencyRateService _currencyRateService;
        private AccountingService _accountingService;
        private TransactionService _transactionService;
        private TaxableEventService _taxableEventService;

        public AccountingServiceTests() : base()
        {
            var cryptoExchangeRateApi = new MockCryptoExchangeRateApi(TestConstants.CryptoRate);
            var fiatExchangeRateApi = new MockFiatExchangeRateApi(TestConstants.FiatRate);
            _currencyRateService = new CurrencyRateService(DbOptions, cryptoExchangeRateApi, fiatExchangeRateApi,
                new CurrencyRateRetrievalDateRangeService(DbOptions),
                new CurrencyReferenceExchangeService(DbOptions, cryptoExchangeRateApi, fiatExchangeRateApi));
            _accountingService = new AccountingService(DbOptions, _currencyRateService);
            _transactionService = new TransactionService(DbOptions, _currencyRateService);
            _taxableEventService = new TaxableEventService(DbOptions, _transactionService);
        }

        [TestMethod]
        public async Task CalculateCryptoHoldingsAsyncTest()
        {
            using (var db = GetDbContext())
            {
                var taxableEventsToSave = new List<TaxableEvent>()
                {
                    new TaxableEvent()
                    {
                        EventType = TaxableEventType.Bonus,
                        CreatedDateTime = DateTime.Now.AddDays(-3),
                        Transactions = new List<Transaction>()
                        {
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency1ID,
                                Amount = 1
                            },
                            new Transaction()
                            {
                                CurrencyID = TestConstants.FiatCurrency1ID,
                                Amount = 1
                            }
                        }
                    },
                    new TaxableEvent()
                    {
                        EventType = TaxableEventType.Exchange,
                        CreatedDateTime = DateTime.Now.AddDays(-2),
                        Transactions = new List<Transaction>()
                        {
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency1ID,
                                Amount = -0.5m
                            },
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency2ID,
                                Amount = 15m
                            }
                        }
                    }
                };
                await _taxableEventService.SaveTaxableEventsAsync(taxableEventsToSave);

                List<CryptoHoldingViewModel> cryptoHoldings = await _accountingService.CalculateCryptoHoldingsAsync(true);
                CryptoHoldingViewModel? crypto1Holding = cryptoHoldings
                    .SingleOrDefault(holding => holding.CurrencyID == TestConstants.CryptoCurrency1ID);
                CryptoHoldingViewModel? crypto2Holding = cryptoHoldings
                    .SingleOrDefault(holding => holding.CurrencyID == TestConstants.CryptoCurrency2ID);
                CryptoHoldingViewModel? fiat1Holding = cryptoHoldings
                    .SingleOrDefault(holding => holding.CurrencyID == TestConstants.FiatCurrency1ID);

                // Ensure cryptocurrencies are included, and fiat currencies are omitted
                Assert.AreEqual(2, cryptoHoldings.Count);
                Assert.IsNull(fiat1Holding);
                Assert.IsNotNull(crypto1Holding);
                Assert.IsNotNull(crypto2Holding);

                // Verify balance values
                Assert.AreEqual(0.5m, crypto1Holding.Balance);
                Assert.AreEqual(0.5m * TestConstants.CryptoRate * TestConstants.FiatRate,
                    crypto1Holding.CurrentFiatValue);
                Assert.AreEqual(TestConstants.CryptoRate, crypto1Holding.CryptoValuePerUnit);
                Assert.AreEqual(TestConstants.CryptoRate * TestConstants.FiatRate, crypto1Holding.FiatValuePerUnit);

                Assert.AreEqual(15m, crypto2Holding.Balance);
                Assert.AreEqual(15m * TestConstants.CryptoRate * TestConstants.FiatRate,
                    crypto2Holding.CurrentFiatValue);
                Assert.AreEqual(TestConstants.CryptoRate, crypto2Holding.CryptoValuePerUnit);
                Assert.AreEqual(TestConstants.CryptoRate * TestConstants.FiatRate, crypto2Holding.FiatValuePerUnit);

                await _taxableEventService.SaveTaxableEventAsync(
                    new TaxableEvent()
                    {
                        EventType = TaxableEventType.Sale,
                        CreatedDateTime = DateTime.Now.AddDays(-1),
                        Transactions = new List<Transaction>()
                        {
                            new Transaction()
                            {
                                CurrencyID = TestConstants.CryptoCurrency1ID,
                                Amount = -1m
                            }
                        }
                    });

                // Ensure a negative crypto balance results in an error
                cryptoHoldings = await _accountingService.CalculateCryptoHoldingsAsync(true);
                crypto1Holding = cryptoHoldings
                    .SingleOrDefault(holding => holding.CurrencyID == TestConstants.CryptoCurrency1ID);
                
                Assert.IsNotNull(crypto1Holding);
                Assert.IsTrue(crypto1Holding.HasErrors);
                StringAssert.Contains(crypto1Holding.ErrorLog, "Balance Calculation Discrepancy");
            }
        }

        [TestMethod]
        public async Task CalculateTaxableIncomeAsyncTest()
        {
            var taxableEventsToSave = new List<TaxableEvent>()
            {
                new TaxableEvent()
                {
                    CreatedDateTime = DateTime.Now.AddYears(-1),
                    EventType = TaxableEventType.Bonus,
                    Transactions = new List<Transaction>()
                    {
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency1ID,
                            Amount = 20m
                        }
                    }
                },
                new TaxableEvent()
                {
                    EventType = TaxableEventType.Interest,
                    Transactions = new List<Transaction>()
                    {
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency1ID,
                            Amount = 5m
                        },
                    }
                },
                new TaxableEvent()
                {
                    EventType = TaxableEventType.Sale,
                    Transactions = new List<Transaction>()
                    {
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency1ID,
                            OverrideFiatValue = -10m,
                            Amount = -10m
                        }
                    }
                },
                new TaxableEvent()
                {
                    EventType = TaxableEventType.Exchange,
                    Transactions = new List<Transaction>()
                    {
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency1ID,
                            Amount = -7m
                        },
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency2ID,
                            Amount = 5m
                        }
                    }
                },
                new TaxableEvent()
                {
                    EventType = TaxableEventType.Cashback,
                    Transactions = new List<Transaction>()
                    {
                        new Transaction()
                        {
                            CurrencyID = TestConstants.CryptoCurrency1ID,
                            Amount = 1m
                        },
                    }
                }
            };
            await _taxableEventService.SaveTaxableEventsAsync(taxableEventsToSave);

            TaxableIncomeViewModel taxView =
                await _accountingService.CalculateTaxableIncomeAsync(DateTime.Now.Year);

            // Verify transactions and values were collected correctly
            // There are a lot more complex ways to alter capital gains...but for sanity this will do for now
            Assert.AreEqual(DateTime.Now.Year, taxView.TaxYear);
            Assert.AreEqual(3, taxView.ApplicableTransactions.Count);
            Assert.AreEqual(5m * TestConstants.CryptoRate * TestConstants.FiatRate,
                taxView.Income);
            Assert.AreEqual(10m - 10m * TestConstants.CryptoRate * TestConstants.FiatRate,
                taxView.CapitalGains);
        }
    }
}