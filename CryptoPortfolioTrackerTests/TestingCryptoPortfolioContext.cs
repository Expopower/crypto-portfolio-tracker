﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using Microsoft.EntityFrameworkCore;

namespace CryptoPortfolioTracker
{
    internal class TestingCryptoPortfolioContext : CryptoPortfolioContext
    {
        public TestingCryptoPortfolioContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options
                .UseSqlite($"Data Source=:memory:")
                .EnableSensitiveDataLogging();
    }
}
