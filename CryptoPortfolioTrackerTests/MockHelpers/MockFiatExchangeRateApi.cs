﻿using CryptoPortfolioTracker.ExternalApis;
using CryptoPortfolioTracker.Models;
using CryptoPortfolioTracker.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CryptoPortfolioTracker.MockHelpers
{
    /// <summary>
    /// A mock fiat exchange rate service to support unit testing.
    /// </summary>
    internal class MockFiatExchangeRateApi : IExchangeRateApi
    {
        private readonly decimal _exchangeRate;

        public MockFiatExchangeRateApi(decimal exchangeRate)
        {
            _exchangeRate = exchangeRate;
        }

        public async Task<Currency> GetCurrencyAsync(string currencyCode)
        {
            return new Currency()
            {
                CurrencyCode = currencyCode,
                Name = $"Testing Currency {currencyCode}",
                CurrencyType = CurrencyType.Fiat
            };
        }

        public async Task<List<string>> GetAvailableExchangesAsync(string fromCurrencyCode, string toCurrencyCode)
        {
            return new List<string>()
            {
                "fiatexchange"
            };
        }

        public async Task<List<CurrencyRate>?> GetExchangeRatesAsync(string fromCurrencyCode, string toCurrencyCode,
            string? referenceExchange, bool invert, DateTime startDate, DateTime endDate)
        {
            var currencyRates = new List<CurrencyRate>();

            for (DateTime dateTime = startDate; dateTime <= endDate; dateTime = dateTime.AddDays(1))
            {
                currencyRates.Add(new CurrencyRate()
                {
                    EffectiveDateTime = dateTime,
                    Rate = _exchangeRate
                });
            }

            return currencyRates;
        }
    }
}
